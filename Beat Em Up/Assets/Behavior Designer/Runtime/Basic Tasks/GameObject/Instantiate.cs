using UnityEngine;

namespace BehaviorDesigner.Runtime.Tasks.Basic.UnityGameObject
{
    public class InstantiateItemDrop : Action
    {
        public CustomSharedVariables.SharedCharacter myCharacter;
        public SharedFloat itemChance;
        public SharedGameObject itemDrop;
		public SharedQuaternion rotation;

        public override TaskStatus OnUpdate()
        {
			GameObject.Instantiate(itemDrop.Value, myCharacter.Value.transform.position, rotation.Value);
            return TaskStatus.Success;
        }
    }
}