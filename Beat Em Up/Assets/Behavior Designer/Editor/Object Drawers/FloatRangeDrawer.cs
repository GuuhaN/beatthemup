﻿using UnityEngine;
using UnityEditor;
using BehaviorDesigner.Editor;

[CustomObjectDrawer(typeof(FloatRangeAttribute))]
public class FloatRangeDrawer : ObjectDrawer
{
    public override void OnGUI(GUIContent label)
    {
        var rangeAttribute = (FloatRangeAttribute)attribute;
        value = EditorGUILayout.Slider(label, (float)value, rangeAttribute.min, rangeAttribute.max);

    }
}