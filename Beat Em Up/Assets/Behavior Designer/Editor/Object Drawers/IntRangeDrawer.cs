﻿using UnityEngine;
using UnityEditor;
using BehaviorDesigner.Editor;

[CustomObjectDrawer(typeof(IntRangeAttribute))]
public class IntRangeDrawer : ObjectDrawer
{
    public override void OnGUI(GUIContent label)
    {
        var rangeAttribute = (IntRangeAttribute)attribute;
        value = EditorGUILayout.Slider(label, (int)value, rangeAttribute.min, rangeAttribute.max);
    }
}