﻿//Sound Effect handler class that manages all the sounds in the game

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundEffects : MonoBehaviour {
    //Visual sound Array (look inspector)
    [SerializeField] private Sound[] allSounds;

    //Sound file arrays
    [SerializeField] private AudioClip[] punchSounds;
    [SerializeField] private AudioClip[] kickSounds;
    [SerializeField] private AudioClip[] waterSounds;
    [SerializeField] private AudioClip[] backGroundMusic;

    private AudioSource source;
    private AudioClip lastClip;

    //Possible sound options
    public enum soundNames {Punch, Kick, ComboHook, RoundHouseKick, LegSweep, Respawn, RagdollHit, Water};

    private void Start()
    {
        //Create an object with a new audio source and the default clip then hide it in the parent
        for (int i = 0; i < allSounds.Length; i++)
        {
            GameObject soundEffect = new GameObject("Sound_" + i + "_" + allSounds[i].name);
            soundEffect.transform.SetParent(transform);
            allSounds[i].SetNewSource(soundEffect.AddComponent<AudioSource>());
        }
        source = GetComponent<AudioSource>();
        StartCoroutine(playBackgroundMusic());
    }

    //Can be called in another class to play any sound
    public void PlaySound(soundNames currentName)
    {
        //looks for the sound in all the created sounds
        for (int i = 0; i < allSounds.Length; i++)
        {
            //if sound is equal to the requested sound
            if(allSounds[i].name == currentName)
            {
                // if the sound is a Punch add a random clip from all punches
                if (soundNames.Punch == currentName)
                {
                    StartCoroutine(PickANewClip(punchSounds));
                    allSounds[i].SetClip(lastClip);
                }

                if (soundNames.Kick == currentName)
                {
                    StartCoroutine(PickANewClip(kickSounds));
                    allSounds[i].SetClip(lastClip);
                }

                if (soundNames.Water == currentName)
                {
                    StartCoroutine(PickANewClip(waterSounds));
                    allSounds[i].SetClip(lastClip);
                }


                allSounds[i].Play();
                return;
            }
        }
        Debug.Log("No Sound Found" + currentName);
    }

    //play background music
    private IEnumerator playBackgroundMusic()
    {
        AudioClip currentclip = backGroundMusic[Random.Range(0, 2)];
        source.clip = currentclip;
        source.Play();
        //when sound is finished restart the playbackgroundmusic loop with a fresh or the same clip
        yield return new WaitForSeconds(currentclip.length);
        StartCoroutine(playBackgroundMusic());
    }

    //call this to pick a new clip based on the sounds
    private IEnumerator PickANewClip(AudioClip[] soundList)
    {
        int newClip = Random.Range(0, soundList.Length);
        //check if the last clip wasn't the same clip
        if (lastClip != soundList[newClip])
        {
            //set lastclip to the new selected clip
            lastClip = soundList[newClip];
            yield return null;
        }
        //else restart the process
        else
            StartCoroutine(PickANewClip(soundList));
    }
}
