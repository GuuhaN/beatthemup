﻿//Base Sound class

using UnityEngine;
using System;

[System.Serializable]
public class Sound
{
    public SoundEffects.soundNames name;
    public AudioClip clip;
    [Range(0f, 1f)] public float volume = 0.7f;
    public float pitch = 1f;
    public float pitchValue = 0.3f;
    public bool randomPitch;

    private AudioSource source;

    //Set a new audio source with the clip for the object
    public void SetNewSource(AudioSource newSource)
    {
        source = newSource;
        source.clip = clip;
    }

    //Set a different clip independently from the new source function
    public void SetClip(AudioClip newClip)
    {
        source.clip = newClip;
    }

    //Play the audioclip in the audio source
    public void Play()
    {
        //If randompitch bool is enabled choose pitch
        if (randomPitch)
            source.pitch = RandomPitch(pitch - pitchValue, pitch + pitchValue);
        else
            source.pitch = pitch;

        source.volume = volume;
        source.PlayOneShot(source.clip);
    }

    //return a random pitch for the audio source
    private float RandomPitch(float minRange, float maxRange)
    {
        float randomPitch = UnityEngine.Random.Range(minRange, maxRange);
        return randomPitch;
    }
}