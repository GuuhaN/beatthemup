﻿using UnityEngine;
using System.Collections;

/**<summary
 * 
 * This script is specifically for the designer to debug faster through a menu.
 * 
 * </summary>*/


public class TomToggles : MonoBehaviour
{
    private PlayerCamera playerCamera;

    public bool infiniteComboBar
    {
        get { return _infiniteComboBar; }
    }
    public bool infiniteDamage
    {
        get { return _infiniteDamage; }
    }

    [Header("Player Affecting Toggles")]
    [SerializeField, Tooltip("Sets combo bar for EVERY player to infinite")] private bool _infiniteComboBar;
    [SerializeField, Tooltip("Gives EVERY player 500 damage")] private bool _infiniteDamage;

    public bool skipLevel
    {
        get { return _skipLevel; }
        set { _skipLevel = value; }
    }

    [Header("Game Affecting Toggles")]
    [SerializeField, Tooltip("Skip current level ( Restarting the map ATM )")] private bool _skipLevel;

    private void Awake()
    {
        playerCamera = FindObjectOfType<PlayerCamera>();
    }

    private void Update()
    {
        SetInfiniteComboBar();
        SetMaxDamage();
        SkipLevel();
    }

    /** Every player has an infinite combo bar so they can use all their attacks. */
    private void SetInfiniteComboBar()
    {
        if (!infiniteComboBar)
            return;

        for (int i = 0; i < playerCamera.playersAlive.Count; i++)
            playerCamera.playersAlive[i].comboBonus = 100f;
    }

    /** Sets every player to have a max damage. So they can one hit. */
    private void SetMaxDamage()
    {
        if (!infiniteDamage)
            return;

        for (int i = 0; i < playerCamera.playersAlive.Count; i++)
            playerCamera.playersAlive[i].characterDamage = 1000000000;
    }

    /** Skips the level to debug the end screen. */
    private void SkipLevel()
    {
        if (!skipLevel)
            return;

        StartCoroutine(playerCamera.EndingLevel());
    }
}
