﻿//Debug UI in game for easier PlayTesting and BugTesting

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DebugUI : MonoBehaviour
{
    //Toggle the window with this bool before playing
    [SerializeField] private bool debugEnabled;

    private bool debugMenu;
    private string text = "Open Debug";
    private bool godMode;

    private void setGodMode()
    {
        //Find all players to change the values
        Player[] players = FindObjectsOfType<Player>();

        //Set godmode (players can still die to water)
        if (godMode)
        {
            foreach (Player player in players)
            {
                player.characterMaxHealth = 99999;
                player.characterHealth = 99999;
                player.playerLives = 99999;
            }
        }
        else
        {
            //Reset godmode
            foreach (Player player in players)
            {
                player.characterMaxHealth = 100;
                player.characterHealth = 100;
                player.playerLives = 3;
            }
        }
    }

    private void OnGUI()
    {
        //If Debug is on
        if (debugEnabled)
        {

            if (GUI.Button(new Rect(Screen.width * 0.03f, Screen.height * 0.30f, Screen.width * 0.10f, Screen.height * 0.03f), text))
            {
                debugMenu = !debugMenu;
                if (debugMenu)
                    text = "Close Debug";
                else
                    text = "Open Debug";
            }

            //If Debug menu is open
            if (debugMenu)
            {

                //GodMode
                if (GUI.Button(new Rect(Screen.width * 0.15f, Screen.height * 0.30f, Screen.width * 0.10f, Screen.height * 0.03f), "God Mode = " + godMode))
                {
                    godMode = !godMode;
                    setGodMode();
                }

                //Remove the last player (4 , 3 , 2 , 1)
                if (GUI.Button(new Rect(Screen.width * 0.15f, Screen.height * 0.35f, Screen.width * 0.10f, Screen.height * 0.03f), "Remove 1 Player"))
                {
                    PlayerJoining playerJoining = FindObjectOfType<PlayerJoining>();
                    for (int i = playerJoining.playerJoined.Length - 1; i >= 0; i--)
                    {
                        if (playerJoining.playerJoined[i])
                        {
                            playerJoining.playerJoined[i] = false;
                            return;
                        }
                    }
                }

                //Add last player (1 , 2 , 3 , 4)
                if (GUI.Button(new Rect(Screen.width * 0.03f, Screen.height * 0.35f, Screen.width * 0.10f, Screen.height * 0.03f), "Add 1 Player"))
                {
                    PlayerJoining playerJoining = FindObjectOfType<PlayerJoining>();
                    for (int i = 0; i < playerJoining.playerJoined.Length; i++)
                    {
                        if (!playerJoining.playerJoined[i])
                        {
                            playerJoining.playerJoined[i] = true;
                            return;
                        }
                    }
                }

                //Add all players
                if (GUI.Button(new Rect(Screen.width * 0.03f, Screen.height * 0.4f, Screen.width * 0.10f, Screen.height * 0.03f), "Add All Players"))
                {
                    PlayerJoining playerJoining = FindObjectOfType<PlayerJoining>();
                    for (int i = 0; i < playerJoining.playerJoined.Length; i++)
                    {
                        playerJoining.playerJoined[i] = true;
                    }
                }

                //Remove all players
                if (GUI.Button(new Rect(Screen.width * 0.15f, Screen.height * 0.4f, Screen.width * 0.10f, Screen.height * 0.03f), "Remove All Players"))
                {
                    PlayerJoining playerJoining = FindObjectOfType<PlayerJoining>();
                    for (int i = 0; i < playerJoining.playerJoined.Length; i++)
                    {
                        playerJoining.playerJoined[i] = false;
                    }
                }

                //Add 1 enemy
                if (GUI.Button(new Rect(Screen.width * 0.03f, Screen.height * 0.45f, Screen.width * 0.10f, Screen.height * 0.03f), "Add 1 Enemy"))
                {
                    //Loads all spawnwaves enemies
                    SpawnWaves spawnWaves = FindObjectOfType<SpawnWaves>();
                    EnemyData enemy = null;
                    PlayerJoining playerJoining = FindObjectOfType<PlayerJoining>();

                    //Picks a random enemy from the array
                    int selectedEnemy = Random.Range(0, spawnWaves.enemyPrefabs.Length);
                    for (int i = 0; i < spawnWaves.enemyPrefabs.Length; i++)
                    {
                        if (i == selectedEnemy)
                        {
                            enemy = spawnWaves.enemyPrefabs[i];
                        }
                    }
                    //Creates the enemy at the spawnpoint of the players
                    if (enemy)
                        Instantiate(enemy.gameObject, new Vector3(playerJoining.spawnPoint.transform.position.x, 5, playerJoining.spawnPoint.transform.position.z), Quaternion.identity);
                }

                //Remove every enemy in the game
                if (GUI.Button(new Rect(Screen.width * 0.15f, Screen.height * 0.45f, Screen.width * 0.10f, Screen.height * 0.03f), "Remove All Enemies"))
                {
                    EnemyData[] enemiesAlive = FindObjectsOfType<EnemyData>();
                    foreach (EnemyData enemy in enemiesAlive)
                    {
                        Destroy(enemy.gameObject);
                    }
                }

                //Restart the game in the MainScene not the character select
                if (GUI.Button(new Rect(Screen.width * 0.03f, Screen.height * 0.50f, Screen.width * 0.10f, Screen.height * 0.03f), "Restart Game"))
                {
                    SceneManager.LoadScene(1);
                }
            }
        }
    }
}
