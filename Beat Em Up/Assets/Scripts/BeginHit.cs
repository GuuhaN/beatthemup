﻿using UnityEngine;
using System.Collections;

public class BeginHit : StateMachineBehaviour {

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        
        if(!animator.GetBool("IsFlinched"))
	    animator.SetBool("IsFlinched", true);
	}
}
