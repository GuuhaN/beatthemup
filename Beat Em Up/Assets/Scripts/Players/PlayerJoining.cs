﻿//System for loading various player informations and the settings of the UI. This is also used to join players for now but might be removed later on.

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class PlayerJoining : MonoBehaviour {
    public Player[] allPlayers;
    public bool friendlyFire;
    [SerializeField] private GameObject[] allPlayerUI;
    [SerializeField] private GameObject[] allPlayerText;
    public bool[] playerJoined;
    private bool[] gameOver;
    private bool[] setPosition;
    public Transform spawnPoint;
    public Player[] players;

	void Awake ()
    {
        playerJoined = new bool[allPlayers.Length];
        setPosition = new bool[allPlayers.Length];
        gameOver = new bool[allPlayers.Length];
        for (int i = 0; i < allPlayers.Length; i++)
            playerJoined[i] = false;

    }
	
	void Update () {
            players = FindObjectsOfType<Player>().OrderBy(x => x.name).ToArray();
        for (int i = 0; i < players.Length; i++)
        {
            if (!allPlayers[i] && players[i])
            {
                allPlayers[players[i].playerID-1] = players[i];
            }
        }
        for (int i = 0; i < allPlayers.Length; i++)
        {
            if (allPlayers[i] != null)
            {
                //If startkey is pressed set this player to active
                if (Input.GetButtonDown("StartKey_P" + (i + 1)))
                    playerJoined[i] = true;

                //If the player is active
                if (playerJoined[i])
                {
                    //Enable UI and set the friendlyFire to the bool declared in this script
                    //Disable the text to show press start to join
                    allPlayers[i].gameObject.SetActive(true);
                    allPlayers[i].friendlyFire = friendlyFire;
                    allPlayerUI[i].gameObject.SetActive(true);
                    allPlayerText[i].gameObject.SetActive(false);

                    //Loop to see if players are gameover
                    if (checkIfGameOver())
                        SceneManager.LoadScene(0);

                    //Set player to spawn point location
                    if (!setPosition[i])
                    {
                        allPlayers[i].gameObject.transform.position = spawnPoint.transform.position;
                        setPosition[i] = true;
                    }
                }

                //If players are not active or have died
                else
                    if (allPlayers[i].gameObject.activeSelf && !allPlayers[i].GetComponent<Player>().hasDied && !allPlayers[i].GetComponent<Player>().knockedOut)
                {
                    allPlayerUI[i].gameObject.SetActive(false);
                    setPosition[i] = false;
                    allPlayerText[i].gameObject.SetActive(true);
                    allPlayers[i].DisableObject();
                }
            }
        }
	}

    //Game over check
    private bool checkIfGameOver()
    {
        //For each player
        for (int i = 0; i < gameOver.Length; i++)
        {
            //See if player joined
            if (playerJoined[i])
                //If the player joined and the lives are less than 0
                if (allPlayers[i].playerLives > 0)
                    //Not GameOver
                    return false;
        }
        //GameOver
        return true;
    }
}
