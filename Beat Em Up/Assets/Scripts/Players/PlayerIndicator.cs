﻿//Player indicator class for all playerindicator related stuff

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Player))]
[RequireComponent(typeof(SpriteRenderer))]
public class PlayerIndicator : MonoBehaviour {
    //every indicator art
    [SerializeField] private Sprite[] indicators;
    public int id;
    [SerializeField] private Sprite art;

    //Layer used for the indicator
    public LayerMask layer;
    private Vector3 parentPosition;

	void Start () {
        //Set art based on the id
        art = indicators[id-1];
        GetComponent<SpriteRenderer>().sprite = art;       
	}
	
	void Update () {
        //If player is dead remove the indicator
        if (transform.parent.GetComponent<Player>().hasDied)
            GetComponent<SpriteRenderer>().sprite = null;

        else
            GetComponent<SpriteRenderer>().sprite = art;

        //Set the position from the parent and then snap it on the ground with a raycast
        parentPosition = transform.parent.GetComponent<Transform>().position;
        RaycastHit hit;
        Physics.Raycast(new Vector3(parentPosition.x, parentPosition.y + 0.1f, parentPosition.z), -Vector3.up, out hit, Mathf.Infinity, layer);
        transform.position = new Vector3(parentPosition.x, hit.point.y + 0.1f, parentPosition.z);
    }
}
