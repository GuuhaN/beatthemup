﻿//playerSpawn used to spawn the player and load the saved character from characterSelect

using UnityEngine;
using System.Collections;

public class playerSpawn : MonoBehaviour {
    [SerializeField]private int id;
    [SerializeField]
    private Player[] players;
    [SerializeField] private Color[] playerColors;
    [SerializeField] private PlayerIndicator indicator;
    private GameObject current;

    void Awake()
    {
        //Load the saved characters
        SaveCharacters save = FindObjectOfType<SaveCharacters>();
        if (!save)
        {
            //Instantiate the loaded player and set the variables (moveset for the attacks, ID for events and playercolor for UI)
            current = Instantiate(players[id - 1].gameObject, transform.position, Quaternion.identity) as GameObject;
            current.name = "Player" + (id);
            current.GetComponent<Player>().playerID = id;
            current.GetComponent<Player>().colorParticle = playerColors[id-1];
            current.GetComponent<Player>().MoveSetInt = id-1;
        }

        else {
            //If character isn't selected remove the spawn object
            if (save.characterIDs[id - 1] == -1)
            {
                Destroy(gameObject);
                return;
            }
            //Instantiate a default character for debug purposes can be removed
                current = Instantiate(players[save.characterIDs[id - 1]].gameObject, transform.position, Quaternion.identity) as GameObject;
                current.name = "Player" + id;
                current.GetComponent<Player>().playerID = id;
                current.GetComponent<Player>().colorParticle = playerColors[id - 1];
                current.GetComponent<Player>().MoveSetInt = save.characterIDs[id - 1];
        }

        //Instantiate the indicator for the players and destroy the spawn object afterwards
        GameObject i = Instantiate(indicator.gameObject, transform.position, Quaternion.identity) as GameObject;
        i.GetComponent<PlayerIndicator>().id = id;
        i.transform.SetParent(current.transform);
        i.transform.eulerAngles = new Vector3(90, 0, 0);
        Destroy(gameObject);
    }
}
