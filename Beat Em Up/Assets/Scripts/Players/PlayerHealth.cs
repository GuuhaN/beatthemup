﻿//Player health class for the sizes and fills of the player bars

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
    [HideInInspector]public float maxHealth;
    [HideInInspector]public float currentHealth;
    public GameObject healthBar;
    public GameObject middleBar;
    public GameObject comboBar;
    private float middleBarHealth = 100f;
    public bool hitByAnything;
    private float timeTillComboEnds;
    public float comboBonus;

	void Update () {
        if (!comboBar)
        {
            //Loads the stats from the player
            int id = GetComponent<Player>()._playerID;
            healthBar = GameObject.Find("Fronthealth" + id);
            middleBar = GameObject.Find("middleHealth" + id);
            comboBar = GameObject.Find("ComboBonus" + id);
        }

        else {
            //Set the sizes for each bar using the current and max values
            healthBar.GetComponent<RectTransform>().sizeDelta = new Vector2(currentHealth / maxHealth * 99f, 9);
            middleBar.GetComponent<RectTransform>().sizeDelta = new Vector2(middleBarHealth / maxHealth * 99f, 9f);
            comboBar.GetComponent<RectTransform>().sizeDelta = new Vector2(comboBonus / 100f * 99f, 5f);
        }

        //Hit by anything is a timer that counts to show a different color of health for a combo health loss
        if (hitByAnything)
        {
            timeTillComboEnds = 1f;
            hitByAnything = false;      
        }
        
        //If combo ends combo healthbar will decrease down (the orange healthbar)
        if (timeTillComboEnds > 0)
            timeTillComboEnds -= Time.deltaTime;
            if (middleBarHealth > currentHealth && timeTillComboEnds <= 0)
            {
                middleBarHealth -= Time.deltaTime * 25f;
                if (middleBarHealth < currentHealth)
                    middleBarHealth = currentHealth;
            }

            //Prevent healthbar going negative
        if (currentHealth > middleBarHealth)
            middleBarHealth = currentHealth;
	}
}
