﻿using System;
using System.Collections;
using UnityEngine;

/**<summary
 * 
 * With this script, it will make any character/object to have the ability to throw items. Most likely this is only useful on characters.
 * This script is dependable on the script ThrowableObject, since that has the required information to throw an object.
 * 
 * </summary>*/

public class ThrowScript : MonoBehaviour
{
    /** These variables are for the throw object information */
    public GameObject currentLiftingObject;
    public ThrowableObject liftingObjectScript { get; set; }
    public Character liftingCharacter { get; private set; }

    [SerializeField] private LayerMask layersToPickup;

    public bool grabbing { get; set; }
    private Character myCharacterScript;

    private Vector3 moveDirection;
    private Vector3 rotation;

    /** Initializing components */
    private void Awake()
    {
        liftingObjectScript = GetComponent<ThrowableObject>();
        myCharacterScript = GetComponent<Character>();
    }

    private void Update()
    {
        UpdateGrab();
    }

    /** This is where the grabbing/throwing starts of any character. */
    public bool GrabIdentifiedObject()
    {
        IdentifyObject();

        /** When an object does not meet any of these requirements, it will instantly reset the whole grab and returns false. */
        if (!currentLiftingObject || !liftingObjectScript.canGetHit || liftingObjectScript.isGrabbing ||
            liftingObjectScript.isDead || liftingObjectScript.isKnockedOut ||
            liftingObjectScript.isGrabbed || myCharacterScript.usingWeapon || myCharacterScript.hasDied ||
            myCharacterScript.knockedOut ||
            (GetComponent<Character>() && !liftingObjectScript.isStunned))
        {
            ResetGrabIdentification();
            return false;
        }
        if (GetComponent<Player>().grounded)
            GetComponent<Rigidbody>().isKinematic = true;

        /** If the object you're holding is a weapon type, it will set grabbing on true. */
        if (liftingObjectScript._ObjectType != ThrowableObject.ObjectType.Weapon)
            grabbing = true;

        /** When an object is the type of Physics Object, then it will special animations when you try to pick something up. */
        if (liftingObjectScript._ObjectType == ThrowableObject.ObjectType.PhysicsObject)
        {
            myCharacterScript.animatorController.SetFloat("Movement", 0);
            myCharacterScript.animatorController.SetTrigger("PickUp");
        }

        return true;
    }

    private void UpdateGrab()
    {
        /** If grabbing != true, it will cause a return statement. Avoid nesting and for safety. */
        if (!grabbing)
            return;

        /** An extra check if you have an object. */
        if (!currentLiftingObject)
            ResetGrabIdentification();

        /** If ONLY you're a player, then these functions will be executed. */
        if (GetComponent<Player>())
        {
            /** If a player is stunned, knocked or player has no charge on the combobar, the player will release them. */
            if (myCharacterScript.stunDuration > 0 || liftingObjectScript.isKnockedOut)
            {
                if (GetComponent<Player>().comboBonus <= 0 &&
                    liftingObjectScript._ObjectType == ThrowableObject.ObjectType.Character)
                    ResetGrabIdentification();
                return;
            }

            /** This overrides the movement of the player if you own an object as player. */
            float inputHorizontal = Input.GetAxis("Horizontal_P" + GetComponent<Player>()._playerID);
            float inputVertical = Input.GetAxis("Vertical_P" + GetComponent<Player>()._playerID);

            moveDirection = new Vector3(10 * inputHorizontal, 0, 10 * inputVertical);
            rotation = Vector3.RotateTowards(transform.forward, moveDirection, 5 * Time.deltaTime, 0.0f);
        }
        
        switch (liftingObjectScript._ObjectType)
        {
            /** If the object you're holding is type of Character, it will execute this case. */
            case ThrowableObject.ObjectType.Character:
                currentLiftingObject.transform.position = new Vector3(transform.position.x, transform.position.y + 0.1f, transform.position.z) + transform.forward;
                currentLiftingObject.transform.eulerAngles = transform.eulerAngles;
                liftingObjectScript.isGrabbed = true;
                liftingObjectScript.myAnimator.SetBool("IsGrabbed", liftingObjectScript.isGrabbed);
                myCharacterScript.animatorController.SetBool("Lifting", true);

                if (GetComponent<Player>())
                    GetComponent<Player>().comboBonus -= Time.deltaTime*currentLiftingObject.GetComponent<Character>().characterGrabMultiplier;

                break;
            /** If the object you're holding is type of PhysicsObject, it will execute this case. */
            case ThrowableObject.ObjectType.PhysicsObject:
                transform.rotation = Quaternion.LookRotation(rotation);
                currentLiftingObject.transform.position = new Vector3(transform.position.x, transform.position.y + 0.15f, transform.position.z) + transform.up*2;
                currentLiftingObject.transform.eulerAngles = transform.eulerAngles;
                liftingObjectScript.isGrabbed = true;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    /** This function can only be called when you have an object. */
    public void ThrowIdentifiedObject()
    {
        /** When a players has a lifting object that is a weapon, it will automatically add the ThrowableObject on it.
            if not, it will reset the throw. */
        if (!myCharacterScript.usingWeapon)
        {
            if (!currentLiftingObject && !grabbing)
            {
                ResetGrabIdentification();
                return;
            }
        }
        else
            liftingObjectScript = myCharacterScript.weapon.GetComponent<ThrowableObject>();

        switch (liftingObjectScript._ObjectType)
        {
            /** If the object you're holding is a Character. Then the character will be thrown towards the direction your left xbox360 analog stick is facing to. */
            case ThrowableObject.ObjectType.Character:
                liftingCharacter = currentLiftingObject.GetComponent<Character>();
                currentLiftingObject.transform.GetChild(1)
                    .GetComponent<Rigidbody>().velocity = Vector3.zero;
                liftingCharacter.EnableRagdoll();
                liftingCharacter.TakeDamage(10);
                currentLiftingObject.transform.GetChild(1)
                    .GetComponent<Rigidbody>().AddForce(moveDirection * 500 + transform.up * 1000);
                liftingObjectScript.isKnockedOut = true;

                if(myCharacterScript.GetComponent<Player>())
                    myCharacterScript.GetComponent<Player>().GiveMyEnemyHealth(liftingCharacter.characterHealth, liftingCharacter.characterMaxHealth, liftingCharacter.characterPoints, liftingCharacter.characterName);
                if (liftingCharacter.characterHealth <= 0)
                    break;

                StartCoroutine(liftingCharacter.RagdollGetUp(liftingCharacter.getUpTime));
                break;
                /** The Physics Objects are the only objects that will be thrown with a Chargebar. This one requires a hold on the throw button. */
            case ThrowableObject.ObjectType.PhysicsObject:
                liftingObjectScript.myRigidBody.velocity = Vector3.zero;
                if (!GetComponent<Player>())
                    return;

                currentLiftingObject.GetComponent<PhysicsObject>().playerID = GetComponent<Player>()._playerID;
                currentLiftingObject.GetComponent<PhysicsObject>().objectDamage = (int) (10 +
                                                                                         GetComponent<Player>().chargeBar.roundedLength*100);

                Debug.Log(currentLiftingObject.GetComponent<PhysicsObject>().objectDamage);

                liftingObjectScript.myRigidBody.AddForce(transform.forward * 100 * GetComponent<Player>().chargeBar.roundedLength * 10 + transform.up * 35);
                break;
                /** If it's a weapon that you throw, it will throw into a straight line from where you character is facing towards. */
            case ThrowableObject.ObjectType.Weapon:
                myCharacterScript.weapon.GetComponent<Rigidbody>().velocity = Vector3.zero;
                myCharacterScript.weapon.GetComponent<Rigidbody>().useGravity = false;
                myCharacterScript.weapon.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x, 0, transform.forward.z) * 12;
                myCharacterScript.weapon.AddComponent<PhysicsObject>();
                myCharacterScript.weapon.GetComponent<PhysicsObject>().playerID = GetComponent<Player>()._playerID;
                myCharacterScript.weapon.GetComponent<PhysicsObject>().objectDamage = 25;
                myCharacterScript.weapon.GetComponent<PhysicsObject>().particle =
                    FindObjectOfType<ParticleLoader>().hitParticles[0].gameObject;
                myCharacterScript.weapon.GetComponent<PhysicsObject>().objectBreakable = true;
                myCharacterScript.weapon.layer = LayerMask.NameToLayer("LiftableObject");
                Physics.IgnoreCollision(myCharacterScript.weapon.GetComponent<Collider>(), myCharacterScript.GetComponent<Collider>());
                break;
        }

        ResetGrabIdentification();
    }

    /** Use this function if you want to reset the throwing. */
    public void ResetGrabIdentification()
    {
        if (!currentLiftingObject)
            return;

        if (liftingObjectScript._ObjectType == ThrowableObject.ObjectType.PhysicsObject)
        {
            GetComponent<Player>().chargeBar.myImage.fillAmount = 0f;
            GetComponent<Player>().chargeBar.currentSplit = 0;
        }

        /** If only your character is using a weapon. It will execute this piece of code. */
        if (myCharacterScript.weapon)
        {
            foreach (BoxCollider box in myCharacterScript.weapon.GetComponents<BoxCollider>())
            {
                Debug.Log("sssss");
                box.enabled = true;
            }
            myCharacterScript.weapon.transform.SetParent(null);
            myCharacterScript.weapon.GetComponent<Rigidbody>().useGravity = true;
            myCharacterScript.weapon.GetComponent<Rigidbody>().isKinematic = false;
            myCharacterScript.usingWeapon = false;
            myCharacterScript.weapon = null;
        }

        grabbing = false;
        GetComponent<Rigidbody>().isKinematic = false;
        liftingObjectScript.isGrabbed = false;
        currentLiftingObject = null;

        if (liftingObjectScript.myAnimator)
            liftingObjectScript.myAnimator.SetBool("IsGrabbed", liftingObjectScript.isGrabbed);
    }

    /** This Function is meant to identify the object you're standing on and tries to pick it up. */
    public GameObject IdentifyObject()
    {
        /** It will make a OverlapSphere under you that will only check objects with the layers that are checked in the layersToPickup enum. */
        Collider[] allCol = Physics.OverlapSphere(new Vector3(transform.position.x, transform.position.y, transform.position.z)+transform.forward * 1.25f, 0.75f, layersToPickup);
        foreach (Collider col in allCol)
        {
            /** If the object you're standing on is not a ThrowableObject, it will not register it. */
            if (!col.transform.GetComponent<ThrowableObject>())
                continue;

            liftingObjectScript = col.transform.GetComponent<ThrowableObject>();
            return currentLiftingObject = col.transform.gameObject;
        }

        return this.gameObject;
    }
}
