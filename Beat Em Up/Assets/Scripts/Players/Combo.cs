﻿//Combo Class used to store the combos we use in the game

using UnityEngine;
using System.Collections;


public class Combo
{
    public string[] inputButtons;
    public AttackInfo attackName;
    public int attackType;

    //Function to add a combo with the values set
    public Combo(string[] keyCodeCombo, AttackInfo attackName, int attackType = 0)
    {
        this.inputButtons = keyCodeCombo;
        this.attackName = attackName;
        this.attackType = attackType;

    }
}

