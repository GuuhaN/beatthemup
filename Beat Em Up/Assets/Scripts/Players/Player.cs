﻿//Player class that each player in the game uses


using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BehaviorDesigner.Runtime;

public class Player : Character
{
    public int _playerID { get { return playerID; } protected set { playerID = value; } }
    [SerializeField, Range(1, 4)] public int playerID;
    [HideInInspector] public float rotationSpeed;
    [HideInInspector]public float characterBaseSpeed;
    [HideInInspector]public Color colorParticle;
    public int playerLives = 3;
    public float comboBonus { get; set; }
    private float inputHorizontal, inputVertical;
    public bool debugMode;
    public float playerAddPoints;
    private int playerPoints;
    private GameObject scoreDisplay;
    private float lastEnemyHealth, lastEnemyMaxHealth, showHealth, respawnTime, timeGrabbed, immunityTime = 3;
    private bool pressed = false;
    [HideInInspector]public float jumpForce = 300f;
    private int comboStreakPunch, comboStreakKick;
    protected List<Combo> comboList = new List<Combo>();
    protected List<Combo> keyCodeComboList = new List<Combo>();
    private float timeBetweenComboPunch = 1.6f, timeBetweenComboKick = 1.6f;
    [HideInInspector] public GameObject uiCross;
    public bool friendlyFire;
    [HideInInspector] public bool grounded { get; protected set; }
    protected AttackInfo[] attackList;
    [HideInInspector] public MoveSet AttackMoveSet;
    [HideInInspector] public int MoveSetInt;
    private Text characterEnemyDisplay;
    private string enemyText;
    private GameObject enemyHealthBar;
    private GameObject startPos;
    [SerializeField] private ParticleEffects grabEscapeParticle;
    public ChargeBar chargeBar;
    public Sprite UIface;

    //Called after moveset is loaded to load all basic stats for the player
    private void loadBehaviorDesignerStuff()
    {
        //Create an attackInfo list with all the attacks from the Behavior tree component at playerAttacks
        List<AttackInfo> test = (List<AttackInfo>)AttackMoveSet.GetComponent<BehaviorTree>().GetVariable("playerAttacks").GetValue();

        //Put it to an arary
        attackList = test.ToArray();

        //Set local variables for each variable in the player stats
        BehaviorTree statsTree = AttackMoveSet.GetComponent<BehaviorTree>();
        SharedInt localHealth = (SharedInt)statsTree.GetVariable("Health");
        SharedString localName = (SharedString)statsTree.GetVariable("Name");
        SharedInt localType = (SharedInt)statsTree.GetVariable("Type");
        SharedFloat localSpeed = (SharedFloat)statsTree.GetVariable("Speed");
        SharedFloat localRotationSpeed = (SharedFloat)statsTree.GetVariable("RotationSpeed");
        SharedFloat localJumpHeight = (SharedFloat)statsTree.GetVariable("JumpHeight");
        SharedFloat localGetUp = (SharedFloat)statsTree.GetVariable("GetUpTime");
        SharedFloat localGrabMulitplier = (SharedFloat)statsTree.GetVariable("GrabMultiplier");

        //Declare the Class variables using these localVariables
        characterHealth = localHealth.Value;
        characterMaxHealth = localHealth.Value;
        characterName = localName.Value;
        GetComponent<PlayerHealth>().maxHealth = localHealth.Value;
        characterKnockDownStrength = localType.Value;
        characterBaseSpeed = localSpeed.Value;
        rotationSpeed = localRotationSpeed.Value;
        jumpForce = localJumpHeight.Value;
        getUpTime = localGetUp.Value;
        characterGrabMultiplier = localGrabMulitplier.Value;

        //For each attack in the players attackList
        for (int i = 0; i < attackList.Length; i++)
        {
            //Create a new combo using the values from this attack (the buttons, the attack, the attack type)
            comboList.Add(new Combo(attackList[i].attackInputs, attackList[i], attackList[i].attackType)); //i = attacknumber
        }
        //Sort the attacks based on their input buttons length for the attackInput function to work then put this in the keyCodeComboList
        keyCodeComboList = comboList.OrderBy(x => -x.inputButtons.Length).ToList();

        //Set characterbasemovementspeed
        characterSpeed = characterBaseSpeed;
    }

    //Override start function for player specific
    protected override void Start()
    {
        base.Start();
        //Load AttackMoveSet for player equal to this player
        AttackMoveSet = FindObjectsOfType<MoveSet>().ToList().Find(x => (int)x.type == MoveSetInt);

        //Loads all the variables for the player
        loadBehaviorDesignerStuff();     

        //Enable rigidbody and set the next timeGrabbed to a random amount
        rigidBody.isKinematic = false;
        timeGrabbed = Random.Range(2f, 5.5f);
    }

    //load the player UI
    private void LoadPlayerUI()
    {

        //Load spawnpoint, enemyHealthbar, UiCross, Score, Chargebar, Textplayer and character display
        startPos = GameObject.Find("Spawn");       
        enemyHealthBar = GameObject.Find("EnemyHealth" + playerID);
        uiCross = GameObject.Find("CrossPlayer" + playerID);
        scoreDisplay = GameObject.Find("Score" + playerID);
        chargeBar = GameObject.Find("Chargebar" + playerID).GetComponent<ChargeBar>();
        GameObject.Find("TextPlayer" + playerID).GetComponent<Text>().text = characterName;
        characterEnemyDisplay = GameObject.Find("TextEnemy" + playerID).GetComponent<Text>();

        //Set your profile sprite as your players face sprite
        uiCross.transform.parent.GetComponent<Image>().sprite = UIface;

        //Loadbehavior stuff again
        loadBehaviorDesignerStuff();

        //Disable the cross until dead
        uiCross.SetActive(false);
    }


    protected override void Update()
        {

        if (!FindObjectOfType<PlayerJoining>().playerJoined[playerID - 1])
            return;

        base.Update();

        //if your variables are not loaded load them
        if (!enemyHealthBar)    
            LoadPlayerUI();

        //Set character health and combo bonus
        GetComponent<PlayerHealth>().currentHealth = characterHealth;
        GetComponent<PlayerHealth>().comboBonus = comboBonus;

        //Add points if the points are not added yet
        if (playerAddPoints > 0)
        {
            //Slowly add them rounded
            playerPoints += Mathf.CeilToInt(1 * Mathf.Abs((playerAddPoints * Time.deltaTime)));
            playerAddPoints -= Mathf.CeilToInt(1 * Mathf.Abs((playerAddPoints * Time.deltaTime)));
        }
        //ScoreDisplay format
        scoreDisplay.GetComponent<Text>().text = "" + playerPoints.ToString("000000");

        chargeBar.transform.position = transform.position + transform.up * 2.25f;

        //Cap the combobonus to 0 and 100
        if (comboBonus > 100)
            comboBonus = 100;
        if (comboBonus < 0)
            comboBonus = 0;

        //Check if your player is trying to grab 
        GrabObject();

        //If the showHealth is bigger than 0 show the health until its smaller or equal to 0
            if (showHealth > 0)
            {
            //Enable the healthbar form the enemy and set the size and health by the information you have
                enemyHealthBar.SetActive(true);
                characterEnemyDisplay.text = enemyText;
                enemyHealthBar.transform.parent.gameObject.SetActive(true);
                enemyHealthBar.GetComponent<RectTransform>().sizeDelta = new Vector2(lastEnemyHealth / lastEnemyMaxHealth * 49f, 9);
                showHealth -= Time.deltaTime;
            }

            //Remove the healthbar if no enemies where hit in the duration of it being active
            if (showHealth <= 0)
            {
                enemyHealthBar.SetActive(false);
                enemyHealthBar.transform.parent.gameObject.SetActive(false);
            }

            //If your characterattackcooldown is greater than 0 let it countdown
        if (characterAttackCoolDown > 0)
            characterAttackCoolDown -= Time.deltaTime;

        //If not see if you are not holding an object
        else
        {
            if (!throwScript.currentLiftingObject)
            {
                //Set lifting to false and characterspeed to the base. Apply rootmotion to false so you dont become sonic
                animatorController.SetBool("Lifting", false);
                characterSpeed = characterBaseSpeed;
                animatorController.applyRootMotion = false;
            }
        }

        //If immunity time is greater than 0 make player immume and let the player blink
        if (immunityTime > 0)
        {
            canGetHit = false;
            immunityTime -= Time.deltaTime;
            StartCoroutine(Blink(immunityTime));
        }
        else
            canGetHit = true;

        //If player is stunned make the animator play a stunned animation and count the stun down
        if (stunDuration >= 0)
        {
            isStunned = true;
            animatorController.SetBool("Stunned", true);
            stunDuration -= Time.deltaTime;
        }
        else
        {
            isStunned = false;
            animatorController.SetBool("Stunned", false);
        }

        //If player is not knockedout and not stunned and not grabbed
        if (!knockedOut && stunDuration <= 0 & !isGrabbed)
        {
            //See if player is using a weapon
            if (usingWeapon)
                currentWeapon();

            //If not and the player is not holding an object check if the player is is pressing any buttons to attack
            else if (!GetComponent<ThrowScript>().currentLiftingObject)
                CheckButtonsForAttacks();
        }

        //Count the combostreakpunch down
        if (comboStreakPunch > 0)
        {
            timeBetweenComboPunch -= Time.deltaTime;

            //Reset the streak if the player took too long to attack again
            if (timeBetweenComboPunch < 0)
                comboStreakPunch = 0;
        }

        //Count the combostreakkick down
        if (comboStreakKick > 0)
        {
            timeBetweenComboKick -= Time.deltaTime;

            //Reset the streak if the player took too long to attack again
            if (timeBetweenComboKick < 0)
                comboStreakKick = 0;
        }

        //If player is alive and not knockedout can attack and isnt stunned
            if (!hasDied && !knockedOut && stunDuration <= 0 && characterAttackCoolDown <= 0)
            {
            //If player has an object and pressed the throw button / pickup button
                if (Input.GetButtonDown("RBkey_P" + playerID) && !throwScript.GrabIdentifiedObject() && !weapon &&
                    (GetComponent<Player>() && characterAttackCoolDown <= 0))
                {
                //See if there are any pickups in range of your player
                    Collider[] hits = Physics.OverlapSphere(transform.position, 0.4f);
                    for (int i = 0; i < hits.Length; i++)
                    {
                    //Identify the PickUp type
                        if (hits[i].gameObject.GetComponent<PickUp>())
                        {
                        //If its a weapon set all the weapon variables
                            if (hits[i].gameObject.GetComponent<Weapon>() &&
                                !hits[i].gameObject.GetComponent<Weapon>().taken && !usingWeapon)
                            {                
                            //Set weapon position to your players hand (use weaponHand Script)                    
                                hits[i].transform.SetParent(weaponHand.transform);
                                weaponScript = hits[i].GetComponent<Weapon>();
                                weaponScript.gameObject.layer = gameObject.layer;
                                weaponScript.taken = true;
                                weaponScript.GetComponent<Rigidbody>().isKinematic = true;

                            //Disable the object pickup hitbox
                                foreach (BoxCollider box in weaponScript.GetComponents<BoxCollider>())
                                {
                                    if (!box.isTrigger)
                                        box.enabled = false;
                                }
                                weaponScript.GetComponent<Hitbox>().DisableHitboxInstant();
                                hits[i].transform.localPosition = weaponScript.offset;
                                hits[i].transform.localEulerAngles = weaponScript.rotationOffset;
                                usingWeapon = true;
                                weapon = weaponScript.gameObject;

                            //Update animator for the weapon
                                animatorController.SetTrigger("PickUp");
                        }
                            //Just play the animation if it wasnt a weapon
                            else
                                animatorController.SetTrigger("PickUp");

                            //Set a cooldown so the player cant move while picking up and cant attack
                            characterAttackCoolDown = 0.8f;
                            characterSpeed = 0f;

                            //if the pickup was a health pickup
                            if (hits[i].gameObject.GetComponent<Health>())
                            {
                            //Add the health
                                characterHealth += hits[i].gameObject.GetComponent<Health>().health;
                            //Cap the health if it reaches above maxhealth
                                if (characterHealth > characterMaxHealth)
                                    characterHealth = characterMaxHealth;

                                //Destroy the object and return
                                Destroy(hits[i].gameObject);
                                return;
                            }

                            //If the object was a points pickup
                            if (hits[i].gameObject.GetComponent<Points>())
                            {
                                //add the points and destroy the object
                                playerAddPoints += hits[i].gameObject.GetComponent<Points>().points;
                                Destroy(hits[i].gameObject);
                                return;
                            }
                        }
                    }
                }
            }

            //See if debugmode is enabled
            DebugMode();
    }

    private int throwStage;

    /** Function to grab an object and initialize the object you're holding. */
    protected void GrabObject()
    {
        /** If the player is affected by any of these effects, then the player cannot pick up an object. */
        if (isGrabbed || hasDied || knockedOut || isFlinched)
        {
            throwScript.ResetGrabIdentification();
            return;
        }

        /** If only the player presses the grab/throw button, if grabbing is true, if it is not doign any attacks or currently lifting any objects, it will continue the code. */
        if (Input.GetButtonDown("RBkey_P" + playerID) && !throwScript.grabbing && characterAttackCoolDown <= 0 && !throwScript.currentLiftingObject)
        {
            // Special moves with special animations without rootmotion provided by Ian < All animations
            
            if (!throwScript.GrabIdentifiedObject())
                return;

            animatorController.applyRootMotion = false;
            animatorController.SetFloat("Movement", 0);
            throwStage = 1;

            /** If the object is not a PhysicsObject kind, it will give the player new attacks that can only perform when you're grabbing. */
            if(throwScript.liftingObjectScript._ObjectType != ThrowableObject.ObjectType.PhysicsObject)
                CheckButtonsForGrabAttacks();
        }
        /** Extra check before even throwing an object. */
        else if (throwScript.grabbing && throwScript.currentLiftingObject)
        {
            /** The throw of a physics object */
            if (Input.GetButtonDown("RBkey_P" + playerID))
            {
                if (throwScript.liftingObjectScript._ObjectType != ThrowableObject.ObjectType.PhysicsObject)
                {
                    throwScript.ThrowIdentifiedObject();
                    return;
                }

                throwStage = 2;
            }

            if (Input.GetButton("RBkey_P" + playerID) && throwStage == 2)
                chargeBar.myImage.fillAmount += Time.deltaTime * .5f;

            if (Input.GetButtonUp("RBkey_P" + playerID) && throwStage == 2)
            {
                throwScript.ThrowIdentifiedObject();
                chargeBar.myImage.fillAmount = 0f;
                throwStage = 0;
            }
        }
    }

    //Called to execute normal attacks for the player
    private void CheckButtonsForAttacks()
    {
        //Execute the normal attacks that your player has access to
            AttackInput(0);
    }

    //Called when you grab someone to execute grab attacks
    private void CheckButtonsForGrabAttacks()
    {
        //Check for grab attacks
        AttackInput(1);
    }

    //Fixed update for rounding down velocity values and executing the movement with physics
    private void FixedUpdate()
    {
        if (rigidBody.velocity.sqrMagnitude > 5)
            rigidBody.velocity = new Vector3(rigidBody.velocity.x * 0.50f, rigidBody.velocity.y, rigidBody.velocity.z * 0.50f);

        //If not knocked out and you are not grabbed 
        if (!knockedOut && !isGrabbed)
        {
            rigidBody.velocity = new Vector3(0, rigidBody.velocity.y, 0);
            
            //If you are not stunned and you can attack execute the move
            if (stunDuration <= 0 && characterAttackCoolDown <= 0)
                Move();
        }
    }

    //Attack function used to decide which attack you are using give your type of attack (0 = normal, 1 = grab , 2 = Weapon) if weapon id is -1 its not a weapon attack otherwise give the weapon id from the weapon script
    private void AttackInput(int TypeOfAttack, int weaponID = -1)
    {
        //Raycast down to see if you are standing on the ground
        if (Physics.Raycast(transform.position + new Vector3(0, 0.1f, 0), Vector3.down, 0.2f))
        {
            //Look through each attack that the player knows
            for (int i = 0; i < keyCodeComboList.Count; i++)
            {
                //Look if attack type is 0 (0 is normal attacks)
                if (keyCodeComboList[i].attackType == TypeOfAttack)
                {
                    //if you are allowed to attack
                    if (characterAttackCoolDown <= 0)
                    {
                        //check if you are going fast enough to execute the move if it has a movement requirement
                        if ((Mathf.Abs(inputHorizontal) + Mathf.Abs(inputVertical)) >= keyCodeComboList[i].attackName.attackMovementCap)
                        {
                            //if your combobonus is higher than the bonus it takes to use the attack
                            if (comboBonus >= keyCodeComboList[i].attackName.attackComboPointsRequired)
                            {
                                //If the button has atleast one button
                                if (keyCodeComboList[i].inputButtons == null)
                                {
                                    Debug.LogError("Inputbuttons array is null");
                                    return;
                                }
                                //foreach buttoninput string in the attack check if its true otherwise return
                                for (int j = 0; j < keyCodeComboList[i].inputButtons.Length; j++)
                                {
                                    //check if the string is not null
                                    if (!string.IsNullOrEmpty(keyCodeComboList[i].inputButtons[j]))
                                    {
                                        
                                        if (keyCodeComboList[i].inputButtons.Length == 1)
                                        {
                                            if (!Input.GetButtonDown(keyCodeComboList[i].inputButtons[j] + "key_P" + playerID) || !Input.GetButton(keyCodeComboList[i].inputButtons[j] + "key_P" + playerID))
                                            {
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (!Input.GetButton(keyCodeComboList[i].inputButtons[j] + "key_P" + playerID))
                                            {
                                                break;
                                            }
                                        }

                                        if (j == keyCodeComboList[i].inputButtons.Length - 1)
                                        {
                                            //If you dont have a eapon or your attack contains a weapon attack
                                            if (weaponID < 0 || weaponID == keyCodeComboList[i].attackName.weaponAttackType)
                                            {
                                                //execute the attack and remove the combobonus required from your points  
                                                if (keyCodeComboList[i].attackName.attackName == AttackList.attackNames.LowKick.ToString())
                                                {
                                                    //Set values if this attack was a kick for the combokick
                                                    timeBetweenComboKick = 1.6f;
                                                    comboStreakKick++;
                                                    if (comboStreakKick == 4)
                                                    {
                                                        //Execute comboattack if this amount is reached
                                                        comboStreakKick = 0;

                                                        //Find the comboattack for kicking
                                                        for (int k = 0; k < keyCodeComboList.Count; k++)
                                                        {
                                                            if (keyCodeComboList[k].attackName.attackName == AttackList.attackNames.ComboBreakerKick.ToString())
                                                            {
                                                                //Execute it here then reset the combo by the amount the attack costs
                                                                DoAttack(keyCodeComboList[k].attackName);
                                                                comboBonus -= keyCodeComboList[k].attackName.attackComboPointsRequired;
                                                                return;
                                                            }
                                                        }
                                                    }
                                                }
                                                //See if your attack was a punch
                                                else if (keyCodeComboList[i].attackName.attackName == AttackList.attackNames.Punch.ToString())
                                                {
                                                    //Add punch combo time
                                                    timeBetweenComboPunch = 1.6f;
                                                    comboStreakPunch++;
                                                    if (comboStreakPunch == 4)
                                                    {
                                                        //Execute punch combo attack
                                                        comboStreakPunch = 0;
                                                        for (int k = 0; k < keyCodeComboList.Count; k++)
                                                        {
                                                            //Find the punch combo attack
                                                            if (keyCodeComboList[k].attackName.attackName == AttackList.attackNames.ComboBreakerPunch.ToString())
                                                            {
                                                                //execute the punch combo attack
                                                                DoAttack(keyCodeComboList[k].attackName);

                                                                //Remove the combo bonus required for this attack
                                                                comboBonus -= keyCodeComboList[k].attackName.attackComboPointsRequired;
                                                                return;
                                                            }
                                                        }
                                                    }
                                                }
                                                //If no combo attack was done



                                                //Do the normal attack that was completed
                                                DoAttack(keyCodeComboList[i].attackName);

                                                //Remove the combo bonus for this attack
                                                comboBonus -= keyCodeComboList[i].attackName.attackComboPointsRequired;
                                                //Debug.Log(keyCodeComboList[i].attackName.attackName + " Attack Completed");
                                                break;
                                            }    
                                        }
                                        //Debug.LogError("Key is not pressed");
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
    }

    #region functies

    //Movement of the player
    protected virtual void Move()
    {
        //If you are not holding an object
        if (throwScript.currentLiftingObject && throwScript.grabbing) return;

        //Reset player velocity when not in ragdoll
        rigidBody.velocity = new Vector3(0, rigidBody.velocity.y, 0);

        //Player inputs
        inputHorizontal = Input.GetAxis("Horizontal_P" + playerID);
        inputVertical = Input.GetAxis("Vertical_P" + playerID);

        //calculate movement
        Vector3 moveDirection = new Vector3(characterSpeed * inputHorizontal, 0, characterSpeed * inputVertical);
        transform.TransformDirection(moveDirection);
        rigidBody.MovePosition(transform.position + moveDirection * characterSpeed * Time.deltaTime);
        Vector3 rotation = Vector3.RotateTowards(transform.forward, moveDirection, rotationSpeed * Time.deltaTime, 0.0f);
        transform.rotation = Quaternion.LookRotation(rotation);

        //Send animator your speed rounded above 0
        animatorController.SetFloat("Movement", (Mathf.Abs(inputHorizontal) + Mathf.Abs(inputVertical)));
    }

    //function to use attack (DoAttack())
    public override void DoAttack(AttackInfo attack)
    {
        //Set rootmotion when attacking
        animatorController.applyRootMotion = true;
        DisableActiveHitboxes();

        //Set character movementspeed to the movementspeed of the attack
        characterSpeed = attack.attackMovementSpeed;
        
        //if you have a weapon override the animations to weapon animations
        if (usingWeapon)
        {
            animatorController.SetInteger("WeaponType", weaponScript.id);
                base.DoAttack(attack);
            return;
        }

        //if not execute the normal animation
        else
        {
            base.DoAttack(attack);
        }
    }

    //Called on collision with objects
    protected override void OnCollisionEnter(Collision collider)
    {
        base.OnCollisionEnter(collider);
        //If this object is an object layer and you are not knocked out, the object is not thrown by your player and not thrown by a player (unless friendlyfire is on)
        if (collider.gameObject.layer == 12 && !knockedOut && collider.gameObject.GetComponent<PhysicsObject>().playerID != 0 && collider.gameObject.GetComponent<PhysicsObject>().playerID != playerID && canGetHit && friendlyFire)
        {
            //Set variable in playerHealth and play the hit sound
            GetComponent<PlayerHealth>().hitByAnything = true;
            soundEffects.PlaySound(SoundEffects.soundNames.Punch);

            //Get values from the object
            int ID = collider.gameObject.GetComponent<PhysicsObject>().playerID;
            int damage = collider.gameObject.GetComponent<PhysicsObject>().objectDamage;
            float speed = collider.gameObject.GetComponent<PhysicsObject>().currentSpeed;

            //Take damage based on the object that hit you
            TakeDamage(damage);

            //Add force and find the player that hit you with the object
            transform.GetChild(1).GetComponent<Rigidbody>().AddRelativeForce(-transform.forward * (speed * 1200));

            //Give this player your health for the health display in the UI
            GameObject.Find("Player" + ID).GetComponent<Player>().GiveMyEnemyHealth(characterHealth, characterMaxHealth, 0, characterName);

            //Enable ragdoll and disable hitboxes
            DisableActiveHitboxes();
            EnableRagdoll();

            //Check if player is alive then start GetUp coroutine
            if (characterHealth > 0)
                StartCoroutine(RagdollGetUp(getUpTime));

            //Spawn each hitparticle on collision position
            Vector3 colliderPos = collider.transform.position;
            foreach (ParticleEffects party in hitParticle)
                Instantiate(party.gameObject, new Vector3(Mathf.Lerp(colliderPos.x, transform.position.x, 0.5f), colliderPos.y, Mathf.Lerp(colliderPos.z, transform.position.z, 0.5f)), Quaternion.identity);
        }
    }


    protected override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        //Only called if alive
        if (!hasDied)
        {
            //Look for returns

            //If its a weapon
            if (collider.gameObject.layer == LayerMask.NameToLayer("Weapon"))
                return;
            
            //if its not a hitbox
        if (!collider.gameObject.GetComponent<Hitbox>())
            return;

        //if the hitbox was already triggered by this attack
        if (collider.gameObject.GetComponent<Hitbox>().colliderList.Contains(gameObject.GetComponent<Collider>()))
            return;

        //Add hitbox to the list of collider that already hit you
        collider.gameObject.GetComponent<Hitbox>().colliderList.Add(gameObject.GetComponent<Collider>());
        Transform colliderRoot = collider.transform.root;
            // if its not yourself  
            if (colliderRoot.name != transform.root.name)
            {
                //if its a player and friendlyfire is off
                if (colliderRoot.GetComponent<Player>())
                    if (!friendlyFire)
                        return;

                //If its possible to get hit
                if (!knockedOut && canGetHit)
                {
                    //Apply rootmotion and set animations for the impact
                    animatorController.applyRootMotion = true;
                    animatorController.Play("TakePunch", 0, 0);
                    animatorController.SetInteger("HitRandom", Random.Range(0, 5));

                    //Change character attackcooldown to prevent attacks from the player
                    characterAttackCoolDown = 1.3f;
                    
                    //Look towards impact location
                    Vector3 rot = new Vector3(collider.transform.position.x, this.transform.position.y, collider.transform.position.z);
                    transform.LookAt(rot);

                    //Remove a part of your combo
                    comboBonus -= 10;

                    //Disable your hitboxes
                    DisableActiveHitboxes();

                    //If collider stays on do not disable the hitbox of the attack that hit you
                    if (collider.GetComponent<Hitbox>().colliderStaysOn)
                        collider.enabled = true;
                    else
                        collider.enabled = false;

                    //Play sound and set variable of hit in playerHealth
                    GetComponent<PlayerHealth>().hitByAnything = true;
                    soundEffects.PlaySound(collider.transform.GetComponent<Hitbox>().soundName);

                    int damage;
                    Vector3 colliderPos = collider.transform.position;

                    //Spawn every hitparticle on the hit position
                    foreach (ParticleEffects party in hitParticle)
                        Instantiate(party.gameObject, new Vector3(Mathf.Lerp(colliderPos.x, transform.position.x, 0.5f), colliderPos.y, Mathf.Lerp(colliderPos.z, transform.position.z, 0.5f)), Quaternion.identity);

                    //If a player hit you
                    if (colliderRoot.GetComponent<Player>())
                    {
                        Player player = colliderRoot.GetComponent<Player>();
                        damage = player.characterDamage;

                        //Take damage and give your health to the player
                        TakeDamage(damage);
                        player.GiveMyEnemyHealth(characterHealth, characterMaxHealth, 0 , characterName);
                    }

                    //If hit by an enemy
                    else
                    {
                        //Get character damage and take the damage
                        damage = colliderRoot.GetComponent<EnemyData>().characterDamage;
                        TakeDamage(damage);
                    }
                   

                    //If the attack strength is higher or equal to your characters strength
                    if (collider.GetComponent<Hitbox>().knockDownStrength >= characterKnockDownStrength)
                    {
                        //If the stunduration is bigger than 0
                        if (collider.GetComponent<Hitbox>().stunDuration > 0)
                        {
                            //If character is not stunned stun the character
                            if (stunDuration <= 0)
                                stunDuration = collider.GetComponent<Hitbox>().stunDuration;

                        }

                        //If this attack does not stun knockdown your character
                        else
                        {
                            //Enables ragdoll
                            EnableRagdoll();

                            //Add ragdoll force
                            transform.GetChild(1).GetComponent<Rigidbody>().AddRelativeForce(new Vector3(collider.GetComponent<Hitbox>().Physics.x, collider.GetComponent<Hitbox>().Physics.y, 0));

                            //if player didnt die restart getup coroutine
                            if (characterHealth > 0)
                                StartCoroutine(RagdollGetUp(getUpTime));
                        }
                    }
                }
            }
        }
    }

    public void ChargeObject(float newSpeed)
    {
        characterSpeed = newSpeed;
    }

    //Weapon in hand
    private void currentWeapon()
    {
        //Refresh the animator value for weapon
        animatorController.SetBool("Weapon", usingWeapon);

        //Reset weapon
        if (!weaponScript)
        {
            usingWeapon = false;
            animatorController.SetBool("Weapon", usingWeapon);
        }

        //Throw weapon away
        if (Input.GetButtonDown("RBkey_P" + playerID) && characterAttackCoolDown <= 0)
        {
            weaponScript.gameObject.layer = 17;
            weaponScript.taken = false;
            weaponScript.GetComponent<Rigidbody>().isKinematic = false;
            throwScript.ThrowIdentifiedObject();
            foreach (BoxCollider box in weaponScript.GetComponents<BoxCollider>())
            {
                if (!box.isTrigger)
                    box.enabled = enabled;
            }
            weaponScript.gameObject.transform.SetParent(null);
            usingWeapon = false;
            weapon = null;
            animatorController.SetBool("Weapon", usingWeapon);
        }
        //Overwrite attacks with the weapon values using AttackInput(2 and give the weaponScript)
            AttackInput(2, weaponScript.id);
        //weaponfunctie hier
    }

    //Automaticly called when characters die
    protected override void Die()
    {
        //Only run this once
        if (!hasDied)
        {
            base.Die();
            playerLives--;
            hasDied = true;
            uiCross.SetActive(true);

            //Play deathsound and spawn a particle in the color of the player
            soundEffects.PlaySound(SoundEffects.soundNames.Respawn);
            GameObject party = (GameObject)Instantiate(deadParticle[0].gameObject, transform.GetChild(1).position, deadParticle[0].transform.rotation);
            party.GetComponent<ParticleEffects>().particleColor = colorParticle;

            //Reset weapon if the player had a weapon
            if (weapon)
            {
                weapon.GetComponent<Rigidbody>().isKinematic = false;
                weapon.transform.SetParent(null);
                usingWeapon = false;
                weapon = null;
            }

            //Refresh animator value of weapon
            animatorController.SetBool("Weapon", usingWeapon);

            //If player has lives left respawn
            if (playerLives > 0)
                StartCoroutine(Respawn());
        }
    }
    //Respawn player coroutine
    private IEnumerator Respawn()
    {
        //Set respawntime on 5 seconds
        respawnTime = 5f;

        //Kill character 
        characterHealth = 0;

        while (true) { 
            //Slowly heal the character back to full hp
            yield return new WaitForSeconds(1f);
            respawnTime--;
            if (characterHealth < characterMaxHealth)
                characterHealth += (characterMaxHealth /10);

            //WHen full hp set the bones back to their defaults from the dead layer
            if (characterHealth >= characterMaxHealth)
            {
                foreach (Transform child in transform)
                {
                    //If its a hitbox set the hitbox back to the hitbox layer (player layer)
                    if (child.name != "hitbox")
                        child.gameObject.layer = 10;
                    else
                        child.gameObject.layer = 8;
                }

                //Set all the following values back to reset and prevent bugs
                hasDied = false;
                characterHealth = characterMaxHealth;
                uiCross.SetActive(false);
                gameObject.SetActive(true);
                transform.position = startPos.transform.position;
                transform.GetChild(1).position = transform.position;
                DisableRagdoll();
                gameObject.layer = 8;
                knockedOut = false;
                stunDuration = 0f;
                isGrabbed = false;
                animatorController.Play("Movement");
                rigidBody.velocity = Vector3.zero;

                //Call to reset the bones to original position
                FixAllBones();

                //Break coroutine afterwards
                break;
            }
        }
        yield return null;
    }

    //Take health to show beneath the player health in UI for the last hit enemy
    public void GiveMyEnemyHealth(float health, float maxHealth, int points, string enemyText)
    {
        this.enemyText = enemyText;
        //Give health and max health
        lastEnemyHealth = health;
        lastEnemyMaxHealth = maxHealth;

        //Add combo points
        comboBonus += 10;

        //Duration that text is shown
        showHealth = 2f;

        //Add points if enemy died
        if (health <= 0)
            playerAddPoints = points;
    }
    #endregion
    //Enable ragdoll and drop weapon, also resets the combo to zero for the players
    public override void EnableRagdoll()
    {
        base.EnableRagdoll();
        comboBonus = 0;
        usingWeapon = false;
        weapon = null;
    }

    //Disable Ragdoll with added changes for player specificly (reset attack and add immunity)
    protected override void DisableRagdoll()
    {
        base.DisableRagdoll();
        rigidBody.velocity = new Vector3(0, rigidBody.velocity.y, 0);
        characterAttackCoolDown = 0f;
        immunityTime = 3f;
    }

    //Called when the player is disabled to prevent physics, wrong movement speed and other bugs
    public void DisableObject()
    {
        rigidBody.velocity = Vector3.zero;
        characterSpeed = characterBaseSpeed;
        DisableActiveHitboxes();
        isGrabbed = false;
        gameObject.SetActive(false);
    }

    //Player blink on hit and spawn to show that taking damage is impossible for the time being
    private IEnumerator Blink(float waitTime)
    {
        float endTime = Time.time + waitTime;
        while (Time.time < endTime)
        {
            //Disable each skinnedmesh for total invisibility
            foreach (Transform child in transform)
                if (child.GetComponent<SkinnedMeshRenderer>())
                    child.GetComponent<SkinnedMeshRenderer>().enabled = false;
            yield return new WaitForSeconds(0.2f * (immunityTime / 2));

            //Enable them again after a small time
            foreach (Transform child in transform)
                if (child.GetComponent<SkinnedMeshRenderer>())
                    child.GetComponent<SkinnedMeshRenderer>().enabled = true;
            yield return new WaitForSeconds(0.2f * (immunityTime / 2));
        }
        yield return null;
    }

    //A debug mode that changes various variables of the player
    private void DebugMode()
    {
        if (debugMode)
        {
            characterHealth = 99;
            characterAttackCoolDown = 0;
            rotationSpeed = 1000;
            playerLives = 2;
            GetComponent<PlayerHealth>().maxHealth = 99;
            characterKnockDownStrength = 99;
            characterMaxHealth = 99;
            characterDamage = 99;
            jumpForce = 750;
            stunDuration = 0;
            timeBetweenComboKick = 0;
            timeBetweenComboPunch = 0;
        }
    }
}