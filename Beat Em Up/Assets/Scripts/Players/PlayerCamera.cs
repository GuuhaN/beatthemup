﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

/**<summary
 * 
 * This Class is to handle the segments of the level and camera.
 * 
 * </summary>*/

[RequireComponent(typeof(Camera))]
public class PlayerCamera : MonoBehaviour
{
    public int viewSegment { get; set; }
    public float distanceSet { get; protected set; }
    public float depthDistance { get; protected set; }
    public bool unlockedCamera { get; private set; }

    public List<Player> playersAlive = new List<Player>();
    public List<LevelGroundData> levelGroundData
    {
        get { return _levelGroundData; }
        set { _levelGroundData = value; }
    }

    [SerializeField] private Image goImage, endingImage;
    [SerializeField] private AudioClip[] cameraAudioClips;
    [SerializeField] private Collider wreckageDestroyer;

    private float currentCameraZdepth, currentCameraYheight;
    private bool played;
    private bool gameDone, coroutineStarted;

    private List<LevelGroundData> _levelGroundData = new List<LevelGroundData>();
    private PlayerJoining playerJoining;
    private AI_Director aiDirector;
    private TomToggles tomToggles;

    private AudioSource cameraAudioSource;
    private SpawnWaves spawnWaves;
    private Transform cameraTransform;

    /** Callling the SetCameraProperties just for initializing */
    private void Awake()
    {
        SetCameraProperties();
    }

    /** Initializing the components and starting values */
    protected void SetCameraProperties()
    {
        levelGroundData = FindObjectsOfType<LevelGroundData>().OrderBy(x => x.transform.position.x).ToList();
        playerJoining = FindObjectOfType<PlayerJoining>();
        aiDirector = FindObjectOfType<AI_Director>();
        tomToggles = FindObjectOfType<TomToggles>();
        spawnWaves = FindObjectOfType<SpawnWaves>();
        viewSegment = 1; // The very start of the level
        cameraAudioSource = GetComponent<AudioSource>();
        StartCoroutine(BlinkingGo());
        cameraTransform = transform;
        currentCameraZdepth = cameraTransform.position.z;
        currentCameraYheight = cameraTransform.position.y - 3f;
    }

    /** Calling the functions that has to be updated */
    protected void FixedUpdate()
    {
        SetCameraMovement();
        SetCameraDepthHeightMovement();
    }

    /** When a segment of the game has been finished, this will execute to point that the players can move along */
    protected IEnumerator BlinkingGo()
    {
        while (true)
        {
            goImage.enabled = !goImage.enabled;
            yield return new WaitForSeconds(1f);
        }
    }

    /** When the level game ends, this will trigger which will cause a screen that tells you the game is over */
    public IEnumerator EndingLevel()
    {
        /** DEBUG PURPOSE | Called when the skipLevel toggle has been turned on */
        if (!tomToggles.skipLevel)
        {
            if (viewSegment < levelGroundData.Count)
                yield break;
        }

        /** To prevent the coroutine from starting more than once */
        coroutineStarted = true;
        gameDone = true;
        float imageAlpha = endingImage.color.a;
        /** To have the effect of the ending screen */
        while (true)
        {
            imageAlpha += Time.deltaTime;
            endingImage.color = new Color(255, 255, 255, imageAlpha);
            if (imageAlpha >= 1)
            {
                float textAlpha = endingImage.transform.GetChild(0).GetComponent<Text>().color.a;
                textAlpha += Time.deltaTime/2.5f;
                endingImage.transform.GetChild(0).GetComponent<Text>().color = new Color(255, 255, 255, textAlpha);
                if (textAlpha < 1)
                    if (!cameraAudioSource.isPlaying)
                        cameraAudioSource.PlayOneShot(cameraAudioClips[0]);
                if (textAlpha >= 1)
                {
                    SceneManager.LoadScene(0);
                    tomToggles.skipLevel = false; // DONT REMOVE or there will be an infinite loop.
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    //Unlocked Camera functionality for Depth and Height 
    protected void SetCameraDepthHeightMovement()
    {
        //Finds all players alive ( before permadeath )
        if (!playerJoining.players.Any())
            return;

        playersAlive = playerJoining.players.ToList().FindAll(x => x.playerLives > 0 && !x.hasDied);

        /** This will follow the players on the depth and height. This will completely ignore the width */
        foreach (Player t in playersAlive)
        { 
            depthDistance = Vector3.Distance(new Vector3(0, 0, cameraTransform.position.z),
                new Vector3(0, 0, currentCameraZdepth + t.transform.position.z));
            if (currentCameraZdepth + t.transform.position.z >= cameraTransform.position.z + 1.5f)
                cameraTransform.position += Vector3.forward*Time.deltaTime*depthDistance/3;
            if (currentCameraZdepth + t.transform.position.z <= cameraTransform.position.z - 1.5f)
                cameraTransform.position -= Vector3.forward*Time.deltaTime*depthDistance/3;
            if (currentCameraYheight + t.transform.position.y >= cameraTransform.position.y + .5f)
                cameraTransform.position += Vector3.up*Time.deltaTime*depthDistance/3;
            if (currentCameraYheight + t.transform.position.y <= cameraTransform.position.y - .5f)
                cameraTransform.position -= Vector3.up*Time.deltaTime*depthDistance/3;
        }
    }

    // Adds camera screen shake and can be accessed by every event who calls this Coroutine
    // SetScreenShake(1f, 0.04f) < good values
    public IEnumerator SetScreenShake(float shakeIntensity, float shakeDecay)
    {
        Vector3 originalPosition = cameraTransform.position;
        shakeIntensity = shakeIntensity/10;
        shakeDecay = shakeDecay/10;
        if (shakeIntensity <= 0.05f)
            yield break;

        while (shakeIntensity > 0)
        {
            transform.position = originalPosition + Random.insideUnitSphere * shakeIntensity;
            shakeIntensity -= shakeDecay;
            yield return new WaitForEndOfFrame();
        }
    }

    //Locked Camera functionality
    protected void SetCameraMovement()
    {
        /** If all the enemies in the current segment are dead, this will continue and unlock the camera */
        if (spawnWaves.currentEnemies <= 0 && playerJoining.playerJoined.Any())
        {
            if (spawnWaves.enemiesToSpawn.Count > 0)
            {
                for (int i = 0; i < spawnWaves.enemiesToSpawn.Count; i++)
                    if (spawnWaves.enemiesToSpawn[i] == null)
                        unlockedCamera = true;
            }
            else
                unlockedCamera = true;
        }
        else
            unlockedCamera = false;

        goImage.gameObject.SetActive(false);

        if (!unlockedCamera)
            return;

        goImage.gameObject.SetActive(true);
        if (spawnWaves.currentEnemies <= 0 && !coroutineStarted)
            StartCoroutine(EndingLevel());

        if (gameDone)
            return;

        /** To prevent the sound from playing when you startup the game */
        if (!cameraAudioSource.isPlaying && !played && Time.realtimeSinceStartup > 5f)
        {
            cameraAudioSource.PlayOneShot(cameraAudioClips[1]);
            played = true;
        }

        /** This will only activate when the camera is unlocked. This will follow the players ONLY on the X axis. 
            The camera will move when SOMEONE is at the right side of the level, it will scale on the amount of players ( speed of camera ). */
        foreach (Player t in playerJoining.players)
        {
            if(t.transform.position.x >= cameraTransform.position.x + 2)
            {
                distanceSet = Vector3.Distance(new Vector3(cameraTransform.position.x, 0,0), new Vector3(t.transform.position.x - 1.5f, 0, 0));
                if (cameraTransform.position.x < levelGroundData[viewSegment].segmentBoundsCenterX)
                    cameraTransform.position += Vector3.right*Time.deltaTime*distanceSet;
                else
                {
                    /** This will lock the camera when you reached the next segment of the game. */
                    if (viewSegment < levelGroundData.Count)
                    {
                        unlockedCamera = false;
                        played = false;
                        viewSegment++;
                        StartCoroutine(spawnWaves.SpawnEnemies());  
                        break;
                    }
                }
            }
        }
    }
}