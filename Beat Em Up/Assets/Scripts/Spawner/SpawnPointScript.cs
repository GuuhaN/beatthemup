﻿//Base class for SpawnPoints to set all values for the spawner

using UnityEngine;
using System.Collections;

public class SpawnPointScript : MonoBehaviour {
    public int enemyType;
    public string enemyName;
    public int enemyDrop;
    public float enemyDropChance;
    public int enemySpawnSegment;
    public float enemySpawnDelay;
    public int enemyRequiredKillsToSpawn;

    private Color[] gizmoColors;

    void OnDrawGizmos()
    {
        //Show gizmo color in scene view for debugging and spawn editing
        gizmoColors = FindObjectOfType<SpawnWaves>().enemyColors;
        Gizmos.color = gizmoColors[enemyType];
        Gizmos.DrawSphere(transform.position, 0.5f);
    }
}
