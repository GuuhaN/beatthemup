﻿//Editor for the SpawnWaves system

#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(SpawnWaves))]
public class SpawnWavesEditor : Editor {

    private bool editMode;
    private bool dropsItem;
    private GameObject spawnPointObject;
    private SpawnWaves script;

    private string[] dropableItems;
    private int selectedDrop;
    private float dropChance = 0f;
    private string[] enemyTypes;
    private int typeSelector;
    private string enemyName;
    private int spawnSegment = 1;
    private float spawnDelay = 0.2f;
    private int requiredKillsToSpawn = 0;


    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        //Press this button to enter Editor mode which allows you to put spawnpoints
        editMode = EditorGUILayout.Toggle("Editor Mode", editMode);

        //set the color length equal to how many enemies there are and set random color values if they are not declared
        script = FindObjectOfType<SpawnWaves>();
        //Load the names of the enemys and items
        enemyTypes = new string[script.enemyPrefabs.Length];
        dropableItems = new string[script.itemsToDrop.Length];
        for (int j = 0; j < dropableItems.Length; j++)
        {
            dropableItems[j] = script.itemsToDrop[j].gameObject.name;
        }

        for (int i = 0; i < enemyTypes.Length; i++)
        {
            enemyTypes[i] = script.enemyPrefabs[i].name;
            //Set the length of the colors if its not equal to the enemy objects length
            if (script.enemyColors.Length != script.enemyPrefabs.Length)
            {
                script.enemyColors = new Color[script.enemyPrefabs.Length];
            }
            //If the color is empty set a random new color
            if (script.enemyColors[i] == Color.clear)
                script.enemyColors[i] = new Color(Random.value, Random.value, Random.value, 0.85f);
        }

        //Editor if editmode is on
        if (editMode)
        {
            LevelData levelData = FindObjectOfType<LevelData>();
            //editor view
            GUILayout.Space(20);
            GUILayout.BeginHorizontal();
            //Enemy Object field
            EditorGUILayout.LabelField("Enemy Type", EditorStyles.boldLabel);
            typeSelector = EditorGUILayout.Popup(typeSelector, enemyTypes, GUILayout.Width(200));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            //Enemy Name (custom string input)
            EditorGUILayout.LabelField("Enemy Name", EditorStyles.boldLabel);
            enemyName = EditorGUILayout.TextField(enemyName, GUILayout.Width(200));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            //Enemy level segment that it spawns it, use levelData.maxSegments for dynamic loading or a hardcoded value
            EditorGUILayout.LabelField("Enemy SpawnSegment", EditorStyles.boldLabel);
            spawnSegment = EditorGUILayout.IntSlider(spawnSegment, 1, 10/*levelData.maxSegments dynamic amount of levels */);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            //Delay before it spawns once entering the right SpawnSegment
            EditorGUILayout.LabelField("Enemy SpawnDelay", EditorStyles.boldLabel);
            spawnDelay = EditorGUILayout.Slider(spawnDelay, 0, 30);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            //Kills Required in this segment before it spawns
            EditorGUILayout.LabelField("Required Kills To Spawn", EditorStyles.boldLabel);
            requiredKillsToSpawn = EditorGUILayout.IntSlider(requiredKillsToSpawn, 0, 10);
            GUILayout.EndHorizontal();
   
            //search all enemy points and display them for each segment again change the hardcoded value to levelData.maxSegments for a dynamic value
            int[] segmentEnemies = new int[10/*levelData.maxSegments*/];
            SpawnPointScript[] enemies = FindObjectsOfType<SpawnPointScript>();
            for (int i = 0; i < 10/*levelData.maxSegments*/; i++)
            {
                //Find all enemies in this spawnSegment
                for (int j = 0; j < enemies.Length; j++)
                {
                    if (enemies[j].enemySpawnSegment == i + 1)
                    {
                        //Add them to the counter and do this for each segment that exists
                        segmentEnemies[i]++;
                    }
                }
                EditorGUILayout.LabelField("Total Enemies Segment " + (i+1) + " : " + segmentEnemies[i], EditorStyles.boldLabel);
            }

            GUILayout.Space(30);
            //Display dropItems (Toggle this to make an enemy drop an item)
            dropsItem = EditorGUILayout.Toggle("DropsItem", dropsItem);
            if (dropsItem)
            {
                GUILayout.BeginHorizontal();
                //Select a drop for the enemy
                EditorGUILayout.LabelField("Enemy Drops", EditorStyles.boldLabel);
                selectedDrop = EditorGUILayout.Popup(selectedDrop, dropableItems, GUILayout.Width(200));
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                //Give a percentage chance for the enemy to drop this object
                EditorGUILayout.LabelField("Enemy Chance of dropping", EditorStyles.boldLabel);
                dropChance = EditorGUILayout.Slider(dropChance, 0, 100);
                GUILayout.EndHorizontal();
            }
            Repaint();
        }    
    }
    private void OnSceneGUI()
    {
        if (editMode)
        {
            //if there is no spawnpointObject take the one from SpawnWaves script
            if (!spawnPointObject)
                spawnPointObject = FindObjectOfType<SpawnWaves>().spawnPointObjectEmpty.gameObject;

            //Use this to receive mouse inputs
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

            //check if any mouse button is pressed
            if (Event.current.type == EventType.MouseDown)
            {
                //if left mouse button place a spawnpoint
                if (Event.current.button == 0)
                {
                    Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

                    RaycastHit[] hit;
                    hit = Physics.RaycastAll(ray);
                    {
                        for (int i = 0; i < hit.Length; i++)
                        {
                            //If your mouse hits a meshcollider on click spawn a spawnpoint here
                            if (hit[i].collider.GetComponent<MeshCollider>())
                            {
                                //Set all the values used above into the newly created spawnpoint
                                GameObject newSpawnPoint = Instantiate(spawnPointObject) as GameObject;
                                SpawnPointScript newSpawnPointScript = newSpawnPoint.GetComponent<SpawnPointScript>();                    
                                newSpawnPointScript.enemyType = typeSelector;
                                if (dropsItem)
                                {
                                    newSpawnPointScript.enemyDrop = selectedDrop;
                                    newSpawnPointScript.enemyDropChance = dropChance;
                                }
                                else
                                {
                                    newSpawnPointScript.enemyDrop = -1;
                                    newSpawnPointScript.enemyDropChance = 0;
                                }
                                newSpawnPointScript.enemyName = enemyName;
                                newSpawnPointScript.enemySpawnSegment = spawnSegment;
                                newSpawnPointScript.enemySpawnDelay = spawnDelay;
                                newSpawnPointScript.enemyRequiredKillsToSpawn = requiredKillsToSpawn;
                                newSpawnPoint.transform.position = hit[i].point;

                                //If you have a parent object already child the new spawnpoint in the parent
                                if (GameObject.Find("SpawnPointParent"))
                                    newSpawnPoint.transform.SetParent(GameObject.Find("SpawnPointParent").transform);

                                //Otherwise Create a new spawnPointparent this is used to easily reset all the spawnpoints
                                else
                                {
                                    new GameObject("SpawnPointParent");
                                    newSpawnPoint.transform.SetParent(GameObject.Find("SpawnPointParent").transform);
                                }
                            }
                        }                    
                    }
                }

                //if right mouse button remove hovered spawnpoint
                if (Event.current.button == 1)
                {
                    Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 16))
                    {
                        //Only works on spawnpoints if you remove this you will destroy everything
                        if (hit.collider.GetComponent<SpawnPointScript>())
                        {
                            //Destroy the object spawnpoint
                            DestroyImmediate(hit.collider.gameObject);
                        }
                    }
                }
            }
        }
    }
}
#endif