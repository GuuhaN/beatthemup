﻿//Script used to spawn all the set spawnpoints using the Custom Editor Script

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BehaviorDesigner.Runtime;

public class SpawnWaves : MonoBehaviour
{
    public int currentEnemies { get; protected set; }
    public int currentActiveEnemiesDisplay;
    private int activeEnemies;
    [SerializeField]
    private int totalEnemies;
    [SerializeField]private List<EnemyData> deadEnemies = new List<EnemyData>();
    [HideInInspector]public EnemyData[] enemyPrefabs;
    [HideInInspector]public PickUp[] itemsToDrop;
    public Color[] enemyColors;
    private PlayerCamera playerCamera;
    public SpawnPointScript spawnPointObjectEmpty;

    public List<SpawnPointScript> enemiesToSpawn = new List<SpawnPointScript>();

    private void Awake()
    {
        playerCamera = FindObjectOfType<PlayerCamera>();
        StartCoroutine(SpawnEnemies());
    }

    private void LateUpdate()
    {
        EnemyDied();
        currentActiveEnemiesDisplay = currentEnemies;
    }

    public IEnumerator SpawnEnemies()
    {
        //Remove dead enemies from list
        if (deadEnemies.Count > 0)
        {
            for (int i = 0; i < deadEnemies.Count; i++)
            {
                Destroy(deadEnemies[i].gameObject);
                deadEnemies.Remove(deadEnemies[i]);
                i--;
            }
        }
        //Reset the enemies to spawn and find the next Enemies to spawn
        enemiesToSpawn.Clear();
        enemiesToSpawn = FindObjectsOfType<SpawnPointScript>().ToList();

        //If your enemies to spawn is empty Exit loop
        if (enemiesToSpawn.Count == 0)
            yield break;

        //Order on kills to spawn
        enemiesToSpawn.OrderBy(x => x.enemyRequiredKillsToSpawn);

        //If the spawnSegment is not the current segment remove every enemy thats not in this segment
        for (int i = 0; i < enemiesToSpawn.Count; i++)
        {
            if (enemiesToSpawn[i].enemySpawnSegment != playerCamera.viewSegment)
            {
                enemiesToSpawn.Remove(enemiesToSpawn[i]);
                i--;
            }
        }
        //Total enemies used later on
        totalEnemies = enemiesToSpawn.Count;

        while (true)
        {
            //If enemies to spawn equals zero exit spawner
            if (enemiesToSpawn.Count == 0)
                yield break;

            //For each enemy to spawn add a little spawnDelay while there is still enemies being spawned
            for (int i = 0; i < enemiesToSpawn.Count; i++)
            {
                yield return new WaitForSeconds(0.1f);
                if (enemiesToSpawn.Any(x => x != null))
                {
                    //Spawn enemy function
                    StartCoroutine(SpawnEnemy((enemiesToSpawn[i])));
                }
                //If Enemies to spawn is empty break loop
                else
                    yield break;
            }
            if (playerCamera.viewSegment >= playerCamera.levelGroundData.Count)
            {
                yield break;
            }
        }
    }

    private void EnemyDied()
    {
        //If an enemy dies find your current enemies and add the active enemies (enemies that still have to be spawned) and find all the dead enemies afterwards
        currentEnemies = FindObjectsOfType<EnemyData>().Count(x => !x.hasDied) + activeEnemies;
        deadEnemies = FindObjectsOfType<EnemyData>().Where(x => x.hasDied).ToList();
    }

    private IEnumerator SpawnEnemy(SpawnPointScript currentEnemy)
    {
        if (currentEnemy == null)
            yield break;

        //If the enemy is in the correct viewsegment
        if (currentEnemy.enemySpawnSegment == playerCamera.viewSegment)
        {
            //If the players have the required amount of kills to spawn this enemy
            if (currentEnemy.enemyRequiredKillsToSpawn <= deadEnemies.Count /*deadEnemies.Count ==*/)
            {
                //Add enemy and save his local values
                activeEnemies++;
                int enemyType = currentEnemy.enemyType;
                string enemyName = currentEnemy.enemyName;
                Vector3 enemyTransform = currentEnemy.transform.position;
                int itemID = currentEnemy.enemyDrop;
                float itemDropChance = currentEnemy.enemyDropChance;

                //Destroy the spawnpoint
                Destroy(currentEnemy.gameObject);

                //Wait for the enemies spawnDelay
                yield return new WaitForSeconds(currentEnemy.enemySpawnDelay);

                //Create new enemy with all the values set including the Item drop
                GameObject t = (GameObject)Instantiate(enemyPrefabs[enemyType].gameObject, enemyTransform, Quaternion.identity);
                t.GetComponent<Character>().characterName = enemyName;
                if (itemID > -1)
                {
                    t.GetComponent<BehaviorTree>().SetVariableValue("itemDrop", itemsToDrop[itemID].gameObject);
                    t.GetComponent<BehaviorTree>().SetVariableValue("itemChance", itemDropChance);
                }
                //Remove from active enemies because now he is in currentenemies
                activeEnemies--;
                yield return null;
            }
        }
        else
            yield return null;
    }
}
