﻿//Script used on every hitbox in the game


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider))]
public class Hitbox : MonoBehaviour
{
    //The basic values of every hitbox (most of this is saved in the attack creations)
    public int knockDownStrength;
    public float stunDuration;
    public List<Collider> colliderList = new List<Collider>();
    public bool colliderStaysOn;
    public Vector2 Physics;

    public SoundEffects.soundNames soundName = SoundEffects.soundNames.Punch;

    //Put trailrenderer and linerenderer like effects here
    [SerializeField] private GameObject[] Effect;

    //Called from the DoAttack to activate the hitbox with all required parameters
    public IEnumerator EnableBox(float durationTime, float startTime, int knockDown, bool hitboxStaysOn, float stunTime, Vector2 physicsStrength, SoundEffects.soundNames sound = SoundEffects.soundNames.Punch)
    {
        //Overwrite the values
        knockDownStrength = knockDown;
        stunDuration = stunTime;
        colliderStaysOn = hitboxStaysOn;
        soundName = sound;
        Physics = physicsStrength;
      
        //Wait for the hitbox to be allowed to start
        yield return new WaitForSeconds(startTime);

        //Enable the effects
        ChangeEffect(true);
        GetComponent<BoxCollider>().enabled = true;

        //Wait for hitbox to end in this coroutine
        StartCoroutine(DisableBox(durationTime));
        yield return null;
    }

    //Used to disable the hitbox after the duration
    private IEnumerator DisableBox(float time)
    {
        //Wait for the duration time
        yield return new WaitForSeconds(time);

        //Disable the hitbox and clear the colliders that were hit
        GetComponent<BoxCollider>().enabled = false;
        colliderList.Clear();
        
        //Wait a bit before removing the effects to stop abrupt removal
        yield return new WaitForSeconds(time * 1.5f);
        ChangeEffect(false);
        yield return null;
    }

    //Instantly remove all hitboxes
    public void DisableHitboxInstant()
    {
        //Stop each and every coroutine and reset everything
        StopAllCoroutines();
        ChangeEffect(false);
        GetComponent<BoxCollider>().enabled = false;
        colliderList.Clear();
    }

    //Called in character awake function to set a size by code for the hitboxes instead of by the inspector
    public void SetHitboxSize(Vector3 size, Vector3 center, bool hitboxMultipleEnemies)
    {
        GetComponent<BoxCollider>().center = center;
        GetComponent<BoxCollider>().size = size;
        colliderStaysOn = hitboxMultipleEnemies;
    }

    //Used to visualize hitboxes in the scene (INACCURATE REPRESENTATION OF THE SIZES) its used for the durations of the hitboxes
    private void OnDrawGizmos()
    {
            Gizmos.color = new Color(1, 0, 0, 0.5F);
            Gizmos.DrawCube(transform.position, new Vector3(GetComponent<Collider>().bounds.extents.x, GetComponent<Collider>().bounds.extents.y, GetComponent<Collider>().bounds.extents.z));     
    }

    //Enable or Disable the effects depending on the parameter
    private void ChangeEffect(bool status)
    {
        //If there are any effects this code will run
        if (Effect != null)
        {
            //Enable by setting the parameter true
            if (status)
            {
                //Enable each effect and clear it before running the effect
                for (int i = 0; i < Effect.Length; i++)
                {
                    Effect[i].SetActive(true);
                    Effect[i].GetComponent<TrailRenderer>().Clear();
                }             
            }

            //Disable with the parameter false
            else
            {
                //Disable each effect and clear it aswell before
                for (int i = 0; i < Effect.Length; i++)
                {
                    Effect[i].GetComponent<TrailRenderer>().Clear();
                    Effect[i].SetActive(false);
                }

            }
        }
    }
}
