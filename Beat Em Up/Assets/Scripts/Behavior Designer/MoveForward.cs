﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;

public class MoveForward : Action
{
    public SharedFloat speed;
    public override TaskStatus OnUpdate()
    {
        transform.position += transform.forward * speed.Value * Time.deltaTime;
        return TaskStatus.Success;
    }
}
