﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;

public class WalkAroundPoint : Action
{
    public SharedVector3 randomPoint;
    public override void OnAwake()
    {
        randomPoint.SetValue(GetPoint(5));
    }

    public Vector3 GetPoint(float range)
    {
        NavMeshHit hit;
        Vector3 result = Vector3.zero;
        AI_Director aiDirector = GameObject.Find("Camera").GetComponent<AI_Director>();
        for (int i = 0; i < aiDirector.aiDirector.Length; i++)
        {
            Vector3 centralPoint = new Vector3(aiDirector.aiDirector[i].playerPosition.x,
                0f, aiDirector.aiDirector[i].playerPosition.z) + Random.insideUnitSphere * range;

            if (NavMesh.SamplePosition(centralPoint, out hit, 1.0f, NavMesh.AllAreas))
                result = hit.position;
        }   
        return result;
    }
}
