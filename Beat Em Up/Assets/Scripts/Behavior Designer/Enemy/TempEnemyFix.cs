using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class TempEnemyFix : Action
{
	public override TaskStatus OnUpdate()
	{
        GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody>().velocity.x, -0.1f, GetComponent<Rigidbody>().velocity.z);
        return TaskStatus.Success;
	}
}