using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class EnemyGrab : Action
{
    private ThrowScript throwScript;
    public CustomSharedVariables.SharedCharacter target;
    public SharedBool grabbing;
	public override void OnStart()
	{
        throwScript = gameObject.GetComponent<ThrowScript>();
	}

	public override TaskStatus OnUpdate()
	{
        throwScript.grabbing = true;
        throwScript.currentLiftingObject = target.Value.gameObject;
        throwScript.liftingObjectScript._ObjectType = ThrowableObject.ObjectType.Character;
        throwScript.ThrowIdentifiedObject();
        grabbing.Value = false;
        return TaskStatus.Success;
	}

    private IEnumerator Grab()
    {
        yield return new WaitForSeconds(2);
    }
}