﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SetString : Action
{
    public SharedString targetString;
    public SharedString newValue;
    public override void OnStart()
    {
        targetString.Value = newValue.Value;
    }
    public override TaskStatus OnUpdate()
    {
        return TaskStatus.Success;
    }
}
