using System.Linq;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class AttackPattern : Action
{
    public CustomSharedVariables.SharedAttack currentAttack;

    public CustomSharedVariables.SharedAttackList attackList;
    public CustomSharedVariables.SharedAttack[] defaultAttacks;
    public CustomSharedVariables.SharedAttack comboBreakerAttack;

    public SharedInt countBeforeComboBreaker;

    public SharedInt currentCountOfAttacks;
    private Character characterClass;

    public override void OnAwake()
    {
        characterClass = Object.FindObjectOfType<Character>();		        
	}

	public override TaskStatus OnUpdate()
	{
        Debug.Log(attackList.Value.Find(x => x.attackName == defaultAttacks.Any(y => y.Value).ToString()));
        return TaskStatus.Success;
    }
}