﻿using UnityEngine;
using System.Collections.Generic;

namespace BehaviorDesigner.Runtime.Tasks
{
    [TaskDescription("This is the Wait function only in use for when waiting for an attack to happen.")]
    [HelpURL("http://www.opsive.com/assets/BehaviorDesigner/documentation.php?id=22")]
    [TaskIcon("{SkinColor}UtilitySelectorIcon.png")]
    public class AttackWait : Action
    {
        public List<CustomSharedVariables.SharedAttackList> attackLists = new List<CustomSharedVariables.SharedAttackList>();
        public CustomSharedVariables.SharedAttack currentAttack;
        public SharedInt currentAttackID;
        public SharedFloat currentAttackCooldown;
        public SharedInt attacksTillComboMove;
        public SharedBool counterAttack;
        public SharedInt attackCount;

        public SharedFloat distance, maxDistance;

        public CustomSharedVariables.SharedCharacter targetObject;
        public SharedBool canGrab, grabbing;

        //public SharedFloat visionLengthMax;
        //public SharedFloat viewAngle;
        //public SharedFloat rotateSpeed;
        public SharedFloat waitTime;
        public SharedFloat cooldownBetweenAttacks;

        // The time to wait
        private float waitDuration;

        public override void OnStart()
        {
            // Remember the start time.
            waitDuration = (waitTime.Value + cooldownBetweenAttacks.Value);
        }


        public override TaskStatus OnUpdate()
        {
            // The task is done waiting if the time waitDuration has elapsed since the task was started.

            if (waitDuration <= 0)
            {
                if (canGrab.Value)
                {
                    if (CheckForGrab())
                    {
                        grabbing.Value = true;
                        return TaskStatus.Failure;
                    }
                }
                return TaskStatus.Success;
            }
            else if (!CheckIfTargetIsInRange())
                return TaskStatus.Failure;
            // Otherwise we are still waiting.
            //UpdateRotationTowardsTarget();
            waitTime.Value = waitDuration -= Time.deltaTime;


                
            return TaskStatus.Running;
        }
        /*
        public bool UpdateRotationTowardsTarget()
        {
            if (!targetObject.Value || targetObject.Value == GetComponent<Character>())
                return false;

            Vector3 targetDirection = targetObject.Value.transform.position - transform.position;
            float angle = Vector3.Angle(targetDirection, transform.forward);
            float visionLength = Vector3.Distance(transform.position, targetObject.Value.transform.position);
            Quaternion lookAtRot = Quaternion.LookRotation(targetObject.Value.transform.position - transform.position);
            if (angle > -viewAngle.Value && angle < viewAngle.Value && visionLength < visionLengthMax.Value)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation,
                    new Quaternion(0, lookAtRot.y, 0, lookAtRot.w), Time.deltaTime * rotateSpeed.Value);
                return true;
            }
            return false;
        }
       */

        public bool CheckForGrab()
        {
            return targetObject.Value.isStunned;
        }

        private bool CheckIfTargetIsInRange()
        {
            return maxDistance.Value > distance.Value;
        }

        public override void OnEnd()
        {
            SetRandomAttack.RandomAttack(attackCount, attackLists, currentAttack, currentAttackID, currentAttackCooldown, attacksTillComboMove, maxDistance, counterAttack);
        }
    }
}