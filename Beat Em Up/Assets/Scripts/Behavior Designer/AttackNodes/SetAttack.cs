using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections.Generic;
using System.Collections;

public class SetAttack : Action
{
    public List<CustomSharedVariables.SharedAttackList> attackLists = new List<CustomSharedVariables.SharedAttackList>();
    public int attack;
    public CustomSharedVariables.SharedAttack currentAttack;
    public SharedInt currentAttackID;


    public override TaskStatus OnUpdate()
    {
        currentAttack.Value = attackLists[0].Value[attack];
        currentAttackID.Value = currentAttack.Value.attackID;
        return TaskStatus.Success;
    }
}