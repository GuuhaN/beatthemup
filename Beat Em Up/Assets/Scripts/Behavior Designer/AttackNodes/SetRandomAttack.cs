using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SetRandomAttack : Action
{
    public List<CustomSharedVariables.SharedAttackList> attackLists = new List<CustomSharedVariables.SharedAttackList>();
    public CustomSharedVariables.SharedAttack currentAttack;
    public SharedInt currentAttackID;
    public SharedFloat currentAttackCooldown;
    public SharedInt attacksTillComboMove;
    public SharedFloat currentAttackRange;
    public SharedBool counterAttack;
    public SharedInt attackCount;

	public override TaskStatus OnUpdate()
	{
        RandomAttack(attackCount,attackLists,currentAttack,currentAttackID,currentAttackCooldown,attacksTillComboMove,currentAttackRange,counterAttack);
        return TaskStatus.Success;
    }

    public static void RandomAttack(SharedInt attackCount,List<CustomSharedVariables.SharedAttackList> attackLists, CustomSharedVariables.SharedAttack currentAttack, SharedInt currentAttackID, SharedFloat currentAttackCooldown,
        SharedInt attacksTillComboMove, SharedFloat currentAttackRange, SharedBool counterAttack)
    {
        //Pick from the comboAttackList
        if (attackCount.Value >= attacksTillComboMove.Value)
        {
            currentAttack.Value = attackLists[1].Value[Random.Range(0, attackLists[1].Value.Count)];
            attackCount.Value = 0;
        }

        //Pick from the counterAttackList
        else if (counterAttack.Value)
        {
            currentAttack.Value = attackLists[2].Value[Random.Range(0, attackLists[2].Value.Count)];
            counterAttack.Value = false;
        }

        //Pick from the attackList
        else
        {
            currentAttack.Value = attackLists[0].Value[Random.Range(0, attackLists[0].Value.Count)];
            attackCount.Value++;
        }

        currentAttackID.Value = currentAttack.Value.attackID;
        currentAttackCooldown.Value = currentAttack.Value.attackCooldownTime;
        currentAttackRange.Value = currentAttack.Value.attackRange;
    }
}
public static class Test
{
    public static void Test2()
    {
        
    }
}