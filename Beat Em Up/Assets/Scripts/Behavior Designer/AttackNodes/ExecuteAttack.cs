using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class ExecuteAttack : Action
{
    public CustomSharedVariables.SharedAttack currentAttack;
    public SharedFloat attackCooldown;
    public SharedFloat cooldownBetweenAttacks;
    private Character characterClass; 

    private bool test = false;

    public override void OnAwake()
    {
        characterClass = Object.FindObjectOfType<Character>();
    }

    public override void OnStart()
    {
        //test = false;
        //StartCoroutine(DoAttack());
    }

    public override TaskStatus OnUpdate()
	{
        //characterClass.DoAttack(currentAttack.Value);
       // if (test)
            characterClass.DoAttack(currentAttack.Value);
            return TaskStatus.Success;
       // else
       //     return TaskStatus.Running;
	}
    /*
    private IEnumerator DoAttack()
    {
        characterClass.DoAttack(currentAttack.Value);
        yield return new WaitForSeconds((attackCooldown.Value) + cooldownBetweenAttacks.Value);
        test = true;
    }*/

}