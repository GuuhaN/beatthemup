using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

[TaskDescription("The Settings for the attack, If attackStun = 0 the attack will be a knockdown. If attackStun is higher than 0 it will be a stun attack")]
public class CreateAttack : Action
{
    [RequiredField]
    public CustomSharedVariables.SharedAttackList attackList;
    public AttackList.attackNames theEnum;
    [FloatRange(0, 100)] public float attackDamage;
    [FloatRange(0, 3)] public float additionalAttackCooldown;
    [FloatRange(0, 10)]public float attackStunDuration;
    public CustomSharedVariables.SharedKnockDownTypes attackStrength;
    public float attackRange;
    public bool AttackHitboxStaysOn;
    private int attackID;

    private AttackInfo attackInfoClass;

	public override void OnAwake()
	{
	    attackInfoClass = Object.FindObjectOfType<AttackInfo>();
	    attackID = (int)theEnum + 1;
        attackList.Value.Add(attackInfoClass.CreateInstance(theEnum.ToString(), attackDamage, attackID, 0,
	    attackStunDuration, Object.FindObjectOfType<AttackList>().DefaultValue(theEnum) + additionalAttackCooldown, (int)attackStrength.Value, attackRange, AttackHitboxStaysOn));
	}
}