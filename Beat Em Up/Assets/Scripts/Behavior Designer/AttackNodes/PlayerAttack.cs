﻿using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

[TaskDescription("The Settings for the attack, If attackStun = 0 the attack will be a knockdown. If attackStun is higher than 0 it will be a stun attack")]
public class PlayerAttack : Action
{
    [RequiredField]
    public CustomSharedVariables.SharedAttackList attackList;
    public AttackList.attackNames theEnum;
    public string[] buttons;
    public CustomSharedVariables.AttackType attackType;
    public CustomSharedVariables.WeaponAttackType weaponAttackType;
    [FloatRange(0, 100)] public float attackDamage;
    [FloatRange(0, 100)]public float comboPointsRequired;
    [FloatRange(0, 3)] public float additionalAttackCooldown;
    [FloatRange(0, 5)] public float attackStunDuration;
    [FloatRange(0, 1)] public float attackMovementTillAttack;
    public bool AttackHitboxStaysOn;
    public CustomSharedVariables.SharedKnockDownTypes attackStrength;


    private int attackID;

    private AttackInfo attackInfoClass;

    public override void OnAwake()
    {
        attackInfoClass = Object.FindObjectOfType<AttackInfo>();
        attackID = (int)theEnum +1;
        attackList.Value.Add(attackInfoClass.CreateInstance(theEnum.ToString(), attackDamage, attackID, 0,
        attackStunDuration, Object.FindObjectOfType<AttackList>().DefaultValue(theEnum) + additionalAttackCooldown, (int)attackStrength.Value, 0, AttackHitboxStaysOn, comboPointsRequired, buttons, attackMovementTillAttack, (int)attackType, (int)weaponAttackType-1));
    }
}