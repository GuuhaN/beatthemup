﻿using BehaviorDesigner.Runtime;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

public class FloatToVector3 : Action
{
    public BehaviorTree behaviorTree;
    public SharedFloat xAxis, yAxis, zAxis;
    public SharedVector3 finalVector3;

    public override TaskStatus OnUpdate()
    {
        finalVector3.Value = new Vector3(xAxis.Value, yAxis.Value, zAxis.Value);
        return TaskStatus.Success;
    }
}
