﻿using System.Linq;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using Action = BehaviorDesigner.Runtime.Tasks.Action;

/**<summary
 * 
 * Find closest object/character in the game class.
 * 
 * </summary>*/

[TaskDescription("Finds the closest object to this object within a list of GameObjects.")]
public class FindClosestObject : Action
{
    [RequiredField]
    public CustomSharedVariables.SharedCharacterList allObjects;
    public CustomSharedVariables.SharedCharacter closestCharacter;
    public CustomSharedVariables.SharedCharacter lastClosestCharacter;

    private AI_Director aiDirector;

    /** Class initializing */
    public override void OnAwake()
    {
        aiDirector = Object.FindObjectOfType<AI_Director>();
    }

    public override TaskStatus OnUpdate()
    {
        if (allObjects.Value.Count <= 0)
            return TaskStatus.Failure;

        GetClosestTransform();

        bool isActiveList = aiDirector.CheckAvailableTarget(closestCharacter.Value.transform, gameObject);
        if (!isActiveList)
        {
            bool isBackupList = aiDirector.AddToBackUpList(closestCharacter.Value.transform, gameObject);
            if (!isBackupList)
            {
                GetClosestTransform();
                return TaskStatus.Success;
            }
        }

        for (int i = 0; i < aiDirector.aiDirector.Length; i++)
        {
            if (!aiDirector.aiDirector[i].possibleEnemies.Contains(gameObject) || !aiDirector.aiDirector[i].backupEnemies.Contains(gameObject))
                return TaskStatus.Failure;
        }

        if (closestCharacter.Value == null)
            return TaskStatus.Failure;

        return TaskStatus.Success;
    }

    /** Gets closest character in the game world.*/
    public Character GetClosestTransform()
    {
        Transform tMin = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;

        for (int i = 0; i < allObjects.Value.Count; i++)
        {
            if (allObjects.Value.Count > 1)
            {
                foreach (Character t in allObjects.Value.FindAll(x => x != lastClosestCharacter.Value))
                {
                    float dist = Vector3.Distance(t.transform.position, currentPos);
                    if (!(dist < minDist)) continue;
                    tMin = t.transform;
                    minDist = dist;
                }
            }
            else
                tMin = allObjects.Value.First().transform;
        }
        if (tMin != null) return closestCharacter.Value = tMin.root.GetComponent<Character>();
        return null;
    }
}
