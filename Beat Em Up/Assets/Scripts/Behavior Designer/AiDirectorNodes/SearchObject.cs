﻿using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

[TaskDescription("Search object from an array that MUST be filled before this node get called")]
public class SearchObject : Action {

    [RequiredField]
    public SharedGameObjectList foundPlayers;
    [RequiredField]
    public CustomSharedVariables.SharedCharacter storeTarget;

    //public SharedString currentSharedString;

    private AI_Director aiDirector;
    private PlayerJoining playerJoining;


    /** Initializing classes */
    public override void OnAwake()
    {
        aiDirector = Object.FindObjectOfType<AI_Director>();
        playerJoining = Object.FindObjectOfType<PlayerJoining>();
    }

    /** When this node is active, it will keep searching for players */
    public override TaskStatus OnUpdate()
    {
        SearchForPlayer();
        return TaskStatus.Success;
    }

    /** Find all players in the game through this node. */
    public void SearchForPlayer()
    {
        //RaycastHit[] foundPlayers = Physics.SphereCastAll(new Ray(transform.position, transform.forward), 150, 1, (1 << 8));
        for (int i = 0; i < playerJoining.allPlayers.Length; i++)
        {
            if (playerJoining.playerJoined[i])
            {
                if (!foundPlayers.Value.Contains(playerJoining.allPlayers[i].gameObject))
                    foundPlayers.Value.Add(playerJoining.allPlayers[i].gameObject);

                //float distanceCheck = Vector3.Distance(transform.position, foundPlayers[i].position);
            }
        }

        /** When there are not any players found, this will execute. */
        if (foundPlayers.Value.Count > 0)
        {
            Transform foundPlayer = foundPlayers.Value[Random.Range(0, foundPlayers.Value.Count)].transform.root;
            bool isAvailableTarget = aiDirector.CheckAvailableTarget(foundPlayer, gameObject);
            if (!isAvailableTarget)
            {
                bool isBackUpList = aiDirector.AddToBackUpList(foundPlayer, gameObject);
                if (!isBackUpList)
                {
                    //currentSharedString.Value = "idle";
                }
                if (storeTarget.Value)
                {
                    aiDirector.DeleteTarget(storeTarget.Value.transform, gameObject);
                }
                return;
            }

            //currentSharedString.Value = "walk";
            storeTarget.Value = foundPlayer.gameObject.GetComponent<Character>();
        }
    }
}
