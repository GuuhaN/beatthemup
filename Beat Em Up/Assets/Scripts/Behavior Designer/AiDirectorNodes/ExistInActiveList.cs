using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class ExistInActiveList : Action
{
    private AI_Director aiDirector;

	public override void OnStart()
	{
	    aiDirector = Object.FindObjectOfType<AI_Director>();
	}

	public override TaskStatus OnUpdate()
	{
	    for (int i = 0; i < aiDirector.aiDirector.Length; i++)
	    {
	        if(aiDirector.aiDirector[i].possibleEnemies.Contains(gameObject))
                return TaskStatus.Success;
	    }

	    return TaskStatus.Failure;
    }
}