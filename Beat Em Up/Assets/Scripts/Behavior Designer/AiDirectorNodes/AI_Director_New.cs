﻿using System.Linq;
using UnityEngine;  
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

[TaskDescription("A director that handles the flow of the A.I's within the game")]
public class AI_Director_New : Action {

    public SharedInt maxCapacity;
    private SharedInt playerIndex;
    public AI_Director_Serializable_New[] aiDirector;

    public override void OnAwake()
    {
        for (int i = 0; i < aiDirector.Length; i++)
        {
            aiDirector[i].possibleEnemies.Value.Capacity = maxCapacity.Value;
            aiDirector[i].backupEnemies.Value.Capacity = maxCapacity.Value + 1;
        }
    }

    public override TaskStatus OnUpdate()
    {
        for (int i = 0; i < aiDirector.Length; i++)
            aiDirector[i].playerPosition = aiDirector[i].player.Value.transform.position;
        return TaskStatus.Success;
    }

    public bool CheckAvailableTarget(Transform playerTarget, GameObject requestedObject)
    {
        //Finds the player target and see if it contains in the list.
        playerIndex = aiDirector.ToList().FindIndex(x => x.player.Value.transform == playerTarget);

        // Checks if the player is knocked, grabbed or can they get hit?
        aiDirector[playerIndex.Value].isPlayerAffected =
            aiDirector[playerIndex.Value].player.Value.GetComponent<Character>().knockedOut ||
            aiDirector[playerIndex.Value].player.Value.GetComponent<Character>().isGrabbed
            || aiDirector[playerIndex.Value].player.Value.GetComponent<Character>().hasDied;

        for (int i = 0; i < aiDirector.Length; i++)
        {
            if (playerIndex.Value != i)
                if (aiDirector[i].possibleEnemies.Value.Contains(requestedObject) || aiDirector[i].backupEnemies.Value.Contains(requestedObject))
                    return false;
        }
        //Adds an enemy to the player's list they are targeting. 
        //If they don't exist in the list already.
        //If the count of the list is lower than the maxCapacity and if the player is affected or not.
        if (!aiDirector[playerIndex.Value].possibleEnemies.Value.Contains(requestedObject) &&
            aiDirector[playerIndex.Value].possibleEnemies.Value.Count < maxCapacity.Value && !aiDirector[playerIndex.Value].isPlayerAffected.Value)
        {
            if (aiDirector[playerIndex.Value].backupEnemies.Value.Count > 0)
            {
                if (aiDirector[playerIndex.Value].backupEnemies.Value.Contains(requestedObject))
                {
                    if (requestedObject.GetComponent<EnemyData>())
                    {
                        //requestedObject.GetComponent<JustForUnityReferences>().target = playerTarget;
                        aiDirector[playerIndex.Value].possibleEnemies.Value.Add(requestedObject);
                        aiDirector[playerIndex.Value].backupEnemies.Value.Remove(requestedObject);
                        return true;
                    }
                }
            }
            else
            {
                aiDirector[playerIndex.Value].possibleEnemies.Value.Add(requestedObject);
                return true;
            }
        }
        return false;
    }

    public bool AddToBackUpList(Transform playerTarget, GameObject requestedObject)
    {
        playerIndex = aiDirector.ToList().FindIndex(x => x.player.Value.transform == playerTarget);

        for (int i = 0; i < aiDirector.Length; i++)
        {
            if (aiDirector[i].possibleEnemies.Value.Contains(requestedObject) || aiDirector[i].backupEnemies.Value.Contains(requestedObject))
                return false;
        }
        if (!aiDirector[playerIndex.Value].backupEnemies.Value.Contains(requestedObject) &&
            aiDirector[playerIndex.Value].backupEnemies.Value.Count < aiDirector[playerIndex.Value].backupEnemies.Value.Capacity && !aiDirector[playerIndex.Value].isPlayerAffected.Value)
        {
            aiDirector[playerIndex.Value].backupEnemies.Value.Add(requestedObject);
            return true;
        }
        return false;
    }

    //Called at enemy A.I
    public void DeleteTarget(Transform playerTarget, GameObject requestedObject)
    {
        //Finds the player target and see if it contains in the list.
        int playerIndex = aiDirector.ToList().FindIndex(x => x.player.Value.transform == playerTarget);

        //if enemy object exists in the possibleEnemies list.
        if (aiDirector[playerIndex].possibleEnemies.Value.Contains(requestedObject))
        {
            //Remove the target out of the list.
            aiDirector[playerIndex].possibleEnemies.Value.Remove(requestedObject);
        }
    }

    public void DeleteBackUpTarget(GameObject requestedObject)
    {
        //if enemy object exists in the backupEnemies list.
        if (aiDirector[playerIndex.Value].backupEnemies.Value.Contains(requestedObject))
        {
            //Remove the target out of the list.
            aiDirector[playerIndex.Value].backupEnemies.Value.Remove(requestedObject);
        }
    }
}
