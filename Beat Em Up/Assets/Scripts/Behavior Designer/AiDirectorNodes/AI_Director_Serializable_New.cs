﻿using System;
using BehaviorDesigner.Runtime;

[Serializable]
public class AI_Director_Serializable_New
{
    public SharedGameObject player;
    public SharedVector3 playerPosition;
    public SharedGameObjectList possibleEnemies, backupEnemies = new SharedGameObjectList();
    public SharedBool isPlayerAffected;
}