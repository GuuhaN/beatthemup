using System.Linq;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CheckPlayer : Action
{
    private AI_Director aiDirector;

    public CustomSharedVariables.SharedCharacter targetCharacter;
    public SharedBool isTargetAffected;
    public SharedBool evaluateBoolCheck;
	public override void OnAwake()
	{
	    aiDirector = Object.FindObjectOfType<AI_Director>();
	}

    public override TaskStatus OnUpdate()
    {
        if (targetCharacter.Value != null)
        {
            int playerIndex = aiDirector.aiDirector.ToList()
                .FindIndex(x => x.player == targetCharacter.Value.gameObject);
            isTargetAffected.Value = aiDirector.aiDirector[playerIndex].isPlayerAffected;
            evaluateBoolCheck = targetCharacter == null;

            if (isTargetAffected.Value)
            {
                if (aiDirector.aiDirector[playerIndex].possibleEnemies.Contains(gameObject))
                    aiDirector.DeleteTarget(targetCharacter.Value.transform, gameObject);
                else if (aiDirector.aiDirector[playerIndex].backupEnemies.Contains(gameObject))
                    aiDirector.DeleteBackUpTarget(targetCharacter.Value.transform, gameObject);
                return TaskStatus.Success;
            }
        }

        return TaskStatus.Success;
    }
}