using System.Linq;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

/**<summary
 * 
 * Class to check every status of the character within this node. Every info is extracted from the AI_Director class.
 * 
 * </summary>*/
public class CheckPlayerStatus : Action
{
    private AI_Director aiDirector;

    public CustomSharedVariables.SharedCharacterList allTargets;
    public CustomSharedVariables.SharedCharacter targetCharacter;
    public CustomSharedVariables.SharedCharacter lastClosestCharacter;
    public SharedBool isTargetAffected;
    public override void OnAwake()
    {
        aiDirector = Object.FindObjectOfType<AI_Director>();
    }

    /** Checks every players effect. */
    public override TaskStatus OnUpdate()
    {
        int playerIndex = aiDirector.aiDirector.ToList().FindIndex(x => x.player == targetCharacter.Value.gameObject);
        if (isTargetAffected.Value)
        {
            if (aiDirector.aiDirector[playerIndex].possibleEnemies.Contains(gameObject))
                aiDirector.DeleteTarget(targetCharacter.Value.transform, gameObject);
            else if (aiDirector.aiDirector[playerIndex].backupEnemies.Contains(gameObject))
                aiDirector.DeleteBackUpTarget(targetCharacter.Value.transform, gameObject);
            if (allTargets.Value.Count <= 1)
                lastClosestCharacter.Value = null;
            else
                lastClosestCharacter.Value = targetCharacter.Value;

            targetCharacter.Value = null;

            return TaskStatus.Failure;
        }

        return targetCharacter.Value == null ? TaskStatus.Failure : TaskStatus.Success;
    }
}