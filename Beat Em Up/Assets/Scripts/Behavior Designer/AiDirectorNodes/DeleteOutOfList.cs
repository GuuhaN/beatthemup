using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class DeleteOutOfList : Action
{
    private AI_Director aiDirector;

    [RequiredField] public CustomSharedVariables.SharedCharacter targetCharacter;
	public override void OnStart()
	{
	    aiDirector = Object.FindObjectOfType<AI_Director>();
	}

	public override TaskStatus OnUpdate()
	{
	    if (targetCharacter.Value)
	    {
	        for (int i = 0; i < aiDirector.aiDirector.Length; i++)
	        {
	            if (aiDirector.aiDirector[i].possibleEnemies.Contains(gameObject))
	                aiDirector.DeleteTarget(targetCharacter.Value.transform, gameObject);
	            if (aiDirector.aiDirector[i].backupEnemies.Contains(gameObject))
	                aiDirector.DeleteBackUpTarget(targetCharacter.Value.transform, gameObject);
	        }
	    }
	    return TaskStatus.Success;
	}
}