using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class MoveRandomAround : Action
{
    private NavMeshAgent navMeshAgent;

    public CustomSharedVariables.SharedCharacter targetCharacter;
    public SharedVector3 randomPoint;

	public override void OnStart()
	{
	    navMeshAgent = GetComponent<NavMeshAgent>();
	}

	public override TaskStatus OnUpdate()
	{
	    randomPoint.Value = new Vector3(targetCharacter.Value.transform.position.x, transform.position.y, targetCharacter.Value.transform.position.z) + (Random.insideUnitSphere*8);
        return navMeshAgent.SetDestination(randomPoint.Value) ? TaskStatus.Success : TaskStatus.Failure;
    }
}