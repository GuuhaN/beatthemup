using UnityEngine;
#if !(UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4)
using UnityEngine.AI;
#endif

namespace BehaviorDesigner.Runtime.Tasks.Basic.UnityNavMeshAgent
{
    [TaskCategory("Basic/NavMeshAgent")]
    [TaskDescription("Sets the destination of the agent in world-space units. Returns Success if the destination is valid.")]
    public class SetDestinationForCharacter : Action
    {
        [Tooltip("The GameObject that the task operates on. If null the task GameObject is used.")]
        public CustomSharedVariables.SharedCharacter targetGameObject;
        [SharedRequired]
        [Tooltip("The NavMeshAgent destination")]
        public CustomSharedVariables.SharedCharacter destination;

        // cache the navmeshagent component
        private NavMeshAgent navMeshAgent;
        private GameObject prevGameObject;

        public override void OnStart()
        {
            var currentGameObject = GetDefaultGameObject(targetGameObject.Value.gameObject);
            if (currentGameObject != prevGameObject)
            {
                navMeshAgent = currentGameObject.GetComponent<NavMeshAgent>();
                prevGameObject = currentGameObject;
            }
        }

        public override TaskStatus OnUpdate()
        {
            if (navMeshAgent == null)
            {
                Debug.LogWarning("NavMeshAgent is null");
                return TaskStatus.Failure;
            }

            if(destination.Value == null)
                return TaskStatus.Failure;

            return navMeshAgent.SetDestination(destination.Value.transform.position) ? TaskStatus.Success : TaskStatus.Failure;
        }

        public override void OnReset()
        {
            targetGameObject = null;
            destination.Value.transform.position = Vector3.zero;
        }
    }
}