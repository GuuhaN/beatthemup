using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CheckIfCanMove : Action
{
    public SharedBool knockedDown;
    public SharedBool flinced;
    public SharedBool stunned;
	public override void OnStart()
	{
		
	}

	public override TaskStatus OnUpdate()
	{
		return !knockedDown.Value && !flinced.Value && !stunned.Value ? TaskStatus.Success : TaskStatus.Failure;
	}
}