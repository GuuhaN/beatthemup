using UnityEngine;
namespace BehaviorDesigner.Runtime.Tasks.Basic.UnityGameObject
{
    [TaskCategory("Basic/GameObject")]
    [TaskDescription("Returns the distance between two Vector3s.")]
    public class DistanceWithCharacters : Action
    {
        [Tooltip("The first Vector3")] public CustomSharedVariables.SharedCharacter firstVector3;
        [Tooltip("The second Vector3")] public CustomSharedVariables.SharedCharacter secondVector3;
        [Tooltip("The distance")] [RequiredField] public SharedFloat storeResult;

        public override TaskStatus OnUpdate()
        {
            if (!secondVector3.Value)
                return TaskStatus.Failure;

            storeResult.Value = Vector3.Distance(firstVector3.Value.transform.position, secondVector3.Value.transform.position);
            return TaskStatus.Success;
        }

        public override void OnReset()
        {
            firstVector3.Value.transform.position = secondVector3.Value.transform.position = Vector3.zero;
            storeResult = 0;
        }
    }
}
