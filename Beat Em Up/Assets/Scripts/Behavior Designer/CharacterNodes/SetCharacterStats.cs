using System;
using UnityEngine;
using BehaviorDesigner.Runtime;
using Action = BehaviorDesigner.Runtime.Tasks.Action;

public class SetCharacterStats : Action
{
    private NavMeshAgent myNavMeshAgent;

    public CustomSharedVariables.SharedCharacter myCharacter;
    public SharedGameObject targetObject;
    public int characterHealth;
    public int characterPoints;
    public CustomSharedVariables.SharedKnockDownTypes characterType;
    [FloatRange(0,1)] public float characterSpeed;
    public int attackTillComboMove;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("Stores the value of: Attack Till Combo Move")] public SharedInt storeValueAttCount;
    public float startAttackCooldown;
    [BehaviorDesigner.Runtime.Tasks.Tooltip("Stores the value of: Max Attack Range ( Before the enemy becomes aggressive )")]
    public SharedFloat storeValueStartAttCooldown;
    public float cooldownBetweenAttacks;
    public SharedFloat storeCooldownBetweenAttacks;
    public float immunityTime;
    public SharedFloat storeImmunityTime;
    public float getUpTime;
    public bool canGrab;
    [FloatRange(0,10)]
    public float grabMultiplier;
    public SharedBool storeCanGrab;
    public float healthToDoSomething;
    public SharedFloat storeHealthToDoSomething;
    

    public override void OnAwake()
    {
        myCharacter.Value = GetComponent<Character>();
        myNavMeshAgent = GetComponent<NavMeshAgent>();
        targetObject.Value = gameObject;
        myCharacter.Value.characterHealth = characterHealth;
        myCharacter.Value.characterPoints = characterPoints;
        myCharacter.Value.characterMaxHealth = characterHealth;
        myCharacter.Value.characterKnockDownStrength = (int)characterType.Value;
        myNavMeshAgent.speed = characterSpeed;
        storeValueAttCount.Value = attackTillComboMove;
        storeValueStartAttCooldown.Value = startAttackCooldown;
        storeCooldownBetweenAttacks.Value = cooldownBetweenAttacks;
        storeImmunityTime.Value = immunityTime;
        myCharacter.Value.getUpTime = getUpTime;
        storeCanGrab.Value = canGrab;
        myCharacter.Value.characterGrabMultiplier = grabMultiplier;
        storeHealthToDoSomething.Value = healthToDoSomething;
        WarningVariables();
    }

    private void WarningVariables()
    {
        if (attackTillComboMove <= 0)
            Debug.LogWarning("NOTE: AttackTillComboMove variable is " + attackTillComboMove + " of object " + gameObject);
        if (startAttackCooldown <= 0)
            Debug.LogWarning("NOTE: StartAttackCooldown variable is " + startAttackCooldown + " of object " + gameObject);
    }
}