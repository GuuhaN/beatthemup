using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CheckCharacterHealth : Action
{
    public CustomSharedVariables.SharedCharacter target;
    public SharedFloat targetHealth;

	public override TaskStatus OnUpdate()
	{
		return (target.Value.characterHealth < targetHealth.Value) ? TaskStatus.Success : TaskStatus.Failure;
	}
}