using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SetMyCharacter : Action
{
    public CustomSharedVariables.SharedCharacter myCharacter;
	public override void OnStart()
	{
	    myCharacter.Value = gameObject.GetComponent<Character>();
	}
}