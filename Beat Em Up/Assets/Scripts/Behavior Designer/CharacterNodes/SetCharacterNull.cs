using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SetCharacterNull : Action
{
    public CustomSharedVariables.SharedCharacter character;
    public override void OnStart()
    {
        character.Value = null;
    }
}