using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CheckTargetStatus : Action
{
    public CustomSharedVariables.SharedCharacter target;
    public bool checkKnockedOut;
    public bool checkStunned;
    public bool checkFlinched;

    public bool returnSucces;

	public override TaskStatus OnUpdate()
	{
        if (checkKnockedOut && target.Value.knockedOut || checkStunned && target.Value.isStunned || checkFlinched && target.Value.animatorController.GetBool("IsFlinched"))
            return returnSucces ? TaskStatus.Success : TaskStatus.Failure;
        else
            return returnSucces ? TaskStatus.Failure : TaskStatus.Success;
    }
}