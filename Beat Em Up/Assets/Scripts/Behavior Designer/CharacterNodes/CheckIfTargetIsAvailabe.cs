using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class CheckIfTargetIsAvailabe : Action
{
    public CustomSharedVariables.SharedCharacter targetToCheck;
    
	public override TaskStatus OnUpdate()
	{
        if(targetToCheck.Value)
            if (targetToCheck.Value.characterHealth > 0 && !targetToCheck.Value.knockedOut && !targetToCheck.Value.hasDied && targetToCheck.Value.canGetHit) 
                return TaskStatus.Success;

	    return TaskStatus.Failure;
	}
}