using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class GetMyGameObject : Action
{
    public SharedGameObject target;
	public override void OnStart()
	{
        target.Value = gameObject;
	}

	public override TaskStatus OnUpdate()
	{
		return TaskStatus.Success;
	}
}