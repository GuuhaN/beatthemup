﻿using System;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using Action = BehaviorDesigner.Runtime.Tasks.Action;

[TaskDescription("Checks if a variable is null, else it can continue the behavior tree.")]
public class IsNull : Action
{
    [RequiredField]
    public SharedVariable variableCheck;
    public SharedBool isNull;

    public SharedBool evaluationNullCheck;
    // Use this for initialization

    public override TaskStatus OnUpdate()
    {
        if (!isNull.Value)
        {
            if (variableCheck.GetValue() == null)
            {
                evaluationNullCheck.Value = true;
                return TaskStatus.Failure;
            }
            evaluationNullCheck.Value = false;
            return TaskStatus.Success;
        }

        else
        {
            if (variableCheck.GetValue() == null)
            {
                evaluationNullCheck.Value = true;
                return TaskStatus.Success;
            }

            evaluationNullCheck = false;
            return TaskStatus.Failure;
        }
    }
}

