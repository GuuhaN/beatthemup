﻿using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;

/**<summary
 * 
 * In this script can make your own custom variables for the Behavior Designer. Also here you can ONLY make enums that you can use in Behavior Designer.
 * http://www.opsive.com/assets/BehaviorDesigner/documentation.php?id=88
 * 
 * </summary>*/

public class CustomSharedVariables : Behavior {

    [System.Serializable]
    public class SharedRigidBody : SharedVariable<Rigidbody>
    {
        public static implicit operator SharedRigidBody(Rigidbody value)
        {
            return new SharedRigidBody { Value = value };
        }
    }

    [System.Serializable]
    public class SharedCollider : SharedVariable<Collider>
    {
        public static implicit operator SharedCollider(Collider value)
        {
            return new SharedCollider { Value = value };
        }
    }

    [System.Serializable]
    public class SharedAttack : SharedVariable<AttackInfo>
    {
        public static implicit operator SharedAttack(AttackInfo value)
        {
            return new SharedAttack { Value = value };
        }
    }

    [System.Serializable]
    public class SharedAttackList : SharedVariable<List<AttackInfo>>
    {
        public static implicit operator SharedAttackList(List<AttackInfo> value)
        {
            return new SharedAttackList {mValue = value};
        }
    }

    public enum KnockDownTypes
    {
        None, Light, Heavy
    }

    [System.Serializable]
    public class SharedKnockDownTypes : SharedVariable<KnockDownTypes>
    {
        public override string ToString() { return mValue.ToString(); }
        public static implicit operator SharedKnockDownTypes(KnockDownTypes value) { return new SharedKnockDownTypes { mValue = value }; }
    }

    public enum Buttons
    {
        None, Akey_P, Bkey_P, Xkey_P, Ykey_P
    }

    [System.Serializable]
    public class SharedButtons : SharedVariable<Buttons>
    {
        public override string ToString() { return mValue.ToString(); }
        public static implicit operator SharedButtons(Buttons value) { return new SharedButtons { mValue = value }; }
    }

    public enum AttackType
    {
        NormalAttack, GrabAttack, WeaponAttack
    }

[System.Serializable]
public class SharedAttackType : SharedVariable<AttackType>
{
    public override string ToString() { return mValue.ToString(); }
    public static implicit operator SharedAttackType(AttackType value) { return new SharedAttackType { mValue = value }; }
}

    public enum WeaponAttackType
    {
        None, Knife, Pole, Sword, Gun
    }

    [System.Serializable]
    public class SharedWeaponAttack : SharedVariable<WeaponAttackType>
    {
        public override string ToString() { return mValue.ToString(); }
        public static implicit operator SharedWeaponAttack(WeaponAttackType value) { return new SharedWeaponAttack { mValue = value }; }
    }

    [System.Serializable]
    public class SharedStringList : SharedVariable<string>
    {
        public override string ToString() { return mValue.ToString(); }
        public static implicit operator SharedStringList(string value) { return new SharedStringList { mValue = value }; }
    }

    [System.Serializable]
    public class SharedCharacter : SharedVariable<Character>
    {
        public static implicit operator SharedCharacter(Character value) { return new SharedCharacter { mValue = value }; }
    }

    [System.Serializable]
    public class SharedCharacterList : SharedVariable<List<Character>>
    {
        public static implicit operator SharedCharacterList(List<Character> value)
        {
            return new SharedCharacterList {mValue = value};
        }
    }

    [System.Serializable]
    public class SharedFloatList : SharedVariable<List<float>>
    {
        public static implicit operator SharedFloatList(List<float> value)
        {
            return new SharedFloatList {mValue = value};
        }
    }
}
