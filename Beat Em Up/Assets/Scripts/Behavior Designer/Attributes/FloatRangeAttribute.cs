﻿using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

public class FloatRangeAttribute : ObjectDrawerAttribute
{
    public float min;
    public float max;

    public FloatRangeAttribute(float min, float max)
    {
        this.min = min;
        this.max = max;
    }
}