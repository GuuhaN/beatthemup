﻿using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;

public class IntRangeAttribute : ObjectDrawerAttribute
{
    public int min;
    public int max;

    public IntRangeAttribute(int min, int max)
    {
        this.min = min;
        this.max = max;
    }
}