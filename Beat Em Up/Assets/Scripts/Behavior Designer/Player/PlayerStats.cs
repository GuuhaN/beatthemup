using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

/**<summary
 * 
 * This is to initialize the stats fo the current AI. This makes for the designer easier to edit the values.
 * 
 * </summary>*/

public class PlayerStats : Action
{
    public SharedInt Health, Type;
    public SharedFloat Speed, RotationSpeed, JumpHeight, GetUpTime, GrabMultiplier;
    public SharedString Name;
    public int characterHealth;
    public string characterName;
    public CustomSharedVariables.SharedKnockDownTypes characterType;

    [FloatRange(0.5f, 3)]
    public float characterSpeed;
    [FloatRange(5, 30)]
    public float characterRotationSpeed;
    [FloatRange(100, 500)]
    public float characterJumpHeight;
    public float getUpTime;
    [FloatRange(0, 10)]
    public float grabMultiplier;

    /** Initializing stats for this character. */
    public override void OnAwake()
    {
        Health.Value = characterHealth;
        Name.Value = characterName;
        Type.Value = (int)characterType.Value;
        Speed.Value = characterSpeed;
        RotationSpeed.Value = characterRotationSpeed;
        JumpHeight.Value = characterJumpHeight;
        GetUpTime.Value = getUpTime;
        GrabMultiplier.Value = grabMultiplier;
    }
}