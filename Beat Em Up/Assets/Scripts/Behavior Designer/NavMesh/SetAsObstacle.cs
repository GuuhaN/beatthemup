﻿using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime.Tasks.Basic.UnityGameObject;

public class SetAsObstacle : Action
{
    private bool testBool = true;
    public override void OnStart()
    {
        if(!GetComponent<NavMeshObstacle>().enabled)
        StartCoroutine(test());
    }

    public override TaskStatus OnUpdate()
    {

        if (!GetComponent<NavMeshObstacle>().enabled)
            return TaskStatus.Running;
        else
            return TaskStatus.Success;
    }
    IEnumerator test()
    {
        testBool = true;
        GetComponent<NavMeshAgent>().enabled = false;
        yield return null;
        GetComponent<NavMeshObstacle>().enabled = true;
        testBool = false;
    }
}