﻿using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime.Tasks.Basic.UnityGameObject;

public class SetAsAgent : Action
{
    private bool testBool = true;
    public override void OnStart()
    {
        if(!GetComponent<NavMeshAgent>().enabled)
            StartCoroutine(test());
    }
    //PROBEREN TE ZORGEN DAT DE REST WACHT TOTDAT AGENT ENABLED IS//\\\....///
    public override TaskStatus OnUpdate()
    {
        if (!GetComponent<NavMeshAgent>().enabled)
            return TaskStatus.Running;
        else
            return TaskStatus.Success;
    }
    IEnumerator test()
    {
        testBool = true;
        GetComponent<NavMeshObstacle>().enabled = false;
        yield return null;
        GetComponent<NavMeshAgent>().enabled = true;
        testBool = false;
    }
}