﻿using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class EnableAimAssist : Action
{
    public CustomSharedVariables.SharedCharacter targetObject;
    public SharedFloat visionLengthMax;
    public SharedFloat viewAngle;
    public SharedFloat rotateSpeed;

    public override TaskStatus OnUpdate()
    {
        UpdateRotationTowardsTarget();
        return TaskStatus.Success;
    }

    public bool UpdateRotationTowardsTarget()
    {
        if (!targetObject.Value || targetObject.Value == GetComponent<EnemyData>())
            return false;

        Vector3 targetDirection = targetObject.Value.transform.position - transform.position;
        float angle = Vector3.Angle(targetDirection, transform.forward);
        float visionLength = Vector3.Distance(transform.position, targetObject.Value.transform.position);
        Quaternion lookAtRot = Quaternion.LookRotation(targetObject.Value.transform.position - transform.position);
        if (angle > -viewAngle.Value && angle < viewAngle.Value && visionLength < visionLengthMax.Value)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation,
                new Quaternion(0, lookAtRot.y, 0, lookAtRot.w), Time.deltaTime * rotateSpeed.Value);
            return true;
        }
        return false;
    }
}
