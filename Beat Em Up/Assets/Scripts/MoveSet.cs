﻿//Class used for finding the object that contains the moveset for your character

using UnityEngine;
using System.Collections;

public class MoveSet : MonoBehaviour
{
    //All characters in the game
    public enum PlayerNames { Guy, BlackGirl, BlackGuy, MichaelJacksonGirl }

    //This objects characterMoveSet
    public PlayerNames type;
}