﻿using UnityEngine;

/**<summary
 * 
 * Deze component zit op alle objecten die Static zijn.
 * 
 * </summary>*/
[RequireComponent(typeof(Collider), typeof(NavMeshObstacle))]
public class ItemStatic : MonoBehaviour
{
    [SerializeField] private float staticObjTotalToBreak = 20f;

    private GameObject hitParticleGameObject;
    [SerializeField] private GameObject throwablePart;
    [SerializeField] private GameObject pickUpToDrop;

    [SerializeField] private ParticleSystem hitParticles;

    private NavMeshObstacle myNavMeshObstacle;
    private Collider myCollider;


    private void Awake()
    {
        SetStartProperties();
    }

    /** Alle classes pakken die nodig zijn, worden in de Awake aangeroepen*/
    private void SetStartProperties()
    {
        myCollider = GetComponent<Collider>();
        myNavMeshObstacle = GetComponent<NavMeshObstacle>();
    }

    /** Wanneer een object destroyed word, dan word deze functie gecalled */
    private void ObjectDestroyed()
    {
        throwablePart.layer = LayerMask.NameToLayer("LiftableObject");
        throwablePart.GetComponent<Rigidbody>().isKinematic = false;
        throwablePart.transform.SetParent(null);
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<Rigidbody>().isKinematic = false;
            myCollider.enabled = false;
            myNavMeshObstacle.enabled = false;
            gameObject.layer = LayerMask.NameToLayer("Dead");
        }

        if (!pickUpToDrop)
            return;

        Instantiate(pickUpToDrop, transform.position, Quaternion.identity);

    }

    /** Wanneer dit object een ander object raakt, word dit aangeroepen */
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.GetComponent<Hitbox>() && col.transform.root.GetComponent<Player>())
        {
            col.gameObject.GetComponent<Hitbox>().colliderList.Add(gameObject.GetComponent<Collider>());
            //playbobjectsound
            staticObjTotalToBreak -= col.transform.root.GetComponent<Player>().characterDamage;
            hitParticleGameObject = Instantiate(hitParticles, new Vector3(transform.position.x, transform.position.y + 1f, transform.position.z), Quaternion.identity) as GameObject;
            Destroy(hitParticleGameObject, 2f);

            if (staticObjTotalToBreak <= 0)
            {
                ObjectDestroyed();
            }
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        if (staticObjTotalToBreak >= 0)
        {
            if (col.gameObject.layer == LayerMask.NameToLayer("Ragdoll"))
            {
                Collider colGameObjectCollider = col.transform.GetComponent<Collider>();
                if (colGameObjectCollider.bounds.Intersects(GetComponent<Collider>().bounds))
                {
                    //playobjectsound
                    ObjectDestroyed();
                }
            }
        }
    }
}
