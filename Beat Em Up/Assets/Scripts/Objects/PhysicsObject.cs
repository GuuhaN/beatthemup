﻿using UnityEngine;

/**<summary
 * 
 * This script is for any objects that are tagged as Physics Object.
 * 
 * </summary>*/

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class PhysicsObject : MonoBehaviour {

    public int playerID;
    public int objectDamage;
    [SerializeField] private int objectType; //0 wood, 1 metal, 2 glass
    [SerializeField] private int objectBreaksAfter;

    public float currentSpeed;

    public bool objectBreakable;//objects that break after use
    [SerializeField] private bool throwableDestructable;

    public GameObject particle;

    /** Solely to check the magnitude of the object if its moving (fast) enough or not. */
    void Update()
    {
        currentSpeed = GetComponent<Rigidbody>().velocity.magnitude;
    }

    /** The collision check of the object if its actually hitting something. */
    void OnCollisionEnter(Collision objectCollision)
    {
        if (objectCollision.gameObject.layer == 0 ||
            playerID == 0 && !objectCollision.gameObject.GetComponent<PhysicsObject>() || currentSpeed <= 0)
        {
            playerID = 0;

            if (!GetComponent<Weapon>())
                return;

            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = true;
            Destroy(this);
            gameObject.layer = LayerMask.NameToLayer("Weapon");

            return;
        }

        if (objectCollision.gameObject.name != "Player" + playerID)
        {
            if (objectCollision.gameObject.GetComponent<PhysicsObject>())
            {
                if (objectCollision.gameObject.GetComponent<PhysicsObject>().playerID == 0 && playerID == 0 ||
                    currentSpeed <= 0)
                {
                    playerID = 0;
                    return;
                }

                objectCollision.gameObject.GetComponent<PhysicsObject>().objectBreaksAfter--;
            }

            if (objectBreakable)
                objectBreaksAfter--;

            if (objectBreaksAfter <= 0)
            {
                //playbreaksound
                GameObject activeParticle = (GameObject) Instantiate(particle, transform.position, Quaternion.identity);
                Destroy(activeParticle, 4f);
                if (!throwableDestructable)
                    gameObject.SetActive(false);
                else
                {
                    for (int i = 0; i < transform.childCount; i++)
                    {
                        transform.GetChild(i).GetComponent<Rigidbody>().isKinematic = false;
                        transform.GetChild(i).GetComponent<Collider>().isTrigger = false;
                        transform.GetChild(i).gameObject.layer = LayerMask.NameToLayer("LiftableObject");
                        transform.GetChild(i).parent = null;
                        GetComponent<PhysicsObject>().enabled = false;
                        gameObject.layer = LayerMask.NameToLayer("Default");
                    }
                }
            }
        }
    }
}
