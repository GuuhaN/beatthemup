﻿using UnityEngine;

/**<summary
 * 
 * This script is to gain information about the object that can be thrown.
 * 
 * </summary>*/

public sealed class ThrowableObject : MonoBehaviour {

    public enum ObjectType { Character, PhysicsObject, Weapon } // Define all ObjectTypes in the game.
    public ObjectType _ObjectType; // Get ObjectType enum and set the object type where this Component is placed on.
    public bool isGrabbed
    {
        get
        {
            return GetComponent<Character>() && GetComponent<Character>().isGrabbed;
        }
        set
        {
            if (!GetComponent<Character>())
                return;

            GetComponent<Character>().isGrabbed = value;
        }
    } // Get grab state from Character Class.
    public bool isKnockedOut
    {
        get
        {
            return GetComponent<Character>() && GetComponent<Character>().knockedOut;
        }
        set
        {
            GetComponent<Character>().isGrabbed = value;
        }
    } // Get grab state from Character Class.
    public bool canGetHit
    {
        get
        {
            if (GetComponent<PhysicsObject>() || GetComponent<EnemyData>() || GetComponent<PickUp>()) return true;
            return GetComponent<Player>().canGetHit;
        }
        set
        {
            if (!GetComponent<Player>())
                return;

            GetComponent<Player>().canGetHit = value;
        }
    } // Get canGetHit state form Character Class.
    public bool isStunned
    {
        get
        {
            if (!GetComponent<Character>())
                return true;

            return GetComponent<Character>().stunDuration > 0.5f;
        }
    } // Get isStunned state from Character Class.
    public bool isDead
    {
        get
        {
            if (!GetComponent<Character>())
                return false;

            return GetComponent<Character>().hasDied;
        }
    } // Get hasDied state from Character Class
    public Animator myAnimator
    {
        get
        {
            return !GetComponent<Animator>() ? null : GetComponent<Animator>();
        }
    } // Get animator and checks if it's available.
    public Rigidbody myRigidBody
    {
        get
        {
            return !GetComponent<Rigidbody>() ? null : GetComponent<Rigidbody>();
        }
    } // Get rigidbody and checks if it's available.
    public Collider myCollider { get
    {
        return !GetComponent<Collider>() ? null : GetComponent<Collider>();
    } } // Get collider and checks if it's available.
    public bool isGrabbing
    {
        get
        {
            return GetComponent<Player>() && GetComponent<ThrowScript>().grabbing;
        }
    } // Get grabbing state of another player to prevent from multigrabbing.
    public GameObject grabObject
    {
        get
        {
            return !GetComponent<ThrowScript>() ? null : GetComponent<ThrowScript>().currentLiftingObject;
        }
    } // Get current holding object to prevent from multigrabbing.
}
