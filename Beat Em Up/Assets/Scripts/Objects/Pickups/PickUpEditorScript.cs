﻿//PickUp Editor Script used to create PickUp prefabs


#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PickUp))]
[CanEditMultipleObjects]
public class PickUpEditorScript : Editor
{
    public enum PickUpType { Health, Weapon, Points }
    private PickUpType type;
    private int health;
    private int weaponHitsTillBreak;
    private enum WeaponName { Knife, Pole, Sword, Gun }
    private new WeaponName name;
    private int points;

    private void OnEnable()
    {
        //Set variables if the component exists already
        GameObject t = ((MonoBehaviour)target).gameObject;
        type = (PickUpType)t.GetComponent<PickUp>().type;

        if(t.GetComponent<Health>())
            health = t.GetComponent<Health>().health;

        if (t.GetComponent<Weapon>())
        {
            weaponHitsTillBreak = t.GetComponent<Weapon>().weaponHitsTillBreaks;
            name = (WeaponName)t.GetComponent<Weapon>().id;
        }
        if(t.GetComponent<Points>())
         points = t.GetComponent<Points>().points;

    }
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        //the editor options
        GUILayout.BeginHorizontal();
        //Select the pickup Type
        EditorGUILayout.LabelField("PickUp Type", EditorStyles.boldLabel);
        type = (PickUpType)EditorGUILayout.EnumPopup(type, GUILayout.Width(200));
        GUILayout.EndHorizontal();
        //Find the gameObject
        GameObject t = ((MonoBehaviour)target).gameObject;

        //If you are not running the game (Play Mode)
        if (!Application.isPlaying)
        {
            //Add components that are missing
            if (!t.GetComponent<Health>())
            {
                t.AddComponent<Health>();
            }
            if (!t.GetComponent<Weapon>())
            {
                t.AddComponent<Weapon>();
            }
            if (!t.GetComponent<Points>())
            {
                t.AddComponent<Points>();
            }
        }

        switch (type)
        {
            //Find the PickupType and adjust the settings in the view based on the select pickup
            case PickUpType.Health:

                //Health settings
                int maxvalueHealth = 100;
                EditorGUILayout.LabelField("Add Health", EditorStyles.boldLabel);
                health = EditorGUILayout.IntSlider(health, 1, maxvalueHealth);
                ProgressBar(health / (float)maxvalueHealth, "Health");

                //Save the settings
                t.GetComponent<Health>().health = health;
                t.GetComponent<PickUp>().type = (int)type;
                break;

            case PickUpType.Weapon:
                GUILayout.BeginHorizontal();

                //Weapon Settings
                EditorGUILayout.LabelField("Weapon Type", EditorStyles.boldLabel);
                name = (WeaponName)EditorGUILayout.EnumPopup(name, GUILayout.Width(200));
                GUILayout.EndHorizontal();
                int maxvalueBreak = 20;
                EditorGUILayout.LabelField("Weapon hits till breaks", EditorStyles.boldLabel);
                weaponHitsTillBreak = EditorGUILayout.IntSlider(weaponHitsTillBreak, 1, maxvalueBreak);
                ProgressBar(weaponHitsTillBreak / (float)maxvalueBreak, "Breaks In");

                //Save the settings            
                t.GetComponent<Weapon>().id = (int)name;
                t.GetComponent<Weapon>().weaponHitsTillBreaks = weaponHitsTillBreak;
                t.GetComponent<PickUp>().type = (int)type;
                break;

            case PickUpType.Points:
                int maxvaluePoints = 1000;

                //Points Settings
                EditorGUILayout.LabelField("Points", EditorStyles.boldLabel);
                points = EditorGUILayout.IntSlider(points, 1, maxvaluePoints);
                ProgressBar(points / (float)maxvaluePoints, "Points");

               //Save the Settings
                t.GetComponent<Points>().points = points;
                t.GetComponent<PickUp>().type = (int)type;
                break;
        }

        //save settings in editorprefs
        Repaint();
    }
    //Progress bar to give indication of how strong a pickup is based on set maximum values
    private void ProgressBar(float value, string label)
    {
        Rect rect = GUILayoutUtility.GetRect(18, 18, "TextField");
        EditorGUI.ProgressBar(rect, value, label);
        EditorGUILayout.Space();
    }
   
}
#endif