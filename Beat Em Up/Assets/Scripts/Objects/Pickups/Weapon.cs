﻿//Base Weapon PickUp class

using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
    //playerID when picked up
    [HideInInspector]public int id;

    //If the Object is taken
    [HideInInspector]public bool taken;

    //Basic values of how a character holds the object and how its rotated (change these in the inspector to fix offsets and positions)
    public Vector3 offset;
    public Vector3 rotationOffset;

    [HideInInspector] public int weaponHitsTillBreaks;
    private bool inAir;

    //Function called when Object is hit
    public void TakeHit()
    {
        weaponHitsTillBreaks--;
        if (weaponHitsTillBreaks < 1)
        {
            Destroy(gameObject);
        }
    }

    //Function called when Object is dropped
    public void ResetWeapon()
    {
        taken = false;
    }
}
 

