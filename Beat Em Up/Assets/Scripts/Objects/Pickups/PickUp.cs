﻿//Default baseclass for all PickUps in the game

using UnityEngine;
using System;
using System.Collections;

public class PickUp : MonoBehaviour {
    [SerializeField][FloatRange(0,100)] private float timeBeforeDespawn = 100;
    [HideInInspector]
    public int type;

    private void Start()
    {
        //Remove components that are not used in the game for this object
        if (type == 0)
        {
            Destroy(GetComponent<Weapon>());
            Destroy(GetComponent<Points>());
        }
        if (type == 1)
        {
            Destroy(GetComponent<Health>());
            Destroy(GetComponent<Points>());
        }
        if (type == 2)
        {
            Destroy(GetComponent<Weapon>());
            Destroy(GetComponent<Health>());
        }
    }

    private void Update()
    {
        //Set a despawn time for objects that are not weapons
        if (!GetComponent<Weapon>())
        {
                if (timeBeforeDespawn > 0)
                    timeBeforeDespawn -= Time.deltaTime;
                else
                    Destroy(gameObject);
            }

        //If the weapon is not picked up then also countdown the despawn time for this one
        else
            if (!GetComponent<Weapon>().taken)
             {
            if (timeBeforeDespawn > 0)
                timeBeforeDespawn -= Time.deltaTime;
            else
                Destroy(gameObject);
        }
    }

}
