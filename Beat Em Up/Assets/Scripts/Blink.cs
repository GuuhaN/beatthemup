using UnityEngine;
using System.Collections;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class Blink : Action
{
    public CustomSharedVariables.SharedCharacter target;
    public SharedFloat immunityTime;
    private float _immunityTime;
	public override void OnStart()
	{
        _immunityTime = immunityTime.Value;
        StartCoroutine(BlinkMe());
	}

	public override TaskStatus OnUpdate()
	{
        if (_immunityTime > 0)
        {
            target.Value.canGetHit = false;
            _immunityTime -= Time.deltaTime;
            return TaskStatus.Running;
        }
        else
        {
            target.Value.canGetHit = true;
            return TaskStatus.Success;
        }
	}

    public IEnumerator BlinkMe()
    {
        while (_immunityTime > 0)
        {
            yield return new WaitForSeconds(0.2f * (_immunityTime / 2f));
            foreach (Transform child in transform)
                if (child.GetComponent<SkinnedMeshRenderer>())
                    child.GetComponent<SkinnedMeshRenderer>().enabled = false;

            yield return new WaitForSeconds(0.2f * (_immunityTime / 2f));
            foreach (Transform child in transform)
                if (child.GetComponent<SkinnedMeshRenderer>())
                    child.GetComponent<SkinnedMeshRenderer>().enabled = true;
        }
    }
}