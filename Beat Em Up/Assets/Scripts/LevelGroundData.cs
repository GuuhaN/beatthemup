﻿using UnityEngine;
using UnityEngine.UI;

/**<summary
 * 
 * This script is for visibility only to see how big the segments are per game.
 * Main purpose is for the designer to change the segments really easily.
 * 
 * </summary>*/

[ExecuteInEditMode]
public class LevelGroundData : MonoBehaviour
{
    protected int indexNumber
    {
        get { return transform.GetSiblingIndex(); }
    }
    //public int segment { get; protected set; }
    public int segmentBoundsMinX { get; protected set; }
    public int segmentBoundsCenterX { get; protected set; }
    public int segmentBoundsMaxX { get; protected set; }
    public int segmentBoundsMinZ { get; protected set; }
    public int segmentBoundsMaxZ { get; protected set; }

    /** Gets the length and other statistics of each segment object */
    private void LateUpdate()
    {
        Renderer myRenderer = GetComponent<Renderer>();
        /** Renderer.bounds takes the length of the renderer. This could be inaccurate when the mesh is not aligned correctly. */
        segmentBoundsMinX = (int) myRenderer.bounds.min.x;
        segmentBoundsCenterX = (int) myRenderer.bounds.center.x;
        segmentBoundsMaxX = (int) myRenderer.bounds.max.x;
        segmentBoundsMinZ = (int) myRenderer.bounds.min.z;
        segmentBoundsMaxZ = (int) myRenderer.bounds.max.z;
        transform.name = "Segment " + (indexNumber + 1);
    }

    /** To display the size of each segment in  edit mode */
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(200,0,0,0.5f);
        Gizmos.DrawCube(transform.position,new Vector3(segmentBoundsMaxX-segmentBoundsMinX, (transform.localScale.y/2) * 60, segmentBoundsMaxZ-segmentBoundsMinZ));
    }
}
