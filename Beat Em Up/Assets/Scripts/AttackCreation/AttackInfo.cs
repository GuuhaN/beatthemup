﻿//ScriptableObject creation for attacks to use them for each character

using UnityEngine;
using System.Collections;

public class AttackInfo : ScriptableObject {

    //All adjustable variables for an atatck
    public string attackName;
    public float attackDamage;
    public int attackID;
    public float attackMovementSpeed;
    public enum AttackKnockDownStrength {None, Light, Heavy};
    public int attackKnockDown;
    public float attackStunDuration;
    public float attackCooldownTime;
    public float attackRange;
    public bool attackHitboxStaysOn;
    public float attackComboPointsRequired;
    public string[] attackInputs;
    public float attackMovementCap;
    public int attackType;
    public int weaponAttackType;


    //Put every value in the ScriptableObject
    private void Init(
        string name,
        float damage,
        int ID,
        float movementSpeed,
        float stunDuration,
        float coolDownDuration,
        int knockDown,
        float range = 0,
        bool attackhitbox = false,
        float points = 0,
        string[] inputs = null,
        float movementCap = 0,
        int attackType = 0,
        int weaponAttackType = 0
        )
    {
        attackName = name;
        attackDamage = damage;
        attackID = ID;
        attackMovementSpeed = movementSpeed;
        attackKnockDown = knockDown;
        attackStunDuration = stunDuration;
        attackCooldownTime = coolDownDuration;
        attackRange = range;
        attackHitboxStaysOn = attackhitbox;
        attackComboPointsRequired = points;
        attackInputs = inputs;
        attackMovementCap = movementCap;
        this.attackType = attackType;
        this.weaponAttackType = weaponAttackType;
}

[SerializeField]
//When creating the scriptableObject here you can create the instance with all the values it has to create (used in Create Attack Nodes)
public AttackInfo CreateInstance(
    string attack,
    float damage,
    int ID,
    float movementSpeed,
    float stunDuration,
    float coolDownDuration,
    int knockDown,
    float range = 0,
    bool attackhitbox = false,
    float points = 0,
    string[] inputs = null,
    float movementCap = 0,
    int attackType = 0,
    int weaponAttackType = 0
        )
    {
      AttackInfo data = CreateInstance<AttackInfo>();

      //Set values to the ScriptableObject
      data.Init(attack, damage, ID, movementSpeed, stunDuration, coolDownDuration, knockDown, range, attackhitbox, points, inputs, movementCap, attackType, weaponAttackType);
      data.name = attack;
      return data;
    }
 }
