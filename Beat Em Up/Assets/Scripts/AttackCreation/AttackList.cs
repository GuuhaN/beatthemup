﻿//List used to set default values of attacks

using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AttackList : MonoBehaviour
{
    [SerializeField]
    public AttackInfo[] attackList;
    private AttackInfo creationScript;
    private int attackID;

    //Enum for all attackNames of attacks that are created
    public enum attackNames {LowKick, Punch, RoundHouseKick, LegSweep, OneTwo, BackFlip, RunKick, GrabPunch, GrabKick, ComboBreakerKick, ComboBreakerPunch, TurboPunch, WeaponStab, WeaponSlash, WeaponHorizontalSwing, WeaponRoundhouseSwing, WeaponOverheadSwing, BackwardsKick, RoundHouseHook };

    //No longer in use
    void Awake()
    {
        MakeAttacks();  
    }
    [SerializeField]
    private void MakeAttacks()
    {
        creationScript = ScriptableObject.CreateInstance<AttackInfo>();
    }

    //ALL DEFAULT VALUES OF ANIMATIONS 
    //Set values here to change the MINIMAL animation duration to make them not cut too short

    //Animation time set in attack nodes are added ontop of these default values

    public float DefaultValue(attackNames name)
    {
        if (name == attackNames.LowKick)
            return 0.7f;

        else if (name == attackNames.Punch)
            return 0.4f;

        else if(name == attackNames.RoundHouseKick)
            return 1.9f;

        else if(name == attackNames.LegSweep)
            return 2.3f;

        else if(name == attackNames.OneTwo)
            return 1f;

        else if(name == attackNames.BackFlip)
            return 1.5f;

        else if (name == attackNames.RunKick)
            return 2.2f;

        else if (name == attackNames.GrabPunch)
            return 1f;

        else if (name == attackNames.GrabKick)
            return 0.5f;

        else if (name == attackNames.ComboBreakerKick)
            return 1f;

        else if (name == attackNames.ComboBreakerPunch)
            return 0.5f;

        else if (name == attackNames.TurboPunch)
            return 1f;

        else if (name == attackNames.WeaponStab)
            return 1.2f;

        else if (name == attackNames.WeaponSlash)
            return 1.2f;

        else if (name == attackNames.WeaponHorizontalSwing)
            return 1.2f;

        else if (name == attackNames.WeaponRoundhouseSwing)
            return 1.2f;

        else if (name == attackNames.WeaponOverheadSwing)
            return 1.2f;

        else if (name == attackNames.BackwardsKick)
            return 1.55f;

        else if (name == attackNames.RoundHouseHook)
            return 1.9f;

        return 0f;  
    }
}

