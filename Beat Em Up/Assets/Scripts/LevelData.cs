﻿#if UNITY_EDITOR
#if UNITY_STANDALONE_WIN
using UnityEngine;

[ExecuteInEditMode] // Editor purposes
public class LevelData : MonoBehaviour
{
    public int maxSegments
    {
        get { return _maxSegments; }
        private set { _maxSegments = value; }
    }
    private int _maxSegments;

    void OnGUI()
    {
        if (maxSegments == transform.childCount)
            return;

        _maxSegments = transform.childCount; // Purpose of getting the count
    }
}
#endif
#endif