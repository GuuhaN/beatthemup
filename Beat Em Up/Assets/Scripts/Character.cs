﻿//Base class for all characters in the game
//Players and Enemies derive from this class

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Every character must have these components to prevent errors in the code
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ThrowableObject))]

public class Character : MonoBehaviour
{
    public int characterHealth { get; set; }
    public float getUpTime { get; set; }

    [HideInInspector] public int characterMaxHealth;
    [HideInInspector] public float characterSpeed;
    [HideInInspector] public string characterName;
    [HideInInspector]public int characterPoints;
    [Range(0,10)] public float characterGrabMultiplier;
    [HideInInspector] public float characterAttackCoolDown;
    [HideInInspector] public int characterDamage;
    [HideInInspector] public int characterKnockDownStrength;
    [HideInInspector] public Animator animatorController;
    [HideInInspector] public AimAssist aimAssist;

    protected Hitbox[] hitboxes;
    protected CharacterJoint[] allCharacterJoints;
    protected Vector3[] characterJointsPos;
    private Vector3[] HitboxCenter;
    private Vector3[] HitboxSize;
    private bool[] HitboxColliderStaysOn;

    public LayerMask mask;
    protected Rigidbody rigidBody;

    //Particles that are used by the character
    protected ParticleEffects[] hitParticle;
    protected ParticleEffects[] ragdollHitParticle;
    protected ParticleEffects[] deadParticle;
    protected ParticleEffects[] ragdollGroundParticle;

    //Needed to start sound events
    protected SoundEffects soundEffects;

    protected bool groundHitOnce;

    [HideInInspector] public GameObject weaponHand;
    protected Weapon weaponScript;
    protected ThrowScript throwScript;
	public GameObject weapon;
	public bool usingWeapon;

    public bool canGetHit = true;

    [HideInInspector]
    public bool isGrabbed
    {
        get { return _isGrabbed; }
        set { _isGrabbed = value; }
    }
    [SerializeField]private bool _isGrabbed;
    [HideInInspector] public bool isFlinched { get; set; }
    [HideInInspector] public bool isStunned { get; set; }
    [HideInInspector] public bool knockedOut { get; set; }
    [HideInInspector] public bool hasDied { get; set; }
    [HideInInspector] public float stunDuration { get; set; }

    private int randomAttackSelection;

    protected virtual void Start()
    {
		//Setting the variables \ loading them
        allCharacterJoints = GetComponentsInChildren<CharacterJoint>();
        characterJointsPos = new Vector3[allCharacterJoints.Length];
        throwScript = GetComponent<ThrowScript>();
        hitParticle = FindObjectOfType<ParticleLoader>().hitParticles;
        ragdollHitParticle = FindObjectOfType<ParticleLoader>().ragdollHitParticles;
        deadParticle = FindObjectOfType<ParticleLoader>().deadParticles;
        ragdollGroundParticle = FindObjectOfType<ParticleLoader>().ragdollGroundParticles;

        //Disable the rigidbodies on the bones
        Rigidbody[] bones = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rigid in bones)
        {
            rigid.isKinematic = true;
        }

        soundEffects = FindObjectOfType<SoundEffects>();
        
        //Load all the hitboxes and information
        hitboxes = GetComponentsInChildren<Hitbox>();
        HitboxColliderStaysOn = new bool[hitboxes.Length];
        HitboxSize = new Vector3[hitboxes.Length];
        HitboxCenter = new Vector3[hitboxes.Length];
        animatorController = GetComponent<Animator>();
        if (GetComponentInChildren<WeaponHand>())
        {
            weaponHand = GetComponentInChildren<WeaponHand>().gameObject;
        }
        rigidBody = GetComponent<Rigidbody>();
        aimAssist = GetComponent<AimAssist>();

        //Hardcoded values for hitbox sizes, can be removed to set custom sizes for each characters hitboxes instead but for playtesting this was easier
        #region HitboxSizes
        //backflip
        HitboxSize[0] = new Vector3(1, 0.88f, 1);
        HitboxCenter[0] = new Vector3(-0.25f, -0.25f, 0);
        //forwardkickL
        HitboxSize[1] = new Vector3(0.6f, 0.8f, 0.38f);
        HitboxCenter[1] = new Vector3(0, 0, 0);
        //RunKick
        HitboxSize[2] = new Vector3(0.2f, 1.32f, 0.53f);
        HitboxCenter[2] = new Vector3(0, 0, 0);
        //LegSweep
        HitboxSize[3] = new Vector3(0.95f, 0.90f, 0.5f);
        HitboxCenter[3] = new Vector3(-0.28f, 0, 0.1f);
        //ForwardKickR
        HitboxSize[4] = new Vector3(0.6f, 0.8f, 0.38f);
        HitboxCenter[4] = new Vector3(0, 0, 0);
        //RoundHouse
        HitboxSize[5] = new Vector3(0.3f, 1.32f, 0.7f);
        HitboxCenter[5] = new Vector3(0, 0, 0.23f);
        //LeftHand
        HitboxSize[6] = new Vector3(0.75f, 0.75f, 0.2f);
        HitboxCenter[6] = new Vector3(0, 0.18f, 0);
        //RightHand
        HitboxSize[7] = new Vector3(0.75f, 0.75f, 0.4f);
        HitboxCenter[7] = new Vector3(0, 0.18f, 0);
        #endregion

        for (int i = 0; i < hitboxes.Length; i++)
        {
            HitboxColliderStaysOn[i] = true;
            hitboxes[i].SetHitboxSize(HitboxSize[i], HitboxCenter[i], HitboxColliderStaysOn[i]);
        }

        for (int i = 0; i < allCharacterJoints.Length; i++)
            characterJointsPos[i] = allCharacterJoints[i].transform.localPosition;
    }

    //Function to take damage (called from the receiving end)
    public void TakeDamage(int damage)
    {
        characterHealth -= damage;
        if (characterHealth <= 0)
            Die();


        if (!throwScript) return;
        throwScript.ResetGrabIdentification();
    }


    protected virtual void Update()
    {
        //If the character dies and is not playing a dead animation
        if (hasDied)
            if (animatorController.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f && !animatorController.IsInTransition(0))
            {
                if (animatorController.enabled)
                {
                    //Disable the animator, reset the bones and enable ragdoll bones
                    FixAllBones();
                    animatorController.enabled = false;
                    Rigidbody[] bones = GetComponentsInChildren<Rigidbody>();
                    foreach (Rigidbody rigid in bones)
                        rigid.isKinematic = false;

                    //Change layer to the dead layer for collision purposes
                    foreach (CharacterJoint joint in allCharacterJoints)
                        joint.gameObject.layer = 11;
                }
            }

        /*this was still work in progress for collision on ground hit after ragdolling for a nice impact effect */
        //Only when character is knockedout there will be a raycast for the hit location
        if (knockedOut && !groundHitOnce)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, -transform.up, out hit, 0.1f, mask.value))
            {
                Debug.Log("Hit");
                foreach (ParticleEffects party in ragdollGroundParticle)
                {
                    Instantiate(ragdollGroundParticle[0].gameObject, new Vector3(hit.point.x, hit.point.y, hit.point.z), Quaternion.identity);
                }
                groundHitOnce = true;
            }
        }
    }
    
    //Called automaticly when characters die
    protected virtual void Die()
    {
        //Play a random KO animation when the character dies
        animatorController.SetInteger("KO", Random.Range(0, 5));
        animatorController.Play("SelectorKO");

        //Set all layers to the Dead layer
            foreach (CharacterJoint allCharacterJoint in allCharacterJoints)
        {
            allCharacterJoint.gameObject.layer = LayerMask.NameToLayer("Dead");
        }
        transform.gameObject.layer = 11;
        foreach (Transform child in transform)
        {
            child.gameObject.layer = 11;
        }
        knockedOut = true;
        isGrabbed = false;
        hasDied = true;
    }
    //Big wall of text in this region
    #region .
    
        //Called for every attack by a character to handle the standard attacking behaviours
    public virtual void DoAttack(AttackInfo attack)
    {
        //Disable the current hitboxes
        DisableActiveHitboxes();
        
        //Save all values from the attack about to be executed
        characterDamage = (int)attack.attackDamage;
        int canKnockDown = attack.attackKnockDown;
        bool hitboxStaysOn = attack.attackHitboxStaysOn;
        float stunTime = attack.attackStunDuration;

        //Set the attack animation for this attack
        animatorController.SetTrigger("Attack");
        animatorController.SetInteger("AttackID", attack.attackID);

        //If random attack is available here the random attack is selected
        StartCoroutine(PickRandomAttack(6));

        //Give your character the needed attack cooldown
        characterAttackCoolDown = attack.attackCooldownTime;

        //Check for every hitbox if its the correct one
        foreach (Hitbox hitbox in hitboxes)
        {
            //Values downhere are not hardcoded but the duration and startdelay are tweaked as much as we thought was the best feeling for an attack. Physics only apply if the character goes in ragdoll mode after the hit
            //so if this attack doesnt have a knockdown it only applies if the character dies from the attack

            Hitbox hitboxScript = hitbox.GetComponent<Hitbox>();
            //EnableBox(HitBoxduration, HitboxStartDelay, canKnockDown, hitboxStaysOn, stunTime, Physics force)


            if (attack.attackID == 1 || attack.attackID == 11 )  //normalkick
            {
                if (hitbox.gameObject.name == "hitboxForwardKickR")
                    {
                    //Find which animation is called and set invidual values for it if needed
                    if (randomAttackSelection == 0) //forwardKickR
                        StartCoroutine(hitboxScript.EnableBox(0.4f, 0.3f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));
                    if (randomAttackSelection == 2) //straightKickR
                        StartCoroutine(hitboxScript.EnableBox(0.4f, 0.3f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));
                    if (randomAttackSelection == 3) //MiddleKIckR
                        StartCoroutine(hitboxScript.EnableBox(0.4f, 0.3f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));
                    if (randomAttackSelection == 5) //AxeKick
                        StartCoroutine(hitboxScript.EnableBox(0.4f, 0.3f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 10000)));
                    hitboxScript.soundName = SoundEffects.soundNames.Kick;
                }
            }

            if (attack.attackID == 1 || attack.attackID == 11 ) //normalkick & combokick & backwardskick
            {
                if (hitbox.gameObject.name == "hitboxForwardKickL")
                {
                    //Find which animation is called and set invidual values for it if needed
                    if (randomAttackSelection == 1) //forwardKickL
                        StartCoroutine(hitboxScript.EnableBox(0.4f, 0.3f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));
                    if (randomAttackSelection == 4) //middleKickL
                        StartCoroutine(hitboxScript.EnableBox(0.4f, 0.3f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));
                    hitboxScript.soundName = SoundEffects.soundNames.Kick;
                }
            }

            if (attack.attackID == 2 || attack.attackID == 10)//normalpunch
            {
                if (hitbox.gameObject.name == "hitboxLeftHand")
                {
                    //Find which animation is called and set invidual values for it if needed
                    if (randomAttackSelection == 0) // hookL
                        StartCoroutine(hitboxScript.EnableBox(0.3f, 0.1f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(2400, 0)));
                    if (randomAttackSelection == 2) //jabL
                        StartCoroutine(hitboxScript.EnableBox(0.3f, 0.1f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(2400, 0)));
                    if (randomAttackSelection == 4) //upperL
                        StartCoroutine(hitboxScript.EnableBox(0.3f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(2400, 20000)));
                }
            }


            if (attack.attackID == 2 || attack.attackID == 10)//normalpunch
            {
                if (hitbox.gameObject.name == "hitboxRightHand")
                {
                    //Find which animation is called and set invidual values for it if needed
                    if (randomAttackSelection == 1) //hookR
                        StartCoroutine(hitboxScript.EnableBox(0.3f, 0.1f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(2400, 0)));
                    if (randomAttackSelection == 3) //jabR
                        StartCoroutine(hitboxScript.EnableBox(0.3f, 0.1f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(2400, 0)));
                    if (randomAttackSelection == 5) //upperR
                        StartCoroutine(hitboxScript.EnableBox(0.3f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(2400, 20000)));
                }
            }

            if (attack.attackID == 3 && hitbox.gameObject.name == "hitboxRoundHouse")//roundhousekick
                StartCoroutine(hitboxScript.EnableBox(0.3f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(6000, 3000), SoundEffects.soundNames.RoundHouseKick));

            if (attack.attackID == 4 && hitbox.gameObject.name == "hitboxSweep")//sweepkick
                StartCoroutine(hitboxScript.EnableBox(0.5f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(6000, 0), SoundEffects.soundNames.LegSweep));

            if (attack.attackID == 5) //elbow + uppercut
            {
                if (hitbox.gameObject.name == "hitboxLeftHand")
                    StartCoroutine(hitboxScript.EnableBox(0.3f, 0.1f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));

                if (hitbox.gameObject.name == "hitboxRightHand")
                    StartCoroutine(hitboxScript.EnableBox(0.3f, 0.3f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));
            }

            if (attack.attackID == 6 && hitbox.gameObject.name == "hitboxBackFlip") //backflip
            {
                StartCoroutine(hitboxScript.EnableBox(0.7f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(0, 0), SoundEffects.soundNames.Kick));
                animatorController.applyRootMotion = false;
            }

            if (attack.attackID == 7 && hitbox.gameObject.name == "hitboxRunKick") //runkick
                StartCoroutine(hitboxScript.EnableBox(0.7f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(5000, 0), SoundEffects.soundNames.Kick));

            if (attack.attackID == 8 && hitbox.gameObject.name == "hitboxLeftHand") //grabpunch
            {
                StartCoroutine(hitboxScript.EnableBox(0.4f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(6000, 1000), SoundEffects.soundNames.ComboHook));
            }

            if (attack.attackID == 9 && hitbox.gameObject.name == "hitboxForwardKickR") //grabkick
            {
                StartCoroutine(hitboxScript.EnableBox(0.4f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(6000, 2000), SoundEffects.soundNames.Kick));
            }

          

            if (attack.attackID == 12) //TurboPunch
            {
                if (hitbox.gameObject.name == "hitboxLeftHand")
                    StartCoroutine(hitboxScript.EnableBox(0.1f, 0.1f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));

                if (hitbox.gameObject.name == "hitboxRightHand")
                    StartCoroutine(hitboxScript.EnableBox(0.1f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));

                if (hitbox.gameObject.name == "hitboxLeftHand")
                    StartCoroutine(hitboxScript.EnableBox(0.1f, 0.3f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));

                if (hitbox.gameObject.name == "hitboxRightHand")
                    StartCoroutine(hitboxScript.EnableBox(0.1f, 0.4f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));
            }

            if (attack.attackID > 12 && attack.attackID < 18) //weapon attacks
            {
                StartCoroutine(weaponScript.GetComponent<Hitbox>().EnableBox(0.5f, 0.2f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));
            }

            if (attack.attackID == 18 && hitbox.gameObject.name == "hitboxForwardKickL")
                 StartCoroutine(hitboxScript.GetComponent<Hitbox>().EnableBox(0.5f, 0.1f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));

            if (attack.attackID == 19 && hitbox.gameObject.name == "hitboxRightHand")
                StartCoroutine(hitboxScript.GetComponent<Hitbox>().EnableBox(0.5f, 0.4f, canKnockDown, hitboxStaysOn, stunTime, new Vector2(3000, 0)));
        }
    }
    #endregion

    //Called to reset the bones to their original position
    protected void FixAllBones()
    {
        //Find all the character bones and reset them to their start offsets
        for (int i = 0; i < allCharacterJoints.Length; i++)
            allCharacterJoints[i].transform.localPosition = characterJointsPos[i];
    }

    //Enable the characters ragdoll
    public virtual void EnableRagdoll()
    {
        FixAllBones();

        //Activate screenshake
        StartCoroutine(FindObjectOfType<PlayerCamera>().SetScreenShake(.9f, 0.06f));

        //Disable animator to get the ragdoll working
        GetComponent<Animator>().enabled = false;

        //Enable the ragdoll bones
        Rigidbody[] bones = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rigid in bones)
            rigid.isKinematic = false;

        //Set the layer to the ragdoll layer
        foreach (CharacterJoint joint in allCharacterJoints)
            joint.gameObject.layer = 15;
    }


    //Called to disable the ragdoll
    protected virtual void DisableRagdoll()
    {
        //Find all bones and reset their force, physics and their layer back to their original
        Rigidbody[] bones = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rigid in bones)
        {
            rigid.gameObject.layer = 10;
            rigid.angularVelocity = Vector3.zero;
            rigid.velocity = Vector3.zero;
            if (rigid.name != transform.root.name)
                rigid.isKinematic = true;
        }
        foreach (CharacterJoint joint in allCharacterJoints)
            joint.gameObject.layer = 10;

        //Transform the player location towards the ragdoll location and enable the animator
        Vector3 ragdollPos = transform.GetChild(1).position;
        transform.position = ragdollPos;
        GetComponent<Animator>().enabled = true;
    }

    //This is used to prevent weird teleportation bugs that happend because our ragdoll is not using the same collision as our active object
    protected void SetPositionAfterRagdoll()
    {
        //Get the position of our Ragdoll location
        GameObject ragdoll = transform.GetChild(1).gameObject;
        Vector3 ragdollPos = transform.GetChild(1).position;
        if (ragdollPos != transform.position)
        {
            //Set our position to our ragdoll aslong as its moving
            if (ragdoll.GetComponent<Rigidbody>().velocity.magnitude > 0.1f)
            {
                transform.position = ragdollPos;
                ragdoll.transform.position = ragdollPos;
            }
        }
    }
    
    //Called to disable the ragdoll after being ragdolled
    public virtual IEnumerator RagdollGetUp(float getUpTime)
    {
        //Set the getUpTime from the character and use this to wait before standing back up
        float time = getUpTime;
        gameObject.layer = 10;
        knockedOut = true;
        isGrabbed = false;
        Time.timeScale = 1f;

        //this loop is used to reset the location of the main object to the ragdoll location otherwise it will get inaccurate positions of eachother
        while (time > 0)
        {
            SetPositionAfterRagdoll();
            time -= 1f;
            yield return new WaitForEndOfFrame();
        }

        //If the character is still alive
            if (characterHealth > 0)
            {
            //Disable the ragdoll
                DisableRagdoll();

            //Play a getup animation based on which way the face is facing (towards the ground or towards the sky)
                if (Physics.Raycast(allCharacterJoints[9].transform.position, Vector3.forward, 1))
                    animatorController.Play("GetUp" + 1);
                else
                    animatorController.Play("GetUp" + 2);

                yield return new WaitForSeconds(2f);

            //Reset the variables
            knockedOut = false;
            groundHitOnce = false;
            stunDuration = 0;
            gameObject.layer = 8;
            characterAttackCoolDown = 0;
        }
        yield return null;
    }
    //Base for collisionEnter events
    protected virtual void OnCollisionEnter(Collision collider)
    {
        //If the object is not a thrown physics object no need to execute this code
        if (!collider.gameObject.GetComponent<PhysicsObject>())
            return;
    }

    //Base for triggerEnter events
    protected virtual void OnTriggerEnter(Collider collider)
    {
        //IF the collider is a weapon and its not your weapon take a hit from that weapon
        if (collider.GetComponent<Weapon>() && collider.GetComponent<Weapon>() != weaponScript)
            collider.GetComponent<Weapon>().TakeHit();
               
        //If you touch the water play the water sound and die
        if (collider.gameObject.name.Contains("Water"))
        {
            soundEffects.PlaySound(SoundEffects.soundNames.Water);
            Die();
        }

        //If you touch a level boundary you die
        if (collider.gameObject.name.Contains("Boundary"))
            Die();
    }

    //Called to disable hitboxes from the player
    protected virtual void DisableActiveHitboxes()
    {
        //Find every hitbox and disable the hitboxinstantly 
        foreach (Hitbox box in hitboxes)
        {
            box.GetComponent<Hitbox>().DisableHitboxInstant();
        }
    }

    //Called in the DoAttack to pick a random attack
    private IEnumerator PickRandomAttack(int attackOptions)
    {
        //Find a random attack in the range
        int newAttackSelect = Random.Range(0, attackOptions);

        //If its not the last attack that was selected (no animations twice in a row from the random selection queue)
        if (randomAttackSelection != newAttackSelect)
        {
            //Set this attack
            randomAttackSelection = newAttackSelect;
            animatorController.SetInteger("AttackRandom", randomAttackSelection);
            yield return null;
        }
        //If it is the same one restart this Coroutine until succes
        else
            StartCoroutine(PickRandomAttack(attackOptions));
    }
}