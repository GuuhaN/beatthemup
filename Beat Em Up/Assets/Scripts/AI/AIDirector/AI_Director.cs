﻿using System.Collections.Generic;
using System.Linq;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

[RequireComponent(typeof(AI_Director_Serializable))]
public class AI_Director : MonoBehaviour
{
    /** Publics */
    public int maxCapacity
    {
        get { return _maxCapacity; }
        protected set { _maxCapacity = value; }
    }
    public const int maxTargetsToLookFor = 4;

    /** Privates */
    [SerializeField] private int _maxCapacity;
    private int playerIndex;

    /** Classes */
    public AI_Director_Serializable[] aiDirector;
    public List<Character> allPlayers = new List<Character>();
    private PlayerJoining playerJoining;

    /** In the awake you initialize all classes that are needed. Also the capacity for the lists that is declared by the user in the inspector. */
    private void Awake()
    {
        playerJoining = FindObjectOfType<PlayerJoining>();
        // Set max capacity for both of the lists.
        for (int i = 0; i < aiDirector.Length; i++)
        {
            aiDirector[i].possibleEnemies.Capacity = maxCapacity;
            aiDirector[i].backupEnemies.Capacity = 1;
        }
    }

    /** In the Update function all players will be checked. If the player joined, check their position and all the players in general. */
    private void Update()
    {
        for (int i = 0; i < playerJoining.players.Length; i++)
        {
            if (!aiDirector[i].player)
            {
                aiDirector[i].player = playerJoining.players[i].gameObject;
            }
            aiDirector[i].playerPosition = aiDirector[i].player.transform.position;
        }
        FindAllPlayers();    
    }

    /** The main function to find all the players in the game. */
    private void FindAllPlayers()
    {
        /** These checks are for the AI_Director class so they know what is happening with the players in game. */
        for (int i = 0; i < playerJoining.players.Length; i++)
        {
            aiDirector[i].isPlayerAffected =
                aiDirector[i].player.GetComponent<Character>().knockedOut ||
                aiDirector[i].player.GetComponent<Character>().isGrabbed
                || aiDirector[i].player.GetComponent<Character>().hasDied;
        }

        /** When the count of allPlayers list is higher/even than maxTargetsToLookFor int, it will execute the return statement. */
        if (allPlayers.Count >= maxTargetsToLookFor)
            return;

        /** Just to clean the list from unneeded information */
        allPlayers.Clear();

        /** Players get added to the list when they are active in game. */
        for (int i = 0; i < playerJoining.players.Length; i++)
        {
                allPlayers.Add(playerJoining.players.ToList().FindAll(x => x.gameObject.activeSelf)[i]);
        }

        /** With this function the list will be copied to the Behavior Designer. */
        GlobalVariables.Instance.SetVariableValue("allPlayers", allPlayers);
    }

    #region Spawn and wave system
    //Called at the enemy a.i.
    public bool CheckAvailableTarget(Transform playerTarget, GameObject requestedObject)
    {
        //Finds the player target and see if it contains in the list.
        playerIndex = aiDirector.ToList().FindIndex(x => x.player.transform == playerTarget);

        for (int i = 0; i < aiDirector.Length; i++)
        {
            if (playerIndex != i)
                if (aiDirector[i].possibleEnemies.Contains(requestedObject) || aiDirector[i].backupEnemies.Contains(requestedObject))
                     return false;
        }
        //Adds an enemy to the player's list they are targeting. 
        //If they don't exist in the list already.
        //If the count of the list is lower than the maxCapacity and if the player is affected or not.
        if (!aiDirector[playerIndex].possibleEnemies.Contains(requestedObject) &&
            aiDirector[playerIndex].possibleEnemies.Count < maxCapacity && !aiDirector[playerIndex].isPlayerAffected)
        {
            if (aiDirector[playerIndex].backupEnemies.Count > 0)
            {
                if (aiDirector[playerIndex].backupEnemies.Contains(requestedObject))
                {
                    if (requestedObject.GetComponent<EnemyData>())
                    {
                        //requestedObject.GetComponent<EnemyData>().target = playerTarget;
                        aiDirector[playerIndex].possibleEnemies.Add(requestedObject);
                        aiDirector[playerIndex].backupEnemies.Remove(requestedObject);
                        return true;
                    }
                }
            }
            else
            {
                aiDirector[playerIndex].possibleEnemies.Add(requestedObject);
                return true;
            }
        }
        return false;
    }

    public bool AddToBackUpList(Transform playerTarget, GameObject requestedObject)
    {
        playerIndex = aiDirector.ToList().FindIndex(x => x.player.transform == playerTarget);

        for (int i = 0; i < aiDirector.Length; i++)
        {
            if (aiDirector[i].possibleEnemies.Contains(requestedObject) || aiDirector[i].backupEnemies.Contains(requestedObject))
                return false;
        }
        if (!aiDirector[playerIndex].backupEnemies.Contains(requestedObject) &&
            aiDirector[playerIndex].backupEnemies.Count < aiDirector[playerIndex].backupEnemies.Capacity && !aiDirector[playerIndex].isPlayerAffected)
        {
            aiDirector[playerIndex].backupEnemies.Add(requestedObject);
            return true;
        }
        return false;
    }

    //Called at enemy A.I
    public void DeleteTarget(Transform playerTarget, GameObject requestedObject)
    {       
        //Finds the player target and see if it contains in the list.
        int playerIndex = aiDirector.ToList().FindIndex(x => x.player.transform == playerTarget);
        //if enemy object exists in the possibleEnemies list.
        if (aiDirector[playerIndex].possibleEnemies.Contains(requestedObject))
        {
            //Remove the target out of the list.
            aiDirector[playerIndex].possibleEnemies.Remove(requestedObject);
        }
    }

    public void DeleteBackUpTarget(Transform playerTarget, GameObject requestedObject)
    {
        playerIndex = aiDirector.ToList().FindIndex(x => x.player.transform == playerTarget);
        //if enemy object exists in the backupEnemies list.
        if (aiDirector[playerIndex].backupEnemies.Contains(requestedObject))
        {
            //Remove the target out of the list.
            aiDirector[playerIndex].backupEnemies.Remove(requestedObject);
        }

    }
    #endregion
}
