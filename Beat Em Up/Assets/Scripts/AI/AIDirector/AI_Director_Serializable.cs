﻿using UnityEngine;
using System;
using System.Collections.Generic;


/**<summary
 * 
 * Deze class is bedoeld om per speler de stats bij te houden. Deze worden zelf ingevuld in de AI_Director class zelf.
 * 
 * </summary>*/
[Serializable]
public class AI_Director_Serializable
{
    public GameObject player;
    public Vector3 playerPosition;
    public List<GameObject> possibleEnemies, backupEnemies = new List<GameObject>();
    public bool isPlayerAffected;
}
