﻿using UnityEngine;

public class BossObject : MonoBehaviour
{

    public float maxHealth;
    public float currentHealth;
    public GameObject healthBar;
    public GameObject middleBar;
    public float middleBarHealth;
    public bool hitByAnything;
    private float timeTillComboEnds;

    void Update()
    {
        healthBar.GetComponent<RectTransform>().sizeDelta = new Vector2(currentHealth / maxHealth * 770f, 10);
        middleBar.GetComponent<RectTransform>().sizeDelta = new Vector2(middleBarHealth / maxHealth * 770f, 10f);

        if (hitByAnything)
        {
            timeTillComboEnds = 1f;
            hitByAnything = false;
        }
        if (timeTillComboEnds > 0)
            timeTillComboEnds -= Time.deltaTime;
        if (middleBarHealth > currentHealth && timeTillComboEnds <= 0)
        {
            middleBarHealth -= Time.deltaTime * 25f;
            if (middleBarHealth < currentHealth)
                middleBarHealth = currentHealth;
        }

        if (currentHealth > middleBarHealth)
            middleBarHealth = currentHealth;
    }
}