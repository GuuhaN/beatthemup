﻿using UnityEngine;
using System.Collections;
/**<summary
 * 
 * This class is for every enemy. 
 * With the purpose of initializing components and having functionality that could not be accessed in Behavior Designer.
 * 
 * </summary>*/
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyData : Character
{
    public NavMeshAgent agent { get; private set; }
    public NavMeshObstacle obstacle { get; private set; }
    public AttackInfo randomAttackInfo { get; protected set; }
    private Animator anim;

    public bool flinching { get; set; }
    public bool gettingUp { get; set; }

    [SerializeField] private int damage, fall;

    /** Initializing the components and inherit the Start from the base class. */
    protected override void Start()
    {
        base.Start();
        agent = GetComponent<NavMeshAgent>();
        obstacle = GetComponent<NavMeshObstacle>();
        anim = GetComponent<Animator>();

        agent.updateRotation = true;
        agent.updatePosition = true;
    }

    /** Game Update */
    protected override void Update()
    {
        base.Update();

        if (stunDuration > 0)
            stunDuration -= Time.deltaTime;

        /** When this character is not knocked out, the character will forced to zero velocity.
            This is to prevent the character from having weird movements. */
        if (!knockedOut)
            GetComponent<Rigidbody>().velocity = new Vector3(0, GetComponent<Rigidbody>().velocity.y, 0);

        /*if (flinching)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                flinching = false;
        }*/
    }

    /** When a Character dies, this function can be called which will spawn particles */
    protected override void Die()
    {
        base.Die();
        hasDied = true;
        GameObject party = (GameObject)Instantiate(deadParticle[0].gameObject, transform.GetChild(1).position, deadParticle[0].transform.rotation);
    }

    /** When this function is called, it will execute an attack that is chosen in Behavior Designer */
    public void ExecuteAttack()
    {
        DoAttack(randomAttackInfo);
    }

    /** */
    protected override void OnCollisionEnter(Collision collider)
    {
        base.OnCollisionEnter(collider);
        /** layer 15 = PhysicsRagdoll. This is to check if any other character
            is ragdolling and falling over this character. */
        if (collider.gameObject.layer == 15 && !knockedOut && !hasDied && canGetHit)
        {
            if (collider.gameObject.GetComponent<Rigidbody>().velocity.magnitude < 5f)
                return;

            if (!collider.transform.root.GetComponent<EnemyData>())
                return;

            /** This is to check if any of the bones of the character joint */
            for (int i = 0; i < allCharacterJoints.Length; i++)
            {
                /** No damage multiplier, cannot get damage from the damage causer
                Make seperate counter to count total damage */
                if (collider.transform.root.GetComponent<EnemyData>().damage >= 10)
                {
                    /** The effect if the enemy gets hit by another flying ragdoll */
                    TakeDamage(collider.transform.root.GetComponent<EnemyData>().damage);
                    DisableActiveHitboxes();
                    EnableRagdoll();
                    
                    soundEffects.PlaySound(SoundEffects.soundNames.RagdollHit);
                    transform.GetChild(1)
                        .GetComponent<Rigidbody>()
                        .AddForce(-transform.forward*
                                  (collider.gameObject.GetComponent<Rigidbody>().velocity.magnitude*100));

                    if (characterHealth > 0)
                        StartCoroutine(RagdollGetUp(getUpTime));
                    else
                        Die();

                    Vector3 colliderPos = collider.transform.position;
                    foreach (ParticleEffects party in hitParticle)
                        Instantiate(party.gameObject,
                            new Vector3(Mathf.Lerp(colliderPos.x, transform.position.x, 0.5f), colliderPos.y,
                                Mathf.Lerp(colliderPos.z, transform.position.z, 0.5f)), Quaternion.identity);
                }

                if (collider.transform.root.GetComponent<EnemyData>().damage > 0)
                    collider.transform.root.GetComponent<EnemyData>().damage -= 10;
                else if(collider.transform.root.GetComponent<EnemyData>().damage <= 0)
                    collider.transform.root.GetComponent<EnemyData>().damage = 0;

            }
        }

        /** This part is to check wether this character gets hit by any physics object that are thrown by players 
            layer 12 = LiftableObject */
        if (collider.gameObject.layer == 12 && !knockedOut && collider.gameObject.GetComponent<PhysicsObject>().playerID != 0)
        {
            int ID = collider.gameObject.GetComponent<PhysicsObject>().playerID;
            damage = (int) collider.gameObject.GetComponent<PhysicsObject>().objectDamage;
            float speed = collider.gameObject.GetComponent<PhysicsObject>().currentSpeed;
            TakeDamage(damage);
            DisableActiveHitboxes();
            EnableRagdoll();

            transform.GetChild(1).GetComponent<Rigidbody>().AddForce(-transform.forward * (speed * 1000));
            GameObject.Find("Player" + ID).GetComponent<Player>().GiveMyEnemyHealth(characterHealth, characterMaxHealth, characterPoints, characterName);

            if (characterHealth > 0)
                StartCoroutine(RagdollGetUp(getUpTime));

            Vector3 colliderPos = collider.transform.position;
            foreach (ParticleEffects party in hitParticle)
                Instantiate(party.gameObject, new Vector3(Mathf.Lerp(colliderPos.x, transform.position.x, 0.5f), colliderPos.y, Mathf.Lerp(colliderPos.z, transform.position.z, 0.5f)), Quaternion.identity);
        }
    }

    /** This function is to check incoming damages that are caused by the player who attacks this character. */
    protected override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if (!hasDied && canGetHit)
        {
            if (!collider.gameObject.GetComponent<Hitbox>())
                return;

            if (collider.gameObject.GetComponent<Hitbox>().colliderList.Contains(gameObject.GetComponent<Collider>()))
                return;
            
            if (collider.transform.root.GetComponent<Player>() && !knockedOut)
            {
                collider.gameObject.GetComponent<Hitbox>().colliderList.Add(gameObject.GetComponent<Collider>());
                DisableActiveHitboxes();
                Vector3 rot = new Vector3(collider.transform.position.x, this.transform.position.y, collider.transform.position.z);
                transform.LookAt(rot);
                soundEffects.PlaySound(collider.transform.GetComponent<Hitbox>().soundName);
                Vector3 colliderPos = collider.transform.position;

                foreach (ParticleEffects party in hitParticle)
                    Instantiate(party.gameObject, new Vector3(Mathf.Lerp(colliderPos.x, transform.position.x, 0.5f), colliderPos.y, Mathf.Lerp(colliderPos.z, transform.position.z, 0.5f)), Quaternion.identity);

                animatorController.SetInteger("HitRandom", Random.Range(0, 5));
                StartCoroutine(ResetFlinching());
                //flinching = true;

                if (collider.GetComponent<Hitbox>().colliderStaysOn)
                    collider.enabled = true;
                else
                    collider.enabled = false;

                damage = collider.transform.root.GetComponent<Player>().characterDamage; 
                TakeDamage(damage);
                collider.gameObject.transform.root.GetComponent<Player>().GiveMyEnemyHealth(characterHealth, characterMaxHealth, characterPoints, characterName);
                if (hasDied)
                    return;

                /** If the knockdown strength is high enough of the player that is hitting this character,
                    then this character will execute this code which will trigger the ragdoll */
                if (collider.GetComponent<Hitbox>().knockDownStrength > characterKnockDownStrength)
                {
                    if (collider.GetComponent<Hitbox>().stunDuration > 0)
                    {
                        isGrabbed = false;
                        stunDuration = collider.GetComponent<Hitbox>().stunDuration;
                        isStunned = true;
                    }

                    else {
                        EnableRagdoll();
                        transform.GetChild(1).GetComponent<Rigidbody>().AddRelativeForce(collider.GetComponent<Hitbox>().Physics.x, collider.GetComponent<Hitbox>().Physics.y, 0);

                        if (characterHealth > 0)
                            StartCoroutine(RagdollGetUp(getUpTime));
                    }
                }
            }
        }
    }

    /** When this character gets down, this function can be called to having the character standing up again. */
    public override IEnumerator RagdollGetUp(float getUpTime)
    {
        float time = getUpTime;
        knockedOut = true;
        isGrabbed = false;
        while (time > 0)
        {
            SetPositionAfterRagdoll();
            time -= Time.deltaTime;
            if (Mathf.Abs(rigidBody.velocity.magnitude) > 0)
                rigidBody.velocity -= new Vector3(rigidBody.velocity.x * 0.5f, rigidBody.velocity.y * 0.5f, rigidBody.velocity.z * 0.5f);
            yield return new WaitForEndOfFrame();
        }
        /** This function is to reset all bones to the original position. In the Character base class. */
        FixAllBones();
        gameObject.layer = 9;
        if (characterHealth > 0)
        {
            if (Physics.Raycast(allCharacterJoints[9].transform.position, Vector3.forward, 1))
                animatorController.Play("GetUp" + 1);
            else
                animatorController.Play("GetUp" + 2);

            DisableRagdoll();
            gettingUp = true;
            yield return new WaitForSeconds(2f);
            gameObject.layer = 9;
            rigidBody.velocity = Vector3.zero;
            agent.velocity = Vector3.zero;
            knockedOut = false;
            groundHitOnce = false;
            fall = 0;
        }
        yield return null;
    }

    /** This is to reset the flinch animation of the character */
    private IEnumerator ResetFlinching()
    {
        isFlinched = true;
        yield return new WaitForEndOfFrame();
        isFlinched = false;
        StopCoroutine(ResetFlinching());
        yield return null;
    }
}