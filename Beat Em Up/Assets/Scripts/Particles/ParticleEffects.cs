﻿//Base class ParticleSystems

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleEffects : MonoBehaviour {

    //Declare duration and color of the particle
    [SerializeField] private float particleDuration;
    public Color particleColor;

	void Start () {
        GetComponent<ParticleSystem>().startColor = particleColor;
        Destroy(gameObject, particleDuration);
	}
}
