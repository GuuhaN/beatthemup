﻿//Player preview for the characters in the game

using UnityEngine;

public class PlayerPreview : MonoBehaviour {
    [SerializeField] private GameObject[] previews;
    [SerializeField]
    private GameObject spawnPoint;
    private GameObject currentPreview;
    public int ID;
    public int mySelected = -1;
    private int selected;
    private Vector3 savedRotation;

    void Start()
    {
        //Standard rotation to face the player
        savedRotation = new Vector3(0, 180, 0);
    }

	void Update () {
        //load selected character
        selected = GetComponentInParent<CharacterSelect>().currentSelected[ID-1];
        //If the selected is not equal to the selected from the character select
        if (mySelected != selected)
        {
            //Remove the preview
            if (currentPreview)
                Destroy(currentPreview);

            //Make a new preview of the new character
            currentPreview = Instantiate(previews[selected], spawnPoint.transform.position, Quaternion.identity) as GameObject;
            currentPreview.transform.SetParent(transform); 
            mySelected = selected;
        }
        //if there is a preview
        if (currentPreview)
        {
            //use right and left horizontal axis (Right Stick) To rotate your character around in the preview
            if (Input.GetAxis("RightAxisX_P" + ID) > 0)
                savedRotation = new Vector3(0, savedRotation.y - (2 * Input.GetAxis("RightAxisX_P" + ID)), 0);

            if (Input.GetAxis("RightAxisX_P" + ID) < 0)
               savedRotation = new Vector3(0, savedRotation.y + (2 * -Input.GetAxis("RightAxisX_P" + ID)), 0);

            //Saves rotation through selection
            currentPreview.transform.eulerAngles = savedRotation;
        }
    }
}
