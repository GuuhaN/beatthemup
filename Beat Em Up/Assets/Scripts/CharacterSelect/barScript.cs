﻿//Script for statistics bars in the Character Select

using UnityEngine;
using UnityEngine.UI;

public class BarScript : MonoBehaviour {
    [SerializeField]private BarType barType;
    private enum BarType { Health, Damage, Speed, Combo }
    private float Value;
    private float lastValue;
    private float barSpeed = 3;
    private int selected;
    public Text text;

    void Start()
    {
        GameObject t = Instantiate(text.gameObject);
        t.transform.SetParent(transform);
        t.transform.localPosition = new Vector3(18, 0, 0);
        t.transform.localScale = new Vector3(0.08f, 0.08f, 0.08f);
        t.GetComponent<Text>().text = barType.ToString();
    }
	void Update () {
        //Find which player you have selected
        if (GetComponentInParent<PlayerPreview>().mySelected > -1)
        {
            selected = GetComponentInParent<PlayerPreview>().mySelected;
        }
        //Take the value of this PlayerStats
        Value = FindObjectOfType<PlayerSelectStats>().playerStats[selected].Stats[(int)barType];
        //Lerp to the new value if its not already there
        if (lastValue != Value)
            lastValue = Mathf.Lerp(lastValue, Value, barSpeed * Time.deltaTime);
        GetComponent<Image>().fillAmount = (lastValue / 100);
    }
}
