﻿//Class used to display all the player stats in the Editor to adjust them with sliders for debugging purposes

using UnityEngine;

public class PlayerSelectStats : MonoBehaviour {
    [SerializeField]
    public SerializePlayerStats[] playerStats;

    private void Start()
    {
        //Load in all the statistics for each player from the bars
        for (int i = 0; i < playerStats.Length; i++)
        {
            playerStats[i].Stats = new float[4];
            playerStats[i].Stats[0] = playerStats[i].Health;
            playerStats[i].Stats[1] = playerStats[i].Damage;
            playerStats[i].Stats[2] = playerStats[i].Speed;
            playerStats[i].Stats[3] = playerStats[i].Combo;
        }      
    }
}
