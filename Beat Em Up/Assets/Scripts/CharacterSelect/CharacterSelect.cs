﻿//The Characterselect manager script. Handles the input and visualisation of the Characterselect


using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Collections;

public class CharacterSelect : MonoBehaviour
{
    private SaveCharacters saveCharacter;
    public Image[] Faces;
    [SerializeField]
    private int amountOfPlayers;
    [SerializeField] private bool[] joined;
    private Selector[] selectors;
    public int[] currentSelected;
    private bool[] pressed;
    private Image[] playerJoinUI;
    private RawImage[] playerSelectUI;

    private void Awake()
    {
        //Find characters and set the values of max 4 for each array
        saveCharacter = FindObjectOfType<SaveCharacters>();
        saveCharacter.characterIDs = new int[4];
        joined = new bool[4];
        playerJoinUI = new Image[4];
        playerSelectUI = new RawImage[4];
        pressed = new bool[4];
        selectors = FindObjectsOfType<Selector>();


        currentSelected = new int[4];
        for (int i = 0; i < 4; i++)
        {
            playerJoinUI[i] = GameObject.Find("JoinPlayer" + (i + 1)).GetComponent<Image>();
            playerSelectUI[i] = GameObject.Find("SelectUI" + (i + 1)).GetComponent<RawImage>();
            currentSelected[i] = 0;
        }
    }
    private void Update()
    {
        SelectedCharacters();
        JoinPlayers();

        //See if everyone is ready to play
        for (int i = 0; i < 4; i++)
        {
            //If atleast 1 player joined
            if (joined[i])
            {
                //If for each player that joined they are ready
                for (int c = 0; c < 4; c++)
                {
                    if (joined[c])
                    {
                        if (!selectors[c].selected)
                        {
                            //Return if not selected
                            return;
                        }
                    }

                    //If everyone joined set selected character
                    if (joined[c])
                        saveCharacter.characterIDs[c] = currentSelected[c];

                    //remaining characters are set to -1 (no character)
                    else
                        saveCharacter.characterIDs[c] = -1;
                }

                SceneManager.LoadScene(1);
            }
        }
    }
    private void JoinPlayers()
    {
        //If startkey is pressed join with that player, back key to remove that player or press space to join everyone at same time
        for (int i = 0; i < 4; i++)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                joined[i] = true;
            }
            if (Input.GetButtonDown("StartKey_P" + (i + 1)))
            {
                joined[i] = true;
            }
            if (Input.GetButtonDown("BackKey_P" + (i + 1)))
            {
                joined[i] = false;
            }

        }
    }

    //Handles the player selection
    private void SelectedCharacters()
    {
        for (int i = 0; i < 4; i++)
        {
            //Once player is joined enable the selector image and the player select UI, remove the joining text
            if (joined[i])
            {
                playerJoinUI[i].gameObject.SetActive(false);
                playerSelectUI[i].gameObject.SetActive(true);
                selectors[i].gameObject.SetActive(true);

                if (!selectors[i].selected)
                {
                    //If a character is not selected and a is pressed this character is selected
                    if (Input.GetButtonDown("Akey_P" + (i + 1)))
                    {
                        selectors[i].selected = true;
                    }

                    //Use horizontal input to the right
                    if (Input.GetAxisRaw("Horizontal_P" + (i + 1)) == 1)
                    {
                        if (!pressed[i])
                        {
                            currentSelected[i]++;
                            pressed[i] = true;
                        }
                    }
                    if (Input.GetAxisRaw("Horizontal_P" + (i + 1)) == 0)
                        pressed[i] = false;

                    //Use horizontal input to the left
                    if (Input.GetAxisRaw("Horizontal_P" + (i + 1)) == -1)
                    {
                        if (!pressed[i])
                        {
                            currentSelected[i]--;
                            pressed[i] = true;
                        }
                    }
                    //Make sure there is no array out of index
                    if (currentSelected[i] > Faces.Length - 1)
                        currentSelected[i] = 0;
                    if (currentSelected[i] < 0)
                        currentSelected[i] = Faces.Length - 1;

                    //Put selector on the right face with the offset set for the player indicator
                    selectors[i].transform.position = new Vector3(Faces[currentSelected[i]].transform.position.x + selectors[i].offsetX, Faces[currentSelected[i]].transform.position.y + selectors[i].offsetY, Faces[currentSelected[i]].transform.position.z);
                }
                //If player leaves remove the selector again
                else
                {
                    if (Input.GetButtonDown("Bkey_P" + (i + 1)) && joined[i])
                    {
                        selectors[i].selected = false;
                    }
                }
            }

            //Remove everything if player is not in the game anymore
            else
            {
                playerJoinUI[i].gameObject.SetActive(true);
                playerSelectUI[i].gameObject.SetActive(false);
                selectors[i].gameObject.SetActive(false);
                selectors[i].selected = false;
            }
        }
   }
}