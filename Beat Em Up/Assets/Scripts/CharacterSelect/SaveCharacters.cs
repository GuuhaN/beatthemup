﻿//Save Class for characters

using UnityEngine;
public class SaveCharacters : MonoBehaviour {
    public int[] characterIDs;

    private void Start()
    {
        //Set default to -1 for characters that have no be changed in the character select
        for (int i = 0; i < characterIDs.Length; i++)
        {
            characterIDs[i] = -1;
        }
        //Make sure it loads in the mainScene
        DontDestroyOnLoad(gameObject);
    }
}
