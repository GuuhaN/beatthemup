﻿//Serializeable Class to visualize the stats with sliders in the inspector

using System;
using UnityEngine;

[Serializable]
public class SerializePlayerStats {
    [SerializeField]private string arrayName;
    [HideInInspector] public float[] Stats;

    //Slider values
    [Range(0,100)]public float Health;
    [Range(0, 100)]public float Damage;
    [Range(0, 100)]public float Speed;
    [Range(0, 100)]public float Combo;

}
