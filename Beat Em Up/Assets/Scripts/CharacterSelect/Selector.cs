﻿//Character Select selector base class

using UnityEngine;
using UnityEngine.UI;

public class Selector : MonoBehaviour {
    //Set default offset x and y to change the location on the character select images
    public float offsetX;
    public float offsetY;
    public bool selected;


    private void Update()
    {
        //Change color to full if selected
        if (selected)
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, 1f);

        //Change color to transparent alpha to show you haven't locked yet
        else
            GetComponent<Image>().color = new Color(GetComponent<Image>().color.r, GetComponent<Image>().color.g, GetComponent<Image>().color.b, 0.4f);
    }
}
