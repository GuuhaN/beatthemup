﻿using UnityEngine;

/**<summary
 * 
 * This script is for the purpose of locking down on characters so the players have a sort of assist on aiming
 * down other characters. Enemy A.I's have constant locking.
 * 
 * </summary>*/
public class AimAssist : MonoBehaviour
{
    public Transform closestCharacter { get; private set; }
    private EnemyData[] allEnemies;
    private Player[] allPlayers;

    public float visionLengthMax
    {
        get { return _visionLengthMax; }
        private set
        {
            _visionLengthMax = value;
        }
    }
    public float viewAngle { get { return _viewAngle; }
        set { _viewAngle = value; }
    }

    [SerializeField, Range(1f,5f)] private float _visionLengthMax;
    [SerializeField, Range(22.5f, 180f)] private float _viewAngle;
    [SerializeField] private float rotateSpeed;

    /** This is the main locking on towards characters */
    public bool UpdateRotationTowardsTarget()
    {
        closestCharacter = GetClosestPlayer();

        if (!closestCharacter)
            return false;

        Vector3 targetDirection = closestCharacter.position - transform.position;
        float angle = Vector3.Angle(targetDirection, transform.forward);
        float visionLength = Vector3.Distance(transform.position, closestCharacter.position);
        Quaternion lookAtRot = Quaternion.LookRotation(closestCharacter.position - transform.position);
        if (angle > -viewAngle && angle < viewAngle && visionLength < visionLengthMax)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation,
                new Quaternion(0, lookAtRot.y, 0, lookAtRot.w), Time.deltaTime*rotateSpeed);
            return true;
        }
        return false;
    }

    /** This script is to find the closest player/enemy, so the script knows which one to lock at */
    private Transform GetClosestPlayer()
    {
        Transform closestCharacterTransform = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;

        /** To differentiate the character classes */
        if (GetComponent<Player>())
        {
            allEnemies = FindObjectsOfType<EnemyData>();
            for (int i = 0; i < allEnemies.Length; i++)
            {
                foreach (Transform t in allEnemies[i].transform)
                {
                    float distance = Vector3.Distance(t.position, currentPos);
                    if (!(distance < minDist)) continue;
                    closestCharacterTransform = t;
                    minDist = distance;
                }
            }
        }
        if (GetComponent<EnemyData>())
        {
            allPlayers = FindObjectsOfType<Player>();
            for (int i = 0; i < allPlayers.Length; i++)
            {
                foreach (Transform t in allPlayers[i].transform)
                {
                    float distance = Vector3.Distance(t.position, currentPos);
                    if (!(distance < minDist)) continue;
                    closestCharacterTransform = t;
                    minDist = distance;
                }
            }
        }
        return closestCharacterTransform.root;
    }
}