﻿using UnityEngine;
using UnityEngine.UI;

public class ChargeBar : MonoBehaviour
{
    public Image myImage { get; set; }
    public float roundedLength { get; protected set; }
    public float fillAmount
    {
        get { return myImage.fillAmount; }
        private set { myImage.fillAmount = value; }
    }

    [SerializeField] private int splitLevels;

    public int currentSplit { get; set; }
    private float sliceLength;

    void Awake()
    {
        myImage = GetComponent<Image>();
        sliceLength = myImage.fillAmount / splitLevels;
        myImage.fillAmount = 0f;
    }

    void Update()
    {
        if (myImage.fillAmount > sliceLength*currentSplit && currentSplit < splitLevels)
            currentSplit++;

        roundedLength = sliceLength*currentSplit;

        myImage.color = new Color(1f, 1f - myImage.fillAmount, 1f - myImage.fillAmount);   
    }
}
