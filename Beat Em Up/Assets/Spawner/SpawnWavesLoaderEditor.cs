﻿//Editor Script for the SpawnWavesLoader that we use to get files from out of a directory
#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SpawnWavesLoad))]
public class SpawnWavesLoaderEditor : Editor {
    
    //Show a button in the inspector and when its pressed refresh the lists with objects (PRESS THIS BUTTON WHEN ADDING NEW OBJECTS TO THE FOLDER)
    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Press this button to refresh the folders (Enemies) & (PickUps)", EditorStyles.boldLabel);
        if (GUILayout.Button("Reload Enemies & PickUps"))
        {
            ((SpawnWavesLoad)target).allFilesInFolder.Clear();
            ((SpawnWavesLoad)target).allFilesInFolder2.Clear();
        }
    }
}
#endif