﻿//Script used for loading all the prefabs used for the SpawnWaves editor script. This way the designers didn't have to touch the arrays to fill them up with the stuff they required.
#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;

[ExecuteInEditMode]
public class SpawnWavesLoad : MonoBehaviour
{
    public List<string> allFilesInFolder, allFilesInFolder2 = new List<string>();
    private SpawnWaves wavesScript;
    void Update()
    {
        wavesScript = GetComponent<SpawnWaves>();
        if (allFilesInFolder.Count == 0)
        {
            //Find the folder we want to fill in this case /Assets/Art/Prefabs/Enemies
            allFilesInFolder = Directory.GetFiles(Directory.GetCurrentDirectory() + "/Assets/Art/Prefabs/Enemies").ToList();
            for (int i = 0; i < allFilesInFolder.Count; i++)
            {
                //Prevent adding .meta files.
                if (allFilesInFolder[i].Contains(".meta")) {
                    allFilesInFolder.Remove(allFilesInFolder[i]);
                    --i;                  
                }
            }
            //Load them into the enemyprefabs used in the editorscript
            wavesScript.enemyPrefabs = new EnemyData[allFilesInFolder.Count];
            for (int i = 0; i < allFilesInFolder.Count; i++)
            {
                //Remove the split symbol that we dont use and causes errors.
                if (allFilesInFolder[i].Contains(Directory.GetCurrentDirectory()))
                {
                    allFilesInFolder[i] = allFilesInFolder[i].Replace(Directory.GetCurrentDirectory() + "/", "");
                }
                //Put them in the wavescript as enemyData so we can instantiate them with their data
                GameObject enemy = (GameObject)AssetDatabase.LoadAssetAtPath(allFilesInFolder[i], typeof(GameObject));
                if (enemy.GetComponent<EnemyData>())
                    wavesScript.enemyPrefabs[i] = enemy.GetComponent<EnemyData>();
                //If your object does not contain an enemyData script but is in this folder
                else
                    Debug.LogError("PREFAB " + enemy.gameObject.name + " DOES NOT HAVE AN ENEMYDATA COMPONENT");
            } 
        }

        //Do the same thing for the PickUps but load in from all the folders
        if (allFilesInFolder2.Count == 0)
        {
            for (int i = 0; i < Directory.GetFiles(Directory.GetCurrentDirectory() + "/Assets/Art/Prefabs/PickUps/Health").Length; i++)
            {
                allFilesInFolder2.Add(Directory.GetFiles(Directory.GetCurrentDirectory() + "/Assets/Art/Prefabs/PickUps/Health").ToList()[i]);
            }
            for (int i = 0; i < Directory.GetFiles(Directory.GetCurrentDirectory() + "/Assets/Art/Prefabs/PickUps/Weapons").Length; i++)
            {
                allFilesInFolder2.Add(Directory.GetFiles(Directory.GetCurrentDirectory() + "/Assets/Art/Prefabs/PickUps/Weapons").ToList()[i]);
            }
            for (int i = 0; i < Directory.GetFiles(Directory.GetCurrentDirectory() + "/Assets/Art/Prefabs/PickUps/Points").Length; i++)
            {
                allFilesInFolder2.Add(Directory.GetFiles(Directory.GetCurrentDirectory() + "/Assets/Art/Prefabs/PickUps/Points").ToList()[i]);
            }    

            //Remove the meta
            for (int i = 0; i < allFilesInFolder2.Count; i++)
            {
                if (!allFilesInFolder2[i].Contains(".meta")) continue;
                allFilesInFolder2.Remove(allFilesInFolder2[i]);
                --i;
            }
            wavesScript.itemsToDrop = new PickUp[allFilesInFolder2.Count];
            //Remove the split char
            for (int i = 0; i < allFilesInFolder2.Count; i++)
            {
                if (allFilesInFolder2[i].Contains(Directory.GetCurrentDirectory()))
                {
                    allFilesInFolder2[i] = allFilesInFolder2[i].Replace(Directory.GetCurrentDirectory() + "/", "");
                }
                
                //Add them to the editor script if they contain the PickUp script
                GameObject item = (GameObject)AssetDatabase.LoadAssetAtPath(allFilesInFolder2[i], typeof(GameObject));
                if (item.GetComponent<PickUp>())
                    wavesScript.itemsToDrop[i] = item.GetComponent<PickUp>();
                else
                    Debug.LogError("PREFAB " + item.gameObject.name + " DOES NOT HAVE A PICKUP COMPONENT");
            }
        }
    }
}
#endif